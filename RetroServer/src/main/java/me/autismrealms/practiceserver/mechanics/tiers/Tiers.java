package me.autismrealms.practiceserver.mechanics.tiers;

import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class Tiers implements Listener {

    public static String[] prefix = {"Weak", "Old", "Magical", "Ancient", "Legendary", "Frozen", "Molten", "Heroic", "Acidic", "Dark"};

    public static String[] mobNames = {"Broken", "Old", "Magical", "Ancient", "Legendary", "Frozen", "Melting", "Heroic", "Burning", "Dark"};

    public static ChatColor[] colors = {ChatColor.WHITE, ChatColor.GREEN, ChatColor.AQUA, ChatColor.LIGHT_PURPLE, ChatColor.YELLOW,
            ChatColor.BLUE, ChatColor.DARK_RED, ChatColor.GOLD, ChatColor.DARK_AQUA, ChatColor.DARK_GRAY};

    public static int getStringTier(String s){
        int tier = 0;
        if(s.contains("[")){
            s = s.split("]")[1];
        }
        for (ChatColor color : Tiers.colors){
            if(s.contains(color.toString())){
                return tier;
            }
            tier++;
        }
        return tier;
    }

    public static int getHighestTierArmor(LivingEntity entity) {
        int tier = 0;
        for (ItemStack item : entity.getEquipment().getArmorContents()) {
            if (tier < getStringTier(item.getItemMeta().getDisplayName()))
                tier = getStringTier(item.getItemMeta().getDisplayName());
        }
        return tier;
    }
}
