package me.autismrealms.practiceserver.mechanics.altar;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.drops.CreateDrop;
import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.player.Listeners;
import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import me.autismrealms.practiceserver.utils.GlowAPI;
import me.autismrealms.practiceserver.utils.JSONMessage;
import me.autismrealms.practiceserver.utils.Particles;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

class AltarInstance {
    public Player p;
    public List<ItemStack> items;
    public Location location;
    public List<Item> droppedItems;

    AltarInstance(List<ItemStack> items, Location location, Player p){
        this.items = items;
        this.location = location;
        this.p = p;
        droppedItems = new ArrayList<>();
    }

    public List<ItemStack> getItems() { return items; }
    public Location getLocation() {return location; }

    public void generateItem(){
        Random r = new Random();
        List<Integer> itemIDs = new ArrayList<>();
        for(ItemStack is : items){
            itemIDs.add(Items.getItemType(is.getType()));
        }
        List<String> itemlore = items.get(0).getItemMeta().getLore();

        int itemID = itemIDs.get(r.nextInt(itemIDs.size()));
        int tier = Tiers.getStringTier(items.get(0).getItemMeta().getDisplayName()) + 1;
        int rarity =  Items.getRarityFromItem(items.get(0));

        ItemStack createdItem = CreateDrop.createDrop(tier, itemID, Math.min(rarity+1, 4));


        String reason = " has created a(n) ";
        final JSONMessage normal = new JSONMessage(p.getDisplayName() + ChatColor.RESET + reason, ChatColor.WHITE);
        List<String> hoveredChat = new ArrayList<>();
        ItemMeta meta = createdItem.getItemMeta();
        hoveredChat.add((meta.hasDisplayName() ? meta.getDisplayName() : createdItem.getType().name()));
        if (meta.hasLore()) hoveredChat.addAll(meta.getLore());
        normal.addHoverText(hoveredChat, ChatColor.getLastColors(createdItem.getItemMeta().getDisplayName()) + ChatColor.BOLD.toString() +  ChatColor.UNDERLINE + "SHOW");
        normal.addText(" using the Altar!");
        for (Entity near : location.getWorld().getNearbyEntities(location, 50, 50, 50)) {
            if (near instanceof Player) {
                Player nearPlayers = (Player) near;
                normal.sendToPlayer(nearPlayers);
            }
        }
        p.getInventory().addItem(createdItem);

        p.getWorld().playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.25f);
        final Firework fw = (Firework) p.getWorld().spawnEntity(p.getLocation(), EntityType.FIREWORK);
        final FireworkMeta fwm = fw.getFireworkMeta();
        final FireworkEffect effect = FireworkEffect.builder().flicker(false).withColor(Color.YELLOW).withFade(Color.YELLOW).with(FireworkEffect.Type.BURST).trail(true).build();
        fwm.addEffect(effect);
        fwm.setPower(0);
        fw.setFireworkMeta(fwm);
    }

    public void cancelAltar(){
        if (Altar.altarInstances.get(p).items.size() > 0) {
            for (ItemStack item1 : Altar.altarInstances.get(p).items) {
                p.getInventory().addItem(item1);
                p.sendMessage(ChatColor.RED + "- " + item1.getItemMeta().getDisplayName());
            }
            for(Item i: droppedItems) i.remove();
            Altar.altarInstances.remove(p);
            p.sendMessage(ChatColor.RED + ">> Altar has been cancelled!");
        }
    }

    Location randomOffset(Location loc, int radius){
        Random r = new Random();
        double xoffset = r.nextInt(radius * 200)/100 - radius;
        double zoffset = r.nextInt(radius * 200)/100 - radius;
        while(Math.sqrt(Math.pow(xoffset, 2) + Math.pow(zoffset, 2)) > radius){ //make sure its an actual radius not a fucking box
            xoffset = (r.nextInt(radius * 200)/100) - radius;
            zoffset = (r.nextInt(radius * 200)/100) - radius;
        }
        return loc.add(xoffset, 0, zoffset);
    }

    public void addItem(Player p, ItemStack hand, int max){
        Altar.altarInstances.get(p).items.add(hand);
        p.getInventory().setItemInMainHand(null);
        p.sendMessage(ChatColor.GRAY + "+ " + hand.getItemMeta().getDisplayName() + ChatColor.GRAY + " (" + Altar.altarInstances.get(p).items.size() + "/" + max + ")");

        ItemStack is = new ItemStack(hand.getType(), 1);
        final ItemMeta meta = is.getItemMeta();
        meta.setLore(Arrays.asList("notarealitem"));
        is.setItemMeta(meta);
        Item i = location.getWorld().dropItem(randomOffset(location, 2), is);
        droppedItems.add(i);
        GlowAPI.setGlowing(i, Listeners.groupOf(hand));
    }
}

public class Altar implements Listener {

    public static Map<Player,AltarInstance> altarInstances = new HashMap<>();

    public void onEnable() {
        PracticeServer.log.info("[Altars] has been enabled.");
        Bukkit.getServer().getPluginManager().registerEvents(this, PracticeServer.plugin);
        altarInstances = new HashMap<>();
        new BukkitRunnable() {
            @Override
            public void run(){
                for(Player p : altarInstances.keySet()){
                    if(p.getLocation().distance(altarInstances.get(p).location) > 10){
                        altarInstances.get(p).cancelAltar();
                    }
                }
            }
        }.runTaskTimer(PracticeServer.plugin, 20, 50);
    }

    @EventHandler
    public void refundLogout(PlayerQuitEvent e){
        if(altarInstances.containsKey(e.getPlayer())){
            altarInstances.get(e.getPlayer()).cancelAltar();
        }
    }

    @EventHandler
    public void dropOnDeath(PlayerDeathEvent e){
        Player p = e.getEntity();
        if(altarInstances.containsKey(p)){
            if (Altar.altarInstances.get(p).items.size() > 0) {
                for (ItemStack item1 : Altar.altarInstances.get(p).items) {
                    p.getWorld().dropItem(p.getLocation(), item1);
                    p.sendMessage(ChatColor.RED + "- " + item1.getItemMeta().getDisplayName());
                }
                for(Item i: altarInstances.get(p).droppedItems) i.remove();
                Altar.altarInstances.remove(p);
                p.sendMessage(ChatColor.RED + ">> Altar has been cancelled!");
            }
        }
    }

    @EventHandler
    public void altarInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        Action a = e.getAction();
        Block b = e.getClickedBlock();
        if ((a == Action.RIGHT_CLICK_BLOCK) && (b.getType() == Material.ENCHANTMENT_TABLE)) {
            e.setCancelled(true);
            addArmor(p, b.getLocation());
        }
        if (a == Action.LEFT_CLICK_BLOCK && b.getType() == Material.ENCHANTMENT_TABLE && altarInstances.containsKey(p)) {
            e.setCancelled(true);
            altarInstances.get(p).cancelAltar();
        }
    }

    public void addArmor(Player p, Location loc) {
        if (p.getInventory().getItemInMainHand().hasItemMeta()) {
            ItemStack hand = p.getInventory().getItemInMainHand();
            List<String> lore = hand.getItemMeta().getLore();
            String rare = ChatColor.stripColor(lore.get(lore.size() - 1));
            int max = 4 - Items.getRarityFromItem(hand);
            Random r = new Random();

            int rarity = Items.getRarityFromItem(hand);
            if (rarity > 3) {
                p.sendMessage(ChatColor.YELLOW + ">> This item is too rare to be upgraded.");
                return;
            }
            if (!altarInstances.containsKey(p)
                    || altarInstances.get(p).items.size() <= (max - 1)) {
                if (!altarInstances.containsKey(p) || altarInstances.get(p).items.size() < 1) {
                    p.sendMessage(ChatColor.YELLOW + "Punch Altar to cancel at any time.");
                    altarInstances.put(p, new AltarInstance(new ArrayList<>(), loc, p));
                    altarInstances.get(p).addItem(p, hand, max);
                } else {
                    ItemStack item1 = altarInstances.get(p).items.get(0);
                    if ((Items.isWeapon(Items.getItemType(item1.getType()))) && (Items.isWeapon(Items.getItemType(hand.getType()))) || (Items.isArmour(Items.getItemType(item1.getType()))) && (Items.isArmour(Items.getItemType(hand.getType())))) {
                        if (Tiers.getStringTier(hand.getItemMeta().getDisplayName()) == Tiers.getStringTier(item1.getItemMeta().getDisplayName())) {
                            if (lore.get(lore.size() - 1).contains(item1.getItemMeta().getLore().get(item1.getItemMeta().getLore().size() - 1))) {
                                altarInstances.get(p).addItem(p, hand, max);
                                int rollChance = r.nextInt(100);                                if (altarInstances.get(p).items.size() == max) {
                                    p.sendMessage(ChatColor.GREEN + ">> Performing Altar");
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            p.getWorld().playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.25f);
                                            Particles.CRIT_MAGIC.display(0.0f, 0.0f, 0.0f, 0.5f, 20, altarInstances.get(p).location.add(0.0, 1.0, 0.0), 20.0);
                                        }
                                    }.runTaskLater(PracticeServer.plugin, 10L);
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            p.getWorld().playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.25f);
                                            Particles.CRIT_MAGIC.display(0.0f, 0.0f, 0.0f, 0.5f, 20, altarInstances.get(p).location.add(0.0, 1.0, 0.0), 20.0);
                                        }
                                    }.runTaskLater(PracticeServer.plugin, 20L);
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            p.getWorld().playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.25f);
                                            Particles.CRIT_MAGIC.display(0.0f, 0.0f, 0.0f, 0.5f, 20, altarInstances.get(p).location.add(0.0, 1.0, 0.0), 20.0);
                                        }
                                    }.runTaskLater(PracticeServer.plugin, 30L);
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            if (rollChance < 50) {
                                                altarInstances.get(p).generateItem();
                                            } else {
                                                p.sendMessage(ChatColor.RED + "You failed your Altar!");
                                            }
                                            for(Item i: altarInstances.get(p).droppedItems) i.remove();
                                            altarInstances.remove(p);
                                        }
                                    }.runTaskLater(PracticeServer.plugin, 40L);
                                }
                            } else {
                                p.sendMessage(ChatColor.YELLOW + ">> Must be the same rarity.");
                            }
                        } else {
                            p.sendMessage(ChatColor.YELLOW + ">> The items must be the same tier.");
                        }
                    } else {
                        p.sendMessage(ChatColor.YELLOW + ">> Item is neither armor, nor weapon");
                    }
                }
            }
        }

    }
}