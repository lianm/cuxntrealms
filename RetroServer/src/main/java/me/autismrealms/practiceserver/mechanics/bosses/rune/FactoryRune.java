package me.autismrealms.practiceserver.mechanics.bosses.rune;

import me.autismrealms.practiceserver.mechanics.bosses.BossRooms;
import me.autismrealms.practiceserver.mechanics.bosses.BossTypes;
import me.autismrealms.practiceserver.mechanics.item.ItemHandler;
import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class FactoryRune {

    public static ItemStack createRune(int tier) {
        ItemStack rune = new ItemStack(Material.NETHER_STAR);
        ItemMeta runeMeta = rune.getItemMeta();

        List<String> lore = new ArrayList<>();

        BossTypes boss =  BossTypes.getRandom(tier);

        BossRooms difficulty = BossRooms.getRandom();
        String hand = ItemHandler.getItem(boss.item);

        runeMeta.setDisplayName("" + Tiers.colors[tier-1] + boss.name);
        lore.add(ChatColor.RED + "STRENGTH: " + boss.strength + "%");
        lore.add(ChatColor.GRAY + "A boss rune, right click to battle.");
        lore.add("" + ChatColor.ITALIC + ChatColor.DARK_PURPLE + "Mythic");


        for (ItemFlag itemFlag : ItemFlag.values()) {
            runeMeta.addItemFlags(itemFlag);
        }

        runeMeta.setLore(lore);
        rune.setItemMeta(runeMeta);

        return rune;
    }

}