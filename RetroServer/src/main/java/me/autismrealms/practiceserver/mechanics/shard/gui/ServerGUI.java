package me.autismrealms.practiceserver.mechanics.shard.gui;

import me.autismrealms.practiceserver.apis.nbt.NBTAccessor;
import me.autismrealms.practiceserver.enums.ranks.RankEnum;
import me.autismrealms.practiceserver.mechanics.enchants.Enchants;
import me.autismrealms.practiceserver.mechanics.moderation.ModerationMechanics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Colorable;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by Giovanni on 10-6-2017.
 */
public class ServerGUI {




    public static Inventory serverGUI(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9, "RETRO REALMS - SERVERS");

        /*TEST*/
        ItemStack testServer = new ItemStack(Material.GOLD_BLOCK);
        ItemMeta testMeta = testServer.getItemMeta();
        testMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e&lDevelopment Server"));
        testMeta.setLore(Arrays.asList(
                "",
                ChatColor.GREEN + "• Access - PMOD+",
                ChatColor.WHITE + "Click here to load into",
                ChatColor.WHITE + "this server"));
        testMeta.addItemFlags(ItemFlag.values());
        testServer.setItemMeta(testMeta);
        NBTAccessor testAccessor = new NBTAccessor(testServer).check();
        testAccessor.setString("server", "dev");
        ItemStack finishedTest = testAccessor.update();
        finishedTest.addUnsafeEnchantment(Enchants.glow, 10);

        /*Retro*/
        ItemStack kitServer = new ItemStack(Material.END_CRYSTAL);
        ItemMeta kitMeta = kitServer.getItemMeta();
        kitMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e&lUS-1"));
        kitMeta.setLore(Arrays.asList(
                "",
                ChatColor.GREEN + "• Access - Everyone!",
                ChatColor.WHITE + "Click here to load into",
                ChatColor.WHITE + "this server"));
        kitMeta.addItemFlags(ItemFlag.values());
        kitServer.setItemMeta(kitMeta);
        NBTAccessor kitAccessor = new NBTAccessor(kitServer).check();
        kitAccessor.setString("server", "retro");
        ItemStack finishedKit = kitAccessor.update();
        finishedKit.addUnsafeEnchantment(Enchants.glow, 10);


        /*Monthly*/
        ItemStack P2WServer = new ItemStack(Material.END_CRYSTAL);
        ItemMeta P2WMeta = P2WServer.getItemMeta();
        P2WMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e&lUS-2"));
        P2WMeta.setLore(Arrays.asList(
                "",
                ChatColor.GREEN + "• Access - Everyone!",
                ChatColor.WHITE + "Click here to load into",
                ChatColor.WHITE + "this server"));
        P2WMeta.addItemFlags(ItemFlag.values());
        P2WServer.setItemMeta(P2WMeta);
        NBTAccessor P2WAccessor = new NBTAccessor(P2WServer).check();
        P2WAccessor.setString("server", "retro2");
        ItemStack finishedP2W = P2WAccessor.update();
        finishedP2W.addUnsafeEnchantment(Enchants.glow, 10);

        /*Build*/
        ItemStack buildServer = new ItemStack(Material.DIAMOND_BLOCK);
        ItemMeta buildMeta = buildServer.getItemMeta();
        buildMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e&lBuild Server"));
        buildMeta.setLore(Arrays.asList(
                "",
                ChatColor.GREEN + "• Access - Builders+",
                ChatColor.WHITE + "Click here to load into",
                ChatColor.WHITE + "this server"));
        buildMeta.addItemFlags(ItemFlag.values());
        buildServer.setItemMeta(buildMeta);
        NBTAccessor buildAccessor = new NBTAccessor(buildServer).check();
        buildAccessor.setString("server", "build");
        ItemStack finishedBuild = buildAccessor.update();
        finishedBuild.addUnsafeEnchantment(Enchants.glow, 10);

        inventory.setItem(3, finishedKit);
        inventory.setItem(5, finishedP2W);
        if(ModerationMechanics.isStaff(player) || ModerationMechanics.getRank(player) == RankEnum.BUILDER) {
            inventory.setItem(0, finishedTest);
            inventory.setItem(8, finishedBuild);
        }
        return inventory;

    }
}