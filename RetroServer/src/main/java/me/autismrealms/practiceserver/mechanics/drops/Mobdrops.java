package me.autismrealms.practiceserver.mechanics.drops;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.apis.itemapi.ItemAPI;
import me.autismrealms.practiceserver.mechanics.donations.Crates.CratesMain;
import me.autismrealms.practiceserver.mechanics.drops.generate.FactoryEliteItem;
import me.autismrealms.practiceserver.mechanics.mobs.Mobs;
import me.autismrealms.practiceserver.mechanics.mobs.elite.EliteType;
import me.autismrealms.practiceserver.mechanics.mobs.elite.SkeletonElite;
import me.autismrealms.practiceserver.mechanics.money.GemPouches;
import me.autismrealms.practiceserver.mechanics.money.Money;
import me.autismrealms.practiceserver.mechanics.player.Stats.StatsMain;
import me.autismrealms.practiceserver.mechanics.teleport.TeleportBooks;
import me.autismrealms.practiceserver.mechanics.item.Items;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.inventivetalent.glow.GlowAPI;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Mobdrops implements Listener {

	private ArrayList<LivingEntity> bugFix = new ArrayList<>();

	public void onEnable() {
		PracticeServer.log.info("[MobDrops] has been enabled.");
		Bukkit.getServer().getPluginManager().registerEvents(this, PracticeServer.plugin);
	}

	public void onDisable() {
		PracticeServer.log.info("[MobDrops] has been disabled.");
	}

	@EventHandler
	public void onMobDeath(final EntityDeathEvent e) {
		if (!(e.getEntity() instanceof Player)) {
			System.out.println("waker");
			e.getDrops().clear();
		}
		e.setDroppedExp(0);
	}

	@EventHandler
	public void mobDeathStatFix(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player && !(e.getEntity() instanceof Player)
				&& e.getEntity() instanceof LivingEntity) {
			Player d = (Player) e.getDamager();
			final LivingEntity s = (LivingEntity) e.getEntity();
			if (e.getDamage() >= s.getHealth()) {
				StatsMain.currentMonsterKills.put(d.getUniqueId(), StatsMain.getMonsterKills(d.getUniqueId()) + 1);

			}
		}
	}

	@EventHandler
	public void onMobDeath(final EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof LivingEntity && !(e.getEntity() instanceof Player)) {
			final LivingEntity s = (LivingEntity) e.getEntity();
			Entity d = e.getDamager();
			if (bugFix.contains(s)) {
				return;
			}
			if (e.getDamage() >= s.getHealth() && s.getEquipment().getItemInMainHand() != null
					&& s.getEquipment().getItemInMainHand().getType() != Material.AIR) {
				s.playEffect(EntityEffect.DEATH);
				s.remove();
				bugFix.add(s);

				if (s.hasMetadata("type")
						&& s.getMetadata("type").get(0).asString().equals("bossSkeleteonDungeonDAEMON")) {
					SkeletonElite.getNearbyPlayers(s).forEach(player -> {

						player.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 10F, 0.1F);
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7[ Daemon Lord defeated ]"));
					});

				}

				if (s.hasMetadata("type") && s.getMetadata("type").get(0).asString().equals("bossSkeletonDungeon")) {
					SkeletonElite.getNearbyPlayers(s).forEach(player -> {

						player.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 10F, 0.1F);
						player.sendMessage(ChatColor.DARK_RED + "The Infernal Abyss: My deathlord.. " + player.getName()
								+ ".. I'm coming for you.");
					});

					/*
					 * List<Player> currentNear =
					 * SkeletonElite.getNearbyPlayers(s); if
					 * (currentNear.isEmpty()) return;
					 * Bukkit.getScheduler().scheduleSyncDelayedTask(
					 * PracticeServer.getInstance(), () -> {
					 * currentNear.forEach(player -> { player.sendMessage("");
					 * player.sendMessage(ChatColor.DARK_RED +
					 * "The Infernal Abyss: My abyssal lords, come to me!");
					 * player.sendMessage(ChatColor.translateAlternateColorCodes
					 * ('&', "&7&oThe Infernal Abyss is coming closer.."));
					 * player.playSound(player.getLocation(),
					 * Sound.ENTITY_ENDERDRAGON_DEATH, 10F, 0.1F); });
					 * Arrays.stream(SkeletonElite.getMaltaiStrike()).forEach(
					 * location -> {
					 * location.getWorld().strikeLightning(location);
					 * location.getWorld().strikeLightningEffect(location); });
					 * Bukkit.getScheduler().scheduleSyncDelayedTask(
					 * PracticeServer.getInstance(), () -> {
					 * currentNear.forEach(player -> { player.sendMessage("");
					 * player.sendMessage(ChatColor.DARK_RED +
					 * "The Infernal Abyss: Daemon Lord Kilatan, Abyssal King Jayden.."
					 * ); player.playSound(player.getLocation(),
					 * Sound.BLOCK_PORTAL_TRIGGER, 10F, 1.5F); LivingEntity
					 * livingEntity1 =
					 * Spawners.getInstance().spawnAndReturn(SkeletonElite.
					 * getMaltaiStrike()[0], "kilatanB", 5, true);
					 * livingEntity1.setMaxHealth(80000);
					 * livingEntity1.setHealth(80000);
					 * livingEntity1.setCustomName(ChatColor.RED +
					 * "Daemon Lord Kilatan"); LivingEntity livingEntity2 =
					 * Spawners.getInstance().spawnAndReturn(SkeletonElite.
					 * getMaltaiStrike()[1], "jayden", 5, true);
					 * livingEntity2.setMaxHealth(80000);
					 * livingEntity2.setHealth(80000);
					 * livingEntity2.setCustomName(ChatColor.RED +
					 * "Abyssal King Jayden");
					 * livingEntity2.getEquipment().setHelmet(SkullTextures.
					 * DEMON_JAYDEN.getSkullByURL());
					 * livingEntity1.getEquipment().setHelmet(SkullTextures.
					 * DEMON_KILATAN.getSkullByURL());
					 * livingEntity2.setMetadata("improvedDropRate", new
					 * FixedMetadataValue(PracticeServer.getInstance(), 20));
					 * livingEntity1.setMetadata("improvedDropRate", new
					 * FixedMetadataValue(PracticeServer.getInstance(), 20));
					 * });
					 * Arrays.stream(SkeletonElite.getMaltaiStrike()).forEach(
					 * location -> {
					 * location.getWorld().strikeLightning(location);
					 * location.getWorld().strikeLightningEffect(location); });
					 * }, 20 * 3); }, 20 * 5);
					 */
				}
				if (s.hasMetadata("type") && s.getMetadata("type").get(0).asString().equals("turkey")) {
					Random rand = new Random();
					int thanks = rand.nextInt(20);
					int item = rand.nextInt(6) + 2;
					int tier = Integer.valueOf(s.getMetadata("tier").get(0).asString());
					ItemStack is = null;
					if (thanks >= 25)
						is = Items.orb(false);
					else if (thanks >= 15)
						is = CreateDrop.createDrop(tier, item, 0);
					else if (thanks >= 9)
						is = CreateDrop.createDrop(tier, item, 1);
					else if (thanks >= 6)
						is = CreateDrop.createDrop(tier, item, 2);
					else if (thanks >= 4)
						is = CreateDrop.createDrop(tier, item, 3);
					else if (thanks >= 2) {
						is = tier == 5 ? Items.legendaryOrb(false) : Items.orb(false);
						is.setAmount(3);
					} else if (thanks < 2)
					
						is = ItemAPI.getScrollGenerator().next(tier - 1);
					if (is != null) {
						DropPriority.DropItem(d, s, s.getLocation(), is);
					} else {
						Bukkit.broadcastMessage("Null case: " + Integer.toString(thanks));
					}
				}
				final Random random = new Random();
				final int gems = random.nextInt(2) + 1;
				int gemamt = 0;
				final int scrolldrop = random.nextInt(100);
				final int sackdrop = random.nextInt(100);
				boolean dodrop = false;
				boolean elite = false;
				int rd = random.nextInt(250);
				final int cratedrop = random.nextInt(50);
				final int tsix = random.nextInt(150);
				final int randomMaskChance = 1000;// ThreadLocalRandom.current().nextInt(100);
				final int randomCandyChance = ThreadLocalRandom.current().nextInt(85);
				int eliteScrollDrop = random.nextInt(300);

				int scrollTier = 0;
				if (PracticeServer.DOUBLE_DROP_RATE) {
					rd /= 2;
				}
				int improvementRate = PracticeServer.buffHandler().isActive()
						? PracticeServer.buffHandler().getActiveBuff().getUpdate() : -1;

				if (improvementRate > 0)
					rd /= (1.0 + (improvementRate / 100));

				if (s.hasMetadata("improvedDropRate")) {
					int amount = s.getMetadata("improvedDropRate").get(0).asInt();

					rd /= (1.0 + (amount / 100));
				}
				if (improvementRate > 0)
					eliteScrollDrop /= (1.0 + (eliteScrollDrop / 100));

				if (s.getEquipment().getItemInMainHand().getItemMeta().hasEnchants()) {
					elite = true;
				}
				if (s.getEquipment().getItemInMainHand().getType().name().contains("WOOD_")) {
					gemamt = random.nextInt(3) + 3;
					if (elite && !this.isCustomNamedElite(s)) {
						if (rd < 20) {
							dodrop = true;
						}
					} else if (elite && this.isCustomNamedElite(s)) {
						if (rd < 20) {
							dodrop = true;
						}
					} else if (rd < 150) {
						dodrop = true;
					}
					if (scrolldrop <= 5) {
						final int scrolltype = random.nextInt(2);
						if (scrolltype == 0) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.deadpeaks_book(false));
						}
						if (scrolltype == 1) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.tripoli_book(false));
						}
					}
					if (sackdrop <= 5) {
						DropPriority.DropItem(d, s, s.getLocation(), GemPouches.gemPouch(1));
					}
					if (randomMaskChance == 0 && s.getEquipment().getHelmet().getType() == Material.JACK_O_LANTERN) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenHat());
					}
					if (randomCandyChance <= 1) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenCandy());
					}
					if (cratedrop < 10) {
						DropPriority.DropItem(d, s, s.getLocation(), CratesMain.createCrate(1, false));
					}

				}
				if (s.getEquipment().getItemInMainHand().getType().name().contains("STONE_")) {
					gemamt = random.nextInt(5) + 5;
					if (elite && !this.isCustomNamedElite(s)) {
						if (rd < 200) {
							dodrop = true;
						}
					} else if (elite && this.isCustomNamedElite(s)) {
						if (rd < 75) {
							dodrop = true;
						}
					} else if (rd < 100) {
						dodrop = true;
					}
					if (scrolldrop <= 5) {
						final int scrolltype = random.nextInt(2);
						if (scrolltype == 0) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.deadpeaks_book(false));
						}
						if (scrolltype == 1) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.avalonBook(false));
						}
						if (scrolltype == 2) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.tripoli_book(false));
						}
					}
					if (sackdrop <= 3) {
						DropPriority.DropItem(d, s, s.getLocation(), GemPouches.gemPouch(2));
					}
					if (randomMaskChance <= 1 && s.getEquipment().getHelmet().getType() == Material.JACK_O_LANTERN) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenHat());
					}
					if (randomCandyChance <= 2) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenCandy());
					}
					if (cratedrop < 7) {
						DropPriority.DropItem(d, s, s.getLocation(), CratesMain.createCrate(2, false));
					}
				}
				if (s.getEquipment().getItemInMainHand().getType().name().contains("IRON_")) {
					gemamt = random.nextInt(10) + 10;
					if (elite && !this.isCustomNamedElite(s)) {
						if (rd < 150) {
							dodrop = true;
						}
					} else if (elite && this.isCustomNamedElite(s)) {
						if (rd < 70) {
							dodrop = true;
						}
					} else if (rd < 75) {
						dodrop = true;
					}
					if (scrolldrop <= 5) {
						final int scrolltype = random.nextInt(5);
						if (scrolltype == 0) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.tripoli_book(false));
						}
						if (scrolltype == 1) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.avalonBook(false));
						}
					}
					if (sackdrop <= 5) {
						DropPriority.DropItem(d, s, s.getLocation(), GemPouches.gemPouch(3));
					}
					if (randomMaskChance <= 2 && s.getEquipment().getHelmet().getType() == Material.JACK_O_LANTERN) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenHat());
					}
					if (randomCandyChance <= 8) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenCandy());
					}
					if (cratedrop < 5) {
						DropPriority.DropItem(d, s, s.getLocation(), CratesMain.createCrate(3, false));
					}
				}
				if (s.getEquipment().getItemInMainHand().getType().name().contains("DIAMOND_")
						&& !isBlueLeather(s.getEquipment().getArmorContents()[0])) {
					gemamt = random.nextInt(30) + 24;
					if (elite && !this.isCustomNamedElite(s)) {
						if (rd < 100) {
							dodrop = true;
						}
					} else if (elite && this.isCustomNamedElite(s)) {
						if (rd < 40) {
							dodrop = true;
						}
					} else if (rd < 50) {
						dodrop = true;
					}
					if (scrolldrop <= 9) {
						final int scrolltype = random.nextInt(3);
						if (scrolltype == 0) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.deadpeaks_book(false));
						}
						if (scrolltype == 1) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.avalonBook(false));
						}
						if (scrolltype == 2) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.tripoli_book(false));
						}
					}
					if (sackdrop <= 5) {
						DropPriority.DropItem(d, s, s.getLocation(), GemPouches.gemPouch(4));
					}
					if (randomMaskChance <= 3 && s.getEquipment().getHelmet().getType() == Material.JACK_O_LANTERN) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenHat());
					}
					if (randomCandyChance <= 8) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenCandy());
					}
					if (cratedrop < 3) {
						DropPriority.DropItem(d, s, s.getLocation(), CratesMain.createCrate(4, false));
					}
				}
				if (s.getEquipment().getItemInMainHand().getType().name().contains("GOLD_")) {
					gemamt = random.nextInt(50) + 14;
					if (elite && !this.isCustomNamedElite(s)) {
						if (rd < 60) {
							dodrop = true;
						}
					} else if (elite && this.isCustomNamedElite(s)) {
						if (rd < 30) {
							dodrop = true;
						}
					} else if (rd < 30) {
						dodrop = true;
					}
					if (scrolldrop <= 5) {
						final int scrolltype = random.nextInt(2);
						if (scrolltype == 0) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.deadpeaks_book(false));
						}
						if (scrolltype == 1) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.avalonBook(false));
						}
					}
					if (sackdrop <= 5) {
						DropPriority.DropItem(d, s, s.getLocation(), GemPouches.gemPouch(4));
					}
					if (tsix <= 2 && elite) {
						DropPriority.DropItem(d, s, s.getLocation(), GemPouches.gemPouch(6));
					}
					if (randomMaskChance <= 4 && s.getEquipment().getHelmet().getType() == Material.JACK_O_LANTERN) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenHat());
					}
					if (randomCandyChance <= 14) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenCandy());
					}
					if (cratedrop < 1) {
						DropPriority.DropItem(d, s, s.getLocation(), CratesMain.createCrate(5, false));
					}
				}
				if (s.getEquipment().getItemInMainHand().getType().name().contains("DIAMOND_")
						&& isBlueLeather(s.getEquipment().getArmorContents()[0])) {
					gemamt = random.nextInt(70) + 25;
					if (elite && !this.isCustomNamedElite(s)) {
						if (rd < 24) {
							dodrop = true;
						}
					} else if (elite && this.isCustomNamedElite(s)) {
						if (rd < 15) {
							dodrop = true;
						}
					} else if (rd < 8) {
						dodrop = true;
					} else if (Mobs.isGolemBoss(s) && rd < 30) {
						dodrop = true;
					}
					if (scrolldrop <= 5) {
						final int scrolltype = random.nextInt(2);
						if (scrolltype == 0) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.frosthold_book(false));
						}
						if (scrolltype == 1) {
							DropPriority.DropItem(d, s, s.getLocation(), TeleportBooks.avalonBook(false));
						}
					}
					if (sackdrop <= 5) {
						DropPriority.DropItem(d, s, s.getLocation(), GemPouches.gemPouch(4));
					}
					if (tsix <= 4 && elite) {
						DropPriority.DropItem(d, s, s.getLocation(), GemPouches.gemPouch(6));
					}
					if (randomMaskChance <= 4 && s.getEquipment().getHelmet().getType() == Material.JACK_O_LANTERN) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenHat());
					}
					if (randomCandyChance <= 18) {
						DropPriority.DropItem(d, s, s.getLocation(),
								PracticeServer.getManagerHandler().getHalloween().halloweenCandy());
					}
					if (cratedrop < 1) {
						DropPriority.DropItem(d, s, s.getLocation(), CratesMain.createCrate(6, false));
					}
				}
				if (gems == 1) {
					if (gemamt > 0) {
						ItemStack itemStack = Money.makeGems(1);

						itemStack.setAmount(gemamt);
						// Prevent potential gems stuck in ground, higher the Y
						// axis by 1
						DropPriority.DropItem(d, s, s.getLocation().add(0, 1, 0), itemStack);
						DropPriority.DropItem(d, s, s.getLocation().add(0, 1, 0), itemStack);
						DropPriority.DropItem(d, s, s.getLocation().add(0, 1, 0), itemStack);

					}
				}
				if (elite && eliteScrollDrop <= 10) {
					scrollTier = Mobs.getMobTier(s) -1;

					ItemStack scroll = ItemAPI.getScrollGenerator().next(scrollTier);
					Item item = DropPriority.DropItem(d, s, s.getLocation(), scroll);

					me.autismrealms.practiceserver.utils.GlowAPI.setGlowing(item, GlowAPI.Color.YELLOW);
				}
				if (dodrop) {

					if (PracticeServer.buffHandler().isActive())
						PracticeServer.buffHandler().updateImprovedDrops();

					if (!this.isCustomNamedElite(s)) {
						final ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
						ItemStack[] armorContents;
						for (int length = (armorContents = s.getEquipment()
								.getArmorContents()).length, i = 0; i < length; ++i) {
							final ItemStack is = armorContents[i];
							if (is != null && is.getType() != Material.AIR && is.hasItemMeta()
									&& is.getItemMeta().hasLore()) {
								drops.add(is);
								drops.add(s.getEquipment().getItemInMainHand());
							}
						}
						int piece = 0;
						if (drops.size() > 1)
							piece = random.nextInt(drops.size());
						final ItemStack is2 = drops.get(piece);
						if (is2.getItemMeta().hasEnchants()
								&& is2.getItemMeta().hasEnchant(Enchantment.LOOT_BONUS_MOBS)) {
							is2.removeEnchantment(Enchantment.LOOT_BONUS_MOBS);
						}
						short dura = (short) -1;
						if (dura == 0) {
							dura = 1;
						}
						if (dura == is2.getType().getMaxDurability()) {
							dura = (short) -1;
						}
						is2.setDurability((short) 0);
						if (is2.getType() == Material.JACK_O_LANTERN)
							return;
						Item item = DropPriority.DropItem(d, s, s.getLocation(), is2);
						me.autismrealms.practiceserver.utils.GlowAPI.setGlowing(item, groupOf(item.getItemStack()));
					} else if (s.hasMetadata("type")) {
                        final String type = s.getMetadata("type").get(0).asString();
                        ItemStack is = FactoryEliteItem.setDrop(type);
                        if (is.getType() == Material.JACK_O_LANTERN)
                            return;
                        Item itemDrop = DropPriority.DropItem(d, s, s.getLocation(), is);

                        me.autismrealms.practiceserver.utils.GlowAPI.setGlowing(itemDrop, groupOf(itemDrop.getItemStack()));
                    }
                }
            }
        }
    }


	private GlowAPI.Color groupOf(ItemStack itemStack) {
		for (String string : itemStack.getItemMeta().getLore()) {
			if (string.contains("Common")) {
				return GlowAPI.Color.WHITE;
			} else if (string.contains("Uncommon")) {
				return GlowAPI.Color.GREEN;
			} else if (string.contains("Rare")) {
				return GlowAPI.Color.AQUA;
			} else if (string.contains("Unique")) {
				return GlowAPI.Color.YELLOW;
			}
		}

		return GlowAPI.Color.WHITE;
	}

	boolean isCustomNamedElite(final LivingEntity l) {
		if (l.hasMetadata("type")) {
			final String type = l.getMetadata("type").get(0).asString();
			for (EliteType elite : EliteType.values()) {
				if (type.equalsIgnoreCase(elite.id)) {
					return true;
				}
			}
		}
		return false;
	}

	PlayerCommandPreprocessEvent event;

	public static ItemStack setItemBlueLeather(ItemStack itemStack) {
		LeatherArmorMeta leather = (LeatherArmorMeta) itemStack.getItemMeta();
		leather.setColor(Color.fromRGB(40, 40, 240));
		itemStack.setItemMeta(leather);
		return itemStack;
	}

	public static boolean isBlueLeather(ItemStack itemStack) {
		if (itemStack != null && itemStack.getType().name().contains("LEATHER_")) {
			LeatherArmorMeta leather = (LeatherArmorMeta) itemStack.getItemMeta();
			if (leather.getColor().toString().equals(Color.fromRGB(40, 40, 240).toString())) {
				return true;
			}
		}
		return false;
	}
}