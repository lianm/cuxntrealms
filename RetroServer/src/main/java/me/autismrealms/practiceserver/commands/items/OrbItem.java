package me.autismrealms.practiceserver.commands.items;

import me.autismrealms.practiceserver.mechanics.drops.generate.GenerateOrb;
import me.autismrealms.practiceserver.mechanics.money.Money;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class OrbItem {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;
        if(Money.hasEnoughGems(p, 500)){
            p.sendMessage(ChatColor.RED + "-500" + ChatColor.BOLD + "G");
            Money.takeGems(p, 500);
            GenerateOrb.orbItem(p.getItemInHand(), false);
        }

        return false;
    }
}
