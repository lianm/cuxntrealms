package me.autismrealms.practiceserver.mechanics.mobs;

import java.util.ArrayList;
import java.util.Arrays;

public enum MobType {

    CaveSpider("Spider", new ArrayList<>(Arrays.asList("Harmless", "Wild", "Fierce", "Dangerous", "Lethal", "Devastating"))),
    Skeleton("Skeleton", new ArrayList<>(Arrays.asList("Broken", "Wandering", "Demonic", "Guardian", "Infernal", "Frozen"))),
    Zombie("Zombie", new ArrayList<>(Arrays.asList("Rotting", "Savaged", "Greater", "Demonic", "Infernal", "Frozen"))),
    MagmaCube("Cube", new ArrayList<>(Arrays.asList("Weak Magma", "Bubbling Magma", "Unstable Magma", "Boiling Magma", "Unstoppable Magma", "Ice"))),
    Golem("Golem", new ArrayList<>(Arrays.asList("Broken", "Rusty", "Restored", "Mountain", "Powerful", "Devastating"))),
    SilverFish("SilverFish", new ArrayList<>(Arrays.asList("Weak", "Pointy", "Unstable", "Mean", "Rude", "Ice-Cold"))),
    Daemon("Daemon", new ArrayList<>(Arrays.asList("Broken", "Wandering Cracking", "Demonic", "Guardian", "Infernal", "Chilled"))),
    Imp("Imp", new ArrayList<>(Arrays.asList("Ugly", "Angry", "Warrior", "Armoured", "Infernal", "Arctic"))),
    WitherSkeleton("Chaos Skeleton", new ArrayList<>(Arrays.asList("Broken", "Wandering", "Demonic", "Dangerous", "Infernal", "Frozen"))),
    Spider("Spider", new ArrayList<>(Arrays.asList("Harmless", "Wild", "Fierce", "Dangerous", "Lethal", "Devastating")));

    public String suffix;
    public ArrayList<String> names;
    public String id;

    MobType(String suffix, ArrayList names){
        this.id = this.toString().toLowerCase();
        this.suffix = suffix;
        this.names = names;
    }

    public static String getName(MobType mobType){
        String name = "";
        switch (mobType){
            case WitherSkeleton: name = "Dark";
                break;
            case Skeleton: name = "Demonic";
                break;
            case Zombie: name = "Rotten";
                break;
            case Spider: name = "Delicate";
                break;
            case Golem: name = "Hard";
                break;
            case CaveSpider: name = "Delicate";
                break;
            case Imp: name = "Magic";
                break;
            case Daemon: name = "Strong";
                break;
            case MagmaCube: name = "Fiery";
                break;
            case SilverFish: name = "Creepy";
                break;
        }
        return name;
    }

}
