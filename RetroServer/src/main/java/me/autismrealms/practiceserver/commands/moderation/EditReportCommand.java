package me.autismrealms.practiceserver.commands.moderation;

import me.autismrealms.practiceserver.mechanics.moderation.Reports;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class EditReportCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;

                if (args.length < 2) {
                    p.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Incorrect Syntax." + ChatColor.GRAY + " /editreport <id> <bug description>");
                } else try {
                    int i = Integer.parseInt(args[0]);
                    Reports.Bug b = Reports.getInstance().getBug(i);
                    if (!b.player.equals(p))
                        p.sendMessage(ChatColor.RED + "You cannot edit another user's bug report.");
                    else if (b.accepted)
                        p.sendMessage(ChatColor.RED + "You cannot edit a bug report that is already accepted.");
                    else {
                        String s = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                        b.report = s;
                        p.sendMessage(ChatColor.YELLOW + "Your bug report (#" + i + ") has been edited.\n" + ChatColor.GRAY + s);
                    }
                } catch (NumberFormatException e) {
                    p.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Incorrect Syntax." + ChatColor.GRAY + " /editreport <id> <bug description>");
                }
            }
        return true;
    }
}
