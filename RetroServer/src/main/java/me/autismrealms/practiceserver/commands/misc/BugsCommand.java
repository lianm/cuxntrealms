package me.autismrealms.practiceserver.commands.misc;

import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.moderation.ModerationMechanics;
import me.autismrealms.practiceserver.mechanics.moderation.Reports;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class BugsCommand implements CommandExecutor {
    public static final int PAGE_SIZE = 6;

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;

        Player player = (Player) sender;

        if (ModerationMechanics.isStaff(player) || player.isOp()) {
            if (Reports.bugs.isEmpty())
                player.sendMessage(ChatColor.RED + "There are no more bug reports.");
            else if (args.length == 0)
                sendPage(player, 0);
            else try {
                    if (args.length == 1) {
                        int page = Integer.parseInt(args[0]);
                        if (page > getPages() || page <= 0)
                            player.sendMessage(ChatColor.RED + "Invalid page.");
                        else
                            sendPage(player, page - 1);
                    } else {
                        Reports.Bug b = Reports.getInstance().getBug(Integer.parseInt(args[1]));
                        if (b == null)
                            player.sendMessage(ChatColor.RED + "Invalid bug ID.");
                        else {
                            if (args[0].equalsIgnoreCase("check")) {
                                player.sendMessage(ChatColor.YELLOW.toString() + ChatColor.BOLD
                                        + "Report #" + b.id + ":"
                                        + "\n" + ChatColor.WHITE + "Submitted by " + b.player.getName()
                                        + (b.accepted ? ChatColor.GREEN + " (Accepted)" : ChatColor.GRAY + " (Pending)")
                                        + "\n" + ChatColor.GRAY + b.report);
                            } else if (args[0].equalsIgnoreCase("decline")) {
                                if (b.accepted)
                                    player.sendMessage(ChatColor.RED + "That bug has already been accepted.");
                                else {
                                    decline(b);
                                    player.sendMessage(ChatColor.YELLOW + "Report #" + b.id + " has been declined.");
                                }
                            } else if (args[0].equalsIgnoreCase("accept")) {
                                if (b.accepted)
                                    player.sendMessage(ChatColor.RED + "That bug has already been accepted.");
                                else {
                                    accept(b, args.length < 3 ? 3 : Integer.parseInt(args[2]));
                                    player.sendMessage(ChatColor.YELLOW + "Report #" + b.id + " has been accepted.");
                                }
                            } else if (args[0].equalsIgnoreCase("dupe")) {
                                if (b.accepted)
                                    player.sendMessage(ChatColor.RED + "That bug has already been accepted.");
                                else {
                                    dupe(b);
                                    player.sendMessage(ChatColor.RED + "Report #" + b.id + " has been declined for duplication.");
                                }
                            } else if (args[0].equalsIgnoreCase("remove")) {
                                Reports.bugs.remove(b);
                                player.sendMessage(ChatColor.RED + "Report #" + b.id + " has been removed.");
                            }
                        }
                    }
                } catch (NumberFormatException e) {
                    player.sendMessage(ChatColor.RED + "Invalid input.");
                }
        }

        return true;
    }

    private void accept(Reports.Bug b, int o) {
        b.accepted = true;
        if (b.player.isOnline()) {
            b.player.getPlayer().sendMessage(ChatColor.GREEN
                    + "Your report (#" + b.id + ") has been approved!" +
                    "\n" + ChatColor.GRAY + "You have been given " + ChatColor.GOLD + ChatColor.BOLD
                    + o + " ORBS" + ChatColor.GRAY + " for this report.");
            if (o >= 1) {
                ItemStack orbs = Items.orb(false);
                orbs.setAmount(o);
                b.player.getPlayer().getInventory().addItem(orbs);
            }
        }
    }

    private void dupe(Reports.Bug b) {
        if (b.player.isOnline())
            b.player.getPlayer().sendMessage(ChatColor.RED
                    + "Your report (#" + b.id + ") has been declined as it is the same as another report.");
        Reports.bugs.remove(b);
    }

    private void decline(Reports.Bug b) {
        if (b.player.isOnline())
            b.player.getPlayer().sendMessage(ChatColor.RED + "Your report (#" + b.id + ") has been declined.");
        Reports.bugs.remove(b);
    }

    private void sendPage(Player player, int page) {
        player.sendMessage(new String[]{
                ChatColor.YELLOW.toString() + ChatColor.BOLD + "BUG REPORTS"
                        + ChatColor.YELLOW + " (" + (page + 1) + "/" + getPages() + ") "
                        + ChatColor.GRAY + "[" + (getBottom(page) + 1) + " - " + getTop(page) + "]",
                getPage(page).stream().map(b -> (b.accepted ? ChatColor.GREEN : ChatColor.GRAY) + "#" + b.id
                        + " - " + ChatColor.YELLOW + b.player.getName() + ": " + ChatColor.WHITE + b.report)
                        .collect(Collectors.joining("\n"))
        });
    }

    private ArrayList<Reports.Bug> getPage(int page) {
        if (page >= getPages()) return new ArrayList<>();
        return new ArrayList<>(Reports.bugs.subList(getBottom(page), getTop(page)));
    }

    private int getBottom(int page) {
        return page * PAGE_SIZE;
    }

    private int getTop(int page) {
        return Math.min(Reports.bugs.size(), getBottom(page) + PAGE_SIZE);
    }

    private int getPages() {
        return (int) Math.ceil((float) Reports.bugs.size() / (float) PAGE_SIZE);
    }
}
