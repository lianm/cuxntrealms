package me.autismrealms.practiceserver.mechanics.progress;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.mobs.elite.EliteType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.File;
import java.io.IOException;

public class ProgressionConfig implements Listener {

    @EventHandler
    public void setupConfigOnJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (!p.hasPlayedBefore()) {
            for(int i = 1; i < 5; i++) {
                for(EliteType elite : EliteType.values()) {
                    if (i == elite.tier) {
                        get().set(p.getUniqueId() + ".tier" + i + "." + elite.id, false);
                    }
                }
            }
            save();
        }

    }

    static FileConfiguration gameFile;
    static File gpfile;

    public static void setup() {
        gpfile = new File(PracticeServer.plugin.getDataFolder(), "progress.yml");

        if (!gpfile.exists()) {
            try {
                gpfile.createNewFile();
            } catch (IOException e) {
            }
        }

        gameFile = YamlConfiguration.loadConfiguration(gpfile);
    }

    public static FileConfiguration get() {
        return gameFile;
    }

    public static void save() {
        try {
            gameFile.save(gpfile);
        } catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save idler.yml!");
        }
    }

    public static void reload() {
        gameFile = YamlConfiguration.loadConfiguration(gpfile);
    }

}

