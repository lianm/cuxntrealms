package me.autismrealms.practiceserver.commands.moderation;

import me.autismrealms.practiceserver.mechanics.moderation.Reports;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReportCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
                if (args.length < 1) {
                    p.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Incorrect Syntax." + ChatColor.GRAY + " /report <bug description>");
                } else {
                    String s = String.join(" ", args);
                    if (s.length() < 5)
                        p.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Not Enough Characters." + ChatColor.GRAY + " Your bug report must have a minimum of 5 characters.");
                    else {
                        Reports.Bug b = new Reports.Bug(p, s);
                        Reports.bugs.add(b);
                        p.sendMessage(ChatColor.YELLOW + "Your bug report (#" + b.id + ") has been submitted.\n" + ChatColor.GRAY + s);
                    }
                }
            }
        return false;
    }
}
