/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.Sound
 *  org.bukkit.World
 *  org.bukkit.entity.Snowball
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.LargeFireball
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.Projectile
 *  org.bukkit.entity.SmallFireball
 *  org.bukkit.entity.Snowball
 *  org.bukkit.entity.WitherSkull
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.block.BlockIgniteEvent
 *  org.bukkit.event.block.BlockIgniteEvent$IgniteCause
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.entity.EntityExplodeEvent
 *  org.bukkit.event.entity.ExplosionPrimeEvent
 *  org.bukkit.event.entity.ProjectileHitEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerTeleportEvent
 *  org.bukkit.event.player.PlayerTeleportEvent$TeleportCause
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.PluginManager
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 *  org.bukkit.projectiles.ProjectileSource
 *  org.bukkit.util.Vector
 */
package me.autismrealms.practiceserver.mechanics.damage;

import com.google.common.collect.Lists;
import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.player.Energy;
import me.autismrealms.practiceserver.mechanics.pvp.Alignments;
import me.autismrealms.practiceserver.utils.Particles;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class Staffs
        implements Listener {
    public static HashMap<Projectile, ItemStack> shots = new HashMap<Projectile, ItemStack>();
    public static HashMap<Player, ItemStack> staff = new HashMap<Player, ItemStack>();
    public static List<UUID> canShoot = Collections.synchronizedList(Lists.newArrayList());
    public PracticeServer m;

    public void onEnable() {
        PracticeServer.log.info("[Staffs] has been enabled.");
        Bukkit.getServer().getPluginManager().registerEvents(this, PracticeServer.plugin);

        new BukkitRunnable() {
            public void run(){
                for(Projectile shot : shots.keySet()){
                    if(shot.getTicksLived() > 5){
                        shot.remove();
                    }
                }
            }
        }.runTaskTimer(PracticeServer.plugin, 1, 1);
    }

    public void onDisable() {
        PracticeServer.log.info("[Staffs] has been disabled.");
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.EGG) {
            event.setCancelled(true);
        }
    }

    private final int STAFF_DELAY = 4;


    @EventHandler
    public void onStaffShot(PlayerInteractEvent e) {
        Player p;
        if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) && (p = e.getPlayer()).getInventory().getItemInMainHand() != null && p.getInventory().getItemInMainHand().getType() != Material.AIR && p.getInventory().getItemInMainHand().getType().name().contains("_HOE") && p.getInventory().getItemInMainHand().getItemMeta().hasLore()) {
            if (Alignments.isSafeZone(p.getLocation())) {
                p.playSound(p.getLocation(), Sound.BLOCK_LAVA_EXTINGUISH, 1.0f, 1.25f);
                Particles.CRIT_MAGIC.display(0.0f, 0.0f, 0.0f, 0.5f, 20, p.getLocation().add(0.0, 1.0, 0.0), 20.0);
            } else {
                if (Energy.nodamage.containsKey(p.getName()) && System.currentTimeMillis() - Energy.nodamage.get(p.getName()) < 100) {
                    e.setCancelled(true);
                    return;
                }
                if (canShoot.contains(p.getUniqueId())) {
                    e.setCancelled(true);
                    return;
                }
                if (Energy.getEnergy(p) > 0.0f) {
                    int amt = 0;
                    Projectile ep = null;
                    if (p.getInventory().getItemInMainHand().getType() == Material.WOOD_HOE) {
                        ep = p.launchProjectile(Snowball.class);
                        ep.setVelocity(p.getLocation().getDirection());
                        ep.setVelocity(ep.getVelocity().multiply(1));
                        this.shots.put(ep, p.getInventory().getItemInMainHand());
                        amt = 7;
                        canShoot.add(p.getUniqueId());
                        new BukkitRunnable() {

                            public void run() {
                                canShoot.remove(p.getUniqueId());
                            }
                        }.runTaskLaterAsynchronously(PracticeServer.plugin, STAFF_DELAY);
                    }
                    if (p.getInventory().getItemInMainHand().getType() == Material.STONE_HOE) {
                        ep = p.launchProjectile(Snowball.class);
                        ep.setVelocity(p.getLocation().getDirection());
                        ep.setVelocity(ep.getVelocity().multiply(1.2));
                        ep.setBounce(false);
                        this.shots.put(ep, p.getInventory().getItemInMainHand());
                        amt = 8;
                        canShoot.add(p.getUniqueId());
                        new BukkitRunnable() {

                            public void run() {
                                canShoot.remove(p.getUniqueId());
                            }
                        }.runTaskLaterAsynchronously(PracticeServer.plugin, STAFF_DELAY);
                    }
                    if (p.getInventory().getItemInMainHand().getType() == Material.IRON_HOE) {
                        ep = p.launchProjectile(Snowball.class);
                        ep.setVelocity(p.getLocation().getDirection());
                        ep.setVelocity(ep.getVelocity().multiply(1.4));
                        this.shots.put(ep, p.getInventory().getItemInMainHand());
                        amt = 9;
                        canShoot.add(p.getUniqueId());
                        new BukkitRunnable() {

                            public void run() {
                                canShoot.remove(p.getUniqueId());
                            }
                        }.runTaskLaterAsynchronously(PracticeServer.plugin, STAFF_DELAY);
                    }
                    if (p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_HOE) {
                        ep = p.launchProjectile(Snowball.class);
                        ep.setVelocity(p.getLocation().getDirection());
                        ep.setVelocity(ep.getVelocity().multiply(1.75   ));
                        this.shots.put(ep, p.getInventory().getItemInMainHand());
                        amt = 10;
                        canShoot.add(p.getUniqueId());
                        new BukkitRunnable() {

                            public void run() {
                                canShoot.remove(p.getUniqueId());
                            }
                        }.runTaskLaterAsynchronously(PracticeServer.plugin, STAFF_DELAY);
                    }
                    if (p.getInventory().getItemInMainHand().getType() == Material.GOLD_HOE) {
                        ep = p.launchProjectile(Snowball.class);
                        ep.setVelocity(p.getLocation().getDirection());
                        ep.setVelocity(ep.getVelocity().multiply(1.75));
                        this.shots.put(ep, p.getInventory().getItemInMainHand());
                        amt = 11;
                        canShoot.add(p.getUniqueId());
                        new BukkitRunnable() {

                            public void run() {
                                canShoot.remove(p.getUniqueId());
                            }
                        }.runTaskLaterAsynchronously(PracticeServer.plugin, STAFF_DELAY);
                    }
                    p.getInventory().getItemInMainHand().setDurability((short) 0);
                    Energy.removeEnergy(p, amt);
                    p.playSound(p.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1.0f, 0.25f);
                    this.shots.put(ep, p.getInventory().getItemInMainHand());
                } else {
                    Energy.setEnergy(p, 0.0f);
                    Energy.cd.put(p.getName(), System.currentTimeMillis());
                    p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 40, 5), true);
                    p.playSound(p.getLocation(), Sound.ENTITY_WOLF_PANT, 10.0f, 1.5f);
                }
            }
        }
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockIgnite(BlockIgniteEvent e) {
        if (e.getCause() == BlockIgniteEvent.IgniteCause.FIREBALL) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityExplodePrimeEvent(ExplosionPrimeEvent e) {
        e.setFire(false);
        e.setRadius(0.0f);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityExplodeEvent(EntityExplodeEvent e) {
        e.setCancelled(true);
        e.setYield(0.0f);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerTp(PlayerTeleportEvent e) {
        if (e.getCause().equals(PlayerTeleportEvent.TeleportCause.ENDER_PEARL)) {
            e.setCancelled(true);
        }
    }
}

