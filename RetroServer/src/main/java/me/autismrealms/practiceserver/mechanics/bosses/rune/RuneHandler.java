package me.autismrealms.practiceserver.mechanics.bosses.rune;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.bosses.BossHandler;
import me.autismrealms.practiceserver.mechanics.bosses.BossRooms;
import me.autismrealms.practiceserver.mechanics.bosses.BossTypes;
import me.autismrealms.practiceserver.mechanics.drops.generate.FactoryBossDrop;
import me.autismrealms.practiceserver.mechanics.player.Mounts.Horses;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class RuneHandler implements Listener {

    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, PracticeServer.plugin);
    }

    public void inDisable(){

    }

    public static boolean isRune(ItemStack rune){
        for(BossTypes boss : BossTypes.values()) {
            if (ChatColor.stripColor(rune.getItemMeta().getDisplayName()).equalsIgnoreCase(boss.name)) {
                return true;
            }
        }
        return false;
    }

    @EventHandler
    public void RuneActivate(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (!(e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.RIGHT_CLICK_BLOCK || p.getInventory().getItemInMainHand() == null || Horses.mounting.containsKey(p.getName()))) {
            BossRooms room = BossRooms.Common;
            if (isRune(e.getItem())) {
                p.setItemInHand(null);
                BossTypes boss = BossTypes.getType(e.getItem());
                BossHandler.startRune(p, room, boss);
            }
        }
    }

    @EventHandler
    public void playerDeath(final PlayerQuitEvent e) {
        BossHandler.endRune(e.getPlayer());
    }

    @EventHandler
    public void playerDeath(final EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && !(e.getEntity() instanceof LivingEntity)) {
            BossHandler.endRune((Player) e.getEntity());
        }
    }

    @EventHandler
    public void onMobDeath(final EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof LivingEntity && !(e.getEntity() instanceof Player)) {

            final LivingEntity s = (LivingEntity) e.getEntity();
            if (e.getDamage() >= s.getHealth() && s.getEquipment().getItemInMainHand() != null
                    && s.getEquipment().getItemInMainHand().getType() != Material.AIR) {
                Entity d = e.getDamager();
                //Check tier
                BossRooms room = BossRooms.roomMap.get(d);
                BossTypes boss = BossRooms.bossMap.get(d);

                int waveKills = (BossRooms.wave.get(room)) * 12;

                final String type = s.getMetadata("type").get(0).asString();

                if(BossTypes.isBoss(type)){
                    int item = new Random().nextInt(7)+1;
                    ItemStack drop = FactoryBossDrop.setDrop(boss, item);
                    PracticeServer.world.dropItemNaturally(s.getLocation(), drop);
                    d.sendMessage(ChatColor.GRAY + "You have defeated " + ChatColor.GOLD + boss.name);
                    d.sendMessage(ChatColor.GRAY + "To return to Deadpeaks " + ChatColor.GREEN + ChatColor.UNDERLINE + "Click Here");
                    //BossHandler.completeRune((Player) d);
                }

                if (BossRooms.roomMap.containsKey(d)) {
                    int kills = 0;
                    if (BossRooms.kills.get(room) != null) {
                        kills = BossRooms.kills.get(room);
                    } else {
                        BossRooms.kills.put(room, 1);
                    }
                    if (kills >= waveKills-1) {
                        BossHandler.waveUp((Player) d, room, boss);
                        BossRooms.kills.put(room, 0);
                    } else {
                        BossRooms.kills.put(room, kills + 1);
                        d.sendMessage("" + ChatColor.GRAY + BossRooms.kills.get(room) + " / " + waveKills + ChatColor.GREEN + " Mobs Killed");
                    }
                }

            }

        }
    }

}
