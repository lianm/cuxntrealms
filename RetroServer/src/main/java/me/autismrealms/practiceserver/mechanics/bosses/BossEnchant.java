package me.autismrealms.practiceserver.mechanics.bosses;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.enchants.GenerateEnchant;
import me.autismrealms.practiceserver.mechanics.item.ItemType;
import me.autismrealms.practiceserver.mechanics.item.Rarity;
import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class BossEnchant implements Listener {

    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, PracticeServer.plugin);
    }

    public static String makeBossName(int tier){
        return Tiers.colors[tier] + Tiers.prefix[tier] + " Boss Enchant";
    }


    public static ItemStack create(int amt, int tier){
        ItemStack enchant = new ItemStack(Material.EMPTY_MAP);
        ItemMeta enchMeta = enchant.getItemMeta();
        enchMeta.setDisplayName(makeBossName(tier));
        List<String> lore = new ArrayList<>();
        lore.add("" + ChatColor.GRAY + ChatColor.ITALIC + "A powerful enchant has the ability to enchant");
        lore.add("" + ChatColor.GRAY + ChatColor.ITALIC + "any item by 5% without the risk of it breaking.");
        lore.add(Rarity.Unique.line);
        enchMeta.setLore(lore);
        enchant.setItemMeta(enchMeta);
        return enchant;
    }

    public static ItemStack enchantItem(Player p, ItemStack enchant, ItemStack hand){
        ItemType type = ItemType.getItemType(hand);
        ItemMeta itemMeta = hand.getItemMeta();
        List<String> lore = new ArrayList<>();
        int handAmt = GenerateEnchant.getPlus(hand.getItemMeta().getDisplayName());
        if(handAmt < 12) {
            switch (type){
                case ARMOR:
                    itemMeta.setDisplayName(GenerateEnchant.applyPlus(itemMeta.getDisplayName()));
                    lore.set(1, GenerateEnchant.increaseHp(lore.get(1)));
                    lore.set(2, GenerateEnchant.increaseEnergyHpRegen(lore.get(2)));
                case WEAPON:
                    itemMeta.setDisplayName(GenerateEnchant.applyPlus(itemMeta.getDisplayName()));
                    lore.set(0, GenerateEnchant.increaseDmg(lore.get(0)));
            }
            handAmt++;
        }
        itemMeta.setLore(lore);
        hand.setItemMeta(itemMeta);
        return hand;
    }

    public static boolean isEnchant(ItemStack item){
        if (item != null && item.getType() == Material.EMPTY_MAP
                && item.getItemMeta().getDisplayName() != null
                && item.getItemMeta().getDisplayName().contains(makeBossName(Tiers.getStringTier(item.getItemMeta().getDisplayName())))){
            return true;
        }
        return false;
    }

    @EventHandler
    public void apply(final InventoryClickEvent e) {
        final Player p = (Player) e.getWhoClicked();
        if (!e.getInventory().getName().equalsIgnoreCase("container.crafting")) {
            return;
        }
        if (e.getSlotType() == InventoryType.SlotType.ARMOR) {
            return;
        }
        if (isEnchant(e.getCursor()) && ItemType.getItemType(e.getCurrentItem()) != null) {
            e.setCurrentItem(enchantItem(p, e.getCursor(), e.getCurrentItem()));
            e.setCursor(null);

        }


    }
}
