package me.autismrealms.practiceserver.mechanics.drops.generate;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.drops.Mobdrops;
import me.autismrealms.practiceserver.mechanics.enchants.EnchantType;
import me.autismrealms.practiceserver.mechanics.enchants.Enchants;
import me.autismrealms.practiceserver.mechanics.item.ItemHandler;
import me.autismrealms.practiceserver.mechanics.item.ItemType;
import me.autismrealms.practiceserver.mechanics.mobs.elite.EliteType;
import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Random;

public class FactoryEliteItem {

    public static ItemStack setDrop(String mobname){
        int item = new Random().nextInt(7)+1;
        EliteType elite = EliteType.getEliteType(mobname);
        int tier = elite.tier;
        ArrayList<String> enchants = new ArrayList<String>();
        ItemType item_type = ItemType.getType(item);
        if(item_type == ItemType.WEAPON){
            item = elite.item;
        }
        String mat = ItemHandler.getMatString(tier, item);
        //Will be overwritten if ItemType is ARMOR
        String item_word = ItemHandler.getItem(item);


        double strength = (elite.strength /200D) + 0.6D;
        //set damage variables
        double mindmg = GenerateDrop.createDamage(tier, 4, item, false) * strength;
        double maxdmg = GenerateDrop.createDamage(tier, 4, item, true) * strength;
        double health = GenerateDrop.createHpBase(tier, 4, item) * strength;

        //Add to lore, content depending on item type
        if (item_type == ItemType.ARMOR){
            enchants.add(setArmDps(elite.armdps, tier));
            enchants.add(ChatColor.RED + "HP: " + GenerateDrop.createHpBase(tier, 3, item));
            enchants.add(setNrgHps(elite.nrghp, tier));
            item_word = ItemHandler.getItem(item);
        }else if(item_type == ItemType.WEAPON){
            enchants.add(ChatColor.RED + "DMG: " + (int) mindmg + " - " + (int) maxdmg);
            item = item;
        }

        ItemStack item_stack = new ItemStack(Material.getMaterial(mat + "_" + item_word));
        ItemMeta item_meta = item_stack.getItemMeta();

        for(EnchantType ench : elite.enchants) {
            if (item_type == ench.itemType) {
                enchants.add(EnchantType.getOrbValue(ench, tier, true));
            }
        }

        //Setting item name;
        String name = ItemHandler.generateNamePrefix(item, Tiers.colors[tier-1] + elite.name + "'s " + Tiers.prefix[tier-1]);
        item_meta.setDisplayName(name);

        //Last lore add (Rarity)
        enchants.add(ItemHandler.rarityColors[3] + ItemHandler.rarity[3]);
        item_meta.setLore(enchants);

        //Remove Minecrafts lores they add
        for (ItemFlag itemFlag : ItemFlag.values()) {
            item_meta.addItemFlags(itemFlag);
        }

        item_stack.setItemMeta(item_meta);

        //Add elite enhancements
        if (PracticeServer.GLOWING_NAMED_ELITE_DROP) {
            item_stack.addUnsafeEnchantment(Enchants.glow, 1);
        }
        if (PracticeServer.RANDOM_DURA_NAMED_ELITE_DROP) {
            item_stack.setDurability((short) random(new Random(), 0, item_stack.getType().getMaxDurability()));
        }

        //Set T6 Color
        if(item >= 4 && tier == 6) return Mobdrops.setItemBlueLeather(item_stack);

        return item_stack;
            
    }

    public static int random(Random random, int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }

    public static String setArmDps(int armdps, int tier){
        if(armdps == 1) {
            return ChatColor.RED + "DPS: " + GenerateOrb.setDps(tier) + " - " + GenerateOrb.setDps(tier);
        }else{
            return ChatColor.RED + "ARMOR: " + GenerateOrb.setDps(tier) + " - " + GenerateOrb.setDps(tier);
        }
    }

    public static String setNrgHps(int nrghps, int tier){
        if(nrghps == 1){
            return ChatColor.RED + "HP REGEN: +" + GenerateOrb.setHps(tier) + " HP/s";
        }else{
            return ChatColor.RED + "ENERGY REGEN: +" + GenerateOrb.setEnergy(tier) + "%";
        }
    }

}
