package me.disregard.bungeesockets;

import fr.rhaz.sockets.server.SocketMessenger;
import fr.rhaz.sockets.socket4mc.Socket4Bungee;
import fr.rhaz.sockets.utils.JSONMap;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.util.HashMap;
import java.util.Map;

public class BungeeSockets extends Plugin implements Listener{
    private Map<String, SocketMessenger> servers;
    @Override
    public void onEnable(){
        servers = new HashMap<>();
        getProxy().getPluginManager().registerListener(this, this);
        getLogger().info("Disregard");

    }
    @EventHandler
    public void onMessage(Socket4Bungee.Server.ServerSocketJSONEvent e){

        if(!e.getChannel().equals("GChat"))
            return;

        String data = e.getExtraString("data");

        if(data.equals("register")){
            if(!servers.containsKey(e.getName().toLowerCase())){
                servers.put(e.getName().toLowerCase(), e.getMessenger());
                getLogger().info("Successfully Registered " + e.getName().toLowerCase());
                e.write("registered");
            }
            return;
        }

        if(data.equals("guildmessage")) {
            getLogger().info("Guild Message Recieved");
            for(String s: servers.keySet()){
                SocketMessenger server = servers.get(s);

                if(!server.isHandshaked()) getLogger().info("NOT HANDSHAKED");
                getLogger().info("Guild Message sent to " + s);
                server.write("GChat", new JSONMap(
                        "data", "guildmessage",
                        "message", e.getExtraString("message"),
                        "server", e.getExtraString("server"),
                        "player", e.getExtraString("player"),
                        "guild", e.getExtraString("guild")
                ));
            }
        }
    }
}
