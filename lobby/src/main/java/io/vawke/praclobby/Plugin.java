package io.vawke.praclobby;

import io.vawke.praclobby.util.item.Glowing;
import io.vawke.praclobby.world.WorldListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Giovanni on 10-6-2017.
 */
public class Plugin extends JavaPlugin {

    @Getter
    private static Plugin instance;

    @Override
    public void onEnable() {
        instance = this;

        Bukkit.getPluginManager().registerEvents(new WorldListener(), this);

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }
}
