/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package me.autismrealms.practiceserver.mechanics.item;

import me.autismrealms.practiceserver.mechanics.enchants.Enchants;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Items {

    public static ItemStack orb(boolean inshop) {
        ItemStack orb = new ItemStack(Material.MAGMA_CREAM);
        ItemMeta orbmeta = orb.getItemMeta();
        ArrayList<String> lore = new ArrayList<String>();
        orbmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Orb of Alteration");
        lore.add(ChatColor.GRAY + "Randomizes stats of selected equipment.");
        if (inshop) {
            lore.add(ChatColor.GREEN + "Price: " + ChatColor.WHITE + "500g");
        }
        orbmeta.setLore(lore);
        orb.setItemMeta(orbmeta);
        return orb;
    }

    public static boolean isWeapon(int item){
        if(item == 0 || item == 1 || item == 2 || item == 3){
            return true;
        }
        return false;
    }

    public static boolean isArmour(int item){
        if(item == 4 || item == 5 || item == 6 || item == 7){
            return true;
        }
        return false;
    }

    public static String[] rarity = {"Common", "Uncommon", "Rare", "Unique"};

    public static int getRarityFromItem(ItemStack is){
        List<String> lore = is.getItemMeta().getLore();
        for(int i = 0; i < lore.size(); i++){
            for(int r = 0; r < rarity.length; r++){
                if(rarity[r].contains(ChatColor.stripColor(lore.get(i)))){
                    return r;
                }
            }
        }
        return 0;
    }


    public static boolean isPlussed(String name){
        if (ChatColor.stripColor(name).startsWith("[+")) {
            return true;
        }
        return false;
    }

    public static int getItemType(Material mat) {
        if (mat.name().contains("_HOE")) {
            return 0;
        }
        if (mat.name().contains("_SPADE")) {
            return 1;
        }
        if (mat.name().contains("_SWORD")) {
            return 2;
        }
        if (mat.name().contains("_AXE")) {
            return 3;
        }
        if (mat.name().contains("_HELMET")) {
            return 4;
        }
        if (mat.name().contains("_CHESTPLATE")) {
            return 5;
        }
        if (mat.name().contains("_LEGGINGS")) {
            return 6;
        }
        if (mat.name().contains("_BOOTS")) {
            return 7;
        }
        return -1;
    }

    public static boolean isEnchant(ItemStack item){
        if (!(item.getType() == Material.EMPTY_MAP)) return false;
        if(item.getItemMeta().getDisplayName().contains("Scroll")){
            return true;
        }
        return false;
    }


    public static int getPlussedAmount(ItemStack itemStack){
        if (itemStack.getItemMeta().hasDisplayName()) {
            String name = ChatColor.stripColor(itemStack.getItemMeta().getDisplayName());
            if (isPlussed(itemStack.getItemMeta().getDisplayName())) {
                name = name.split("\\[+")[1].split("\\]")[0];
                return Integer.parseInt(name);
            }
        }
        return 0;
    }


    public static ItemStack legendaryOrb(boolean inshop) {
        ItemStack orb = new ItemStack(Material.MAGMA_CREAM);
        ItemMeta orbmeta = orb.getItemMeta();
        ArrayList<String> lore = new ArrayList<String>();
        orbmeta.setDisplayName(ChatColor.YELLOW + "Legendary Orb of Alteration");
        lore.add(ChatColor.GRAY + "Plus 4s Items that have a plus lower than 4.");
        lore.add(ChatColor.GRAY + "It also has a extremely high chance of good orbs.");
        if (inshop) {
            lore.add(ChatColor.GREEN + "Price: " + ChatColor.WHITE + "2000g");
        }
        orbmeta.setLore(lore);
        orb.setItemMeta(orbmeta);
        orb.addUnsafeEnchantment(Enchants.glow, 1);
        return orb;
    }

    public static org.inventivetalent.glow.GlowAPI.Color getColorFromTier(int tier) {
        org.inventivetalent.glow.GlowAPI.Color color = null;
        switch (tier) {
            case 0:
                color = org.inventivetalent.glow.GlowAPI.Color.WHITE;
                break;
            case 1:
                color = org.inventivetalent.glow.GlowAPI.Color.GREEN;
                break;
            case 2:
                color = org.inventivetalent.glow.GlowAPI.Color.AQUA;
                break;
            case 3:
                color = org.inventivetalent.glow.GlowAPI.Color.YELLOW;
                break;
        }
        return color;
    }

    public static ItemStack supporterPick(){
        ItemStack P = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta pickmeta = P.getItemMeta();
        pickmeta.setDisplayName(ChatColor.BLUE + "Donator++ Pickaxe");
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(ChatColor.GRAY + "Level: " + ChatColor.BLUE + "120");
        lore.add(ChatColor.GRAY + "0 / 0");
        lore.add(ChatColor.GRAY + "EXP: " + ChatColor.BLUE + "||||||||||||||||||||||||||||||||||||||||||||||||||");
        lore.add(ChatColor.RED + "DOUBLE ORE: 25%");
        lore.add(ChatColor.RED + "GEM FIND: 15%");
        lore.add(ChatColor.RED + "TRIPLE ORE: 10%");
        lore.add(ChatColor.RED + "TREASURE FIND: 5%");
        lore.add(ChatColor.RED + "MINING SUCCESS: 20%");
        lore.add(ChatColor.RED + "DURABILITY: 15%");
        lore.add(ChatColor.RED + "MAGNETISM");
        lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC + "A pickaxe made out of ice.");
        pickmeta.setLore(lore);
        P.setItemMeta(pickmeta);
        return P;
    }

    public static ItemStack subplusPick(){
        ItemStack P = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta pickmeta = P.getItemMeta();
        pickmeta.setDisplayName(ChatColor.BLUE + "Donator+ Pickaxe");
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(ChatColor.GRAY + "Level: " + ChatColor.BLUE + "120");
        lore.add(ChatColor.GRAY + "0 / 0");
        lore.add(ChatColor.GRAY + "EXP: " + ChatColor.BLUE + "||||||||||||||||||||||||||||||||||||||||||||||||||");
        lore.add(ChatColor.RED + "DOUBLE ORE: 10%");
        lore.add(ChatColor.RED + "GEM FIND: 10%");
        lore.add(ChatColor.RED + "TRIPLE ORE: 5%");
        lore.add(ChatColor.RED + "TREASURE FIND: 3%");
        lore.add(ChatColor.RED + "MINING SUCCESS: 20%");
        lore.add(ChatColor.RED + "DURABILITY: 15%");
        lore.add(ChatColor.RED + "MAGNETISM");
        lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC + "A pickaxe made out of ice.");
        pickmeta.setLore(lore);
        P.setItemMeta(pickmeta);
        return P;
    }

    public static ItemStack subPick(){
        ItemStack P = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta pickmeta = P.getItemMeta();
        pickmeta.setDisplayName(ChatColor.BLUE + "Donator Pickaxe");
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(ChatColor.GRAY + "Level: " + ChatColor.BLUE + "120");
        lore.add(ChatColor.GRAY + "0 / 0");
        lore.add(ChatColor.GRAY + "EXP: " + ChatColor.BLUE + "||||||||||||||||||||||||||||||||||||||||||||||||||");
        lore.add(ChatColor.RED + "DOUBLE ORE: 10%");
        lore.add(ChatColor.RED + "GEM FIND: 10%");
        lore.add(ChatColor.RED + "TRIPLE ORE: 5%");
        lore.add(ChatColor.RED + "TREASURE FIND: 3%");
        lore.add(ChatColor.RED + "MINING SUCCESS: 20%");
        lore.add(ChatColor.RED + "DURABILITY: 15%");
        lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC + "A pickaxe made out of ice.");
        pickmeta.setLore(lore);
        P.setItemMeta(pickmeta);
        return P;
    }

    public static ItemStack signNewCustomItem(final Material m, final String name, final String desc) {
        final ItemStack is = new ItemStack(m);
        final ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        final List<String> new_lore = new ArrayList<String>();
        if (desc.contains(",")) {
            String[] split;
            for (int length = (split = desc.split(",")).length, i = 0; i < length; ++i) {
                final String s = split[i];
                new_lore.add(s);
            }
        } else {
            new_lore.add(desc);
        }
        im.setLore(new_lore);
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack enchant(int tier, int type, boolean inshop) {
        ItemStack is = new ItemStack(Material.EMPTY_MAP);
        ItemMeta im = is.getItemMeta();
        ArrayList<String> lore = new ArrayList<String>();
        String name = "";
        int price = 0;
        if (tier == 1) {
            price = 50;
            name = ChatColor.WHITE + " Enchant ";
            if (type == 0) {
                name = String.valueOf(name) + "Wooden";
            }
            if (type == 1) {
                name = String.valueOf(name) + "Leather";
            }
        }
        if (tier == 2) {
            price = 150;
            name = ChatColor.GREEN + " Enchant ";
            if (type == 0) {
                name = String.valueOf(name) + "Stone";
            }
            if (type == 1) {
                name = String.valueOf(name) + "Chainmail";
            }
        }
        if (tier == 3) {
            price = 250;
            name = ChatColor.AQUA + " Enchant Iron";
        }
        if (tier == 4) {
            price = 350;
            name = ChatColor.LIGHT_PURPLE + " Enchant Diamond";
        }
        if (tier == 5) {
            price = 500;
            name = ChatColor.YELLOW + " Enchant Gold";
        }
        if (tier == 6) {
            price = 3200;
            name = ChatColor.BLUE + " Enchant Frozen";
        }
        if (type == 0) {
            //  price = (int) ((double) price * 1.5);
            name = String.valueOf(name) + " Weapon";
            lore.add(ChatColor.RED + "+5% DMG");
            lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC + "Weapon will VANISH if enchant above +3 FAILS.");
        }
        if (type == 1) {
            name = String.valueOf(name) + " Armor";
            lore.add(ChatColor.RED + "+5% HP");
            lore.add(ChatColor.RED + "+5% HP REGEN");
            lore.add(ChatColor.GRAY + "   - OR -");
            lore.add(ChatColor.RED + "+1% ENERGY REGEN");
            lore.add(ChatColor.GRAY.toString() + ChatColor.ITALIC + "Armor will VANISH if enchant above +3 FAILS.");
        }
        if (inshop) {
            lore.add(ChatColor.GREEN + "Price: " + ChatColor.WHITE + price + "g");
        }
        im.setDisplayName(ChatColor.WHITE.toString() + ChatColor.BOLD + "Scroll:" + name);
        im.setLore(lore);
        is.setItemMeta(im);
        return is;
    }
}

