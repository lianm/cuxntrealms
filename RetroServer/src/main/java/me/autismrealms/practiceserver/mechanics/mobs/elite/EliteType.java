package me.autismrealms.practiceserver.mechanics.mobs.elite;

import me.autismrealms.practiceserver.mechanics.enchants.EnchantType;
import me.autismrealms.practiceserver.mechanics.mobs.MobType;

import java.util.ArrayList;
import java.util.Arrays;

public enum EliteType {

    Mitsuki(50, MobType.Skeleton, "Mitsuki The Dominator", 1, 1, 2, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.BLOCK, EnchantType.STR))),
    Riskan(30, MobType.Zombie, "Riskan", 2, 1, 3, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.CRITICAL_HIT, EnchantType.LIFE_STEAL, EnchantType.INT, EnchantType.STR))),
    Copjak(60, MobType.Zombie, "Cop'jak", 2, 1, 1, 1, new ArrayList<>(Arrays.asList(EnchantType.POISON_DMG, EnchantType.STR))),
    KingOfGreed(80, MobType.Skeleton, "The King Of Greed", 3, 1, 3, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.STR))),
    SkeletonKing(50, MobType.Skeleton, "The Skeleton King", 3, 1, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.PURE_DMG,EnchantType.ACCURACY, EnchantType.VIT))),
    Impa(20, MobType.Daemon, "Impa the Impaler", 3, 1, 1, 1, new ArrayList<>(Arrays.asList(EnchantType.POISON_DMG, EnchantType.CRITICAL_HIT, EnchantType.BLOCK, EnchantType.STR))),
    BloodButcher(60, MobType.Zombie, "The Blood Butcher", 4, 1, 2, 2, new ArrayList<>(Arrays.asList(EnchantType.CRITICAL_HIT, EnchantType.ICE_DMG, EnchantType.ACCURACY, EnchantType.VIT))),
    Blayshan(90, MobType.Zombie, "Blayshan The Naga", 4, 2, 3, 2, new ArrayList<>(Arrays.asList(EnchantType.STR, EnchantType.CRITICAL_HIT, EnchantType.ICE_DMG))),
    Duranor(30, MobType.Skeleton, "Duranor", 4, 1, 1, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT))),
    WatchMaster(40, MobType.Skeleton, "The Watchmaster", 4, 1, 2, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.VS_MONSTERS, EnchantType.CRITICAL_HIT, EnchantType.DEX, EnchantType.DODGE, EnchantType.VIT))),
    Jayden(50, MobType.WitherSkeleton, "King Jayden", 5, 1, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.PURE_DMG, EnchantType.CRITICAL_HIT, EnchantType.BLOCK, EnchantType.DODGE, EnchantType.STR))),
    Kilatan(40, MobType.WitherSkeleton, "Kilaton", 5, 1, 2, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.VS_PLAYERS, EnchantType.INT, EnchantType.DODGE))),
    Warden(15, MobType.WitherSkeleton, "The Warden", 5, 1, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.POISON_DMG, EnchantType.LIFE_STEAL, EnchantType.VS_MONSTERS, EnchantType.DEX, EnchantType.VIT))),
    Conquerer(60, MobType.WitherSkeleton, "The Conquerer", 6, 2, 3, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT))),
    Krampus(60, MobType.Zombie, "Krampus", 5, 1, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT))),
    ExiledKing(20, MobType.WitherSkeleton, "The Exiled King", 6, 1, 1, 1, new ArrayList<>(Arrays.asList(EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT, EnchantType.ICE_DMG, EnchantType.CRITICAL_HIT))),
    FrostKing(80, MobType.WitherSkeleton, "Frost Walker", 5, 1, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT))),
    Crypt(95, MobType.Golem, "Crypt Guardian", 6, 2, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.ACCURACY, EnchantType.LIFE_STEAL, EnchantType.CRITICAL_HIT, EnchantType.ICE_DMG, EnchantType.VIT, EnchantType.STR, EnchantType.INT, EnchantType.BLOCK))),
    SpiderQueen(25, MobType.CaveSpider, "Spider Queen", 3, 2, 1, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT))),
    Deathlord(95, MobType.Skeleton, "Deathlord", 5, 2, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT)));

    public String name;
    public int strength;
    public int armdps;
    public ArrayList<EnchantType> enchants;
    public int tier;
    public int nrghp;
    public String id;
    public int item;
    public MobType type;

    EliteType(int strength, MobType type, String name, int tier, int armdps, int item, int nrghp, ArrayList<EnchantType> enchants) {
        this.name = name;
        this.strength = strength/2;
        this.armdps = armdps;
        this.type = type;
        this.nrghp = nrghp;
        this.item = item;
        this.id = this.toString().toLowerCase();
        this.enchants = enchants;
        this.tier = tier;
    }

    public static EliteType getEliteType(String type){
        for(EliteType elite : EliteType.values()){
            if(type.equalsIgnoreCase(elite.id)){
                return elite;
            }
        }
        return null;
    }


}
