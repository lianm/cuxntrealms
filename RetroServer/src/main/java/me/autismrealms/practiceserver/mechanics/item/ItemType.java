package me.autismrealms.practiceserver.mechanics.item;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public enum ItemType {

    ARMOR,
    WEAPON,
    PICKAXE;

    public String type;

    ItemType(){
        this.type = this.toString().toLowerCase();
    }

    public static ItemType getType(int item){
        switch (item){
            case 0:
            case 1:
            case 2:
            case 3: return ItemType.WEAPON;
            case 4:
            case 5:
            case 6:
            case 7: return ItemType.ARMOR;
        }
        return null;
    }

    public static ItemType getItemType(ItemStack is){
        String itemName = ChatColor.stripColor(is.getItemMeta().getDisplayName());
        for(ItemType itemType : ItemType.values()){
            if(itemName.toLowerCase().contains(itemType.type)){
                return itemType;
            }
        }
        return null;
    }

}
