package me.autismrealms.practiceserver.mechanics.idler;

import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.mobs.elite.EliteType;
import me.autismrealms.practiceserver.mechanics.money.Money;
import me.autismrealms.practiceserver.mechanics.progress.ProgressionConfig;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public enum IdlerType {

    Miner("Bob The Miner", 30, "Orbs", Items.orb(false), 2400L, 10, EliteType.ExiledKing),
    Fisherman("Fisherman Fred", 20,  "Fish", new ItemStack(Material.RAW_FISH), 600L, 10, EliteType.KingOfGreed),
    Banker("Banker Betty", 50, "Gems", Money.makeGems(1), 80L, 10, EliteType.Deathlord),
    Hunter("Harry The Hunter", 100, "Crates", new ItemStack(Material.CHEST), 12000L, 5, EliteType.Crypt);

    public String name;
    public int cost;
    public int cost_scale;
    public ItemStack item;
    public long time;
    public int max_upgrades;
    public String id;
    public EliteType requirement;
    public String itemName;
    public String path;

    IdlerType(String name, int cost_scale, String itemName, ItemStack item, long time, int max_upgrades, EliteType requirement){
        this.id = this.toString();
        this.path = "." + id + ".";
        this.name = name;
        this.requirement = requirement;
        this.itemName = itemName;
        this.cost_scale = cost_scale;
        this.item = item;
        this.time = time;
        this.max_upgrades = max_upgrades;
    }

    public static boolean hasRequirement(EliteType type, Player p){
        boolean elite = ProgressionConfig.get().getBoolean(p.getUniqueId() + ".tier" + type.tier + "." + type.id);
        return elite;
    }

}
