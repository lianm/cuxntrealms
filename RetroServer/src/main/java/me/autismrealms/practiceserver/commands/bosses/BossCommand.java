package me.autismrealms.practiceserver.commands.bosses;

import me.autismrealms.practiceserver.mechanics.bosses.BossTypes;
import me.autismrealms.practiceserver.mechanics.bosses.rune.FactoryRune;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class BossCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            Inventory inv = Bukkit.createInventory(null, 27, "Boss's");
            for(BossTypes bossTypes : BossTypes.values()){
                inv.addItem(FactoryRune.createRune(bossTypes.tier));
            }

            player.openInventory(inv);
        }
        return false;
    }
}
