package me.autismrealms.practiceserver.mechanics.progress;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.mobs.elite.EliteType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class ProgressionHandler implements Listener {

    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, PracticeServer.plugin);
    }

    @EventHandler
    public void onMobDeath(final EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof LivingEntity && !(e.getEntity() instanceof Player)) {
            final LivingEntity s = (LivingEntity) e.getEntity();
            Entity d = e.getDamager();
            //Check tier

            /**
            if(getCurrentTier((Player) d) <= Mobs.getMobTier(s)){
                d.sendMessage(ChatColor.RED + "You're not a high enough tier to kill these mobs. Kill elites to up your tier.");
                e.setDamage(0);
                e.setCancelled(true);
                return;
            }
            **/

            if (e.getDamage() >= s.getHealth() && s.getEquipment().getItemInMainHand() != null
                    && s.getEquipment().getItemInMainHand().getType() != Material.AIR) {
                for (EliteType elite : EliteType.values()) {
                    boolean eliteKilled = ProgressionConfig.get().getBoolean(d.getUniqueId() + ".tier" + elite.tier + "." + elite.id);
                    if (s.getMetadata("type").get(0).asString().equals(elite.id) &&
                            eliteKilled != true) {
                        d.sendMessage(ChatColor.GRAY + "You have killed " + ChatColor.LIGHT_PURPLE + elite.name);
                        ProgressionConfig.get().set(d.getUniqueId() + ".tier" + elite.tier + "." + elite.id, true);
                        ProgressionConfig.save();
                    }
                }
            }
        }
    }

    public static int getCurrentTier(Player p){
        for(EliteType type : EliteType.values()){
            boolean eliteKilled = ProgressionConfig.get().getBoolean(p.getUniqueId() + ".tier" + type.tier + "." + type.id);
            if(eliteKilled == false){
                return type.tier;
            }
        }

        return 0;
    }

}