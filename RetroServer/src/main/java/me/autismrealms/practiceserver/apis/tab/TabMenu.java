package me.autismrealms.practiceserver.apis.tab;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.GameProfile;
import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.commands.moderation.VanishCommand;
import me.autismrealms.practiceserver.enums.ranks.RankEnum;
import me.autismrealms.practiceserver.mechanics.guilds.guild.Guild;
import me.autismrealms.practiceserver.mechanics.guilds.guild.GuildManager;
import me.autismrealms.practiceserver.mechanics.guilds.guild.Role;
import me.autismrealms.practiceserver.mechanics.guilds.player.GuildPlayer;
import me.autismrealms.practiceserver.mechanics.guilds.player.GuildPlayers;
import me.autismrealms.practiceserver.mechanics.moderation.ModerationMechanics;
import me.autismrealms.practiceserver.mechanics.player.Stats.StatsMain;
import me.autismrealms.practiceserver.mechanics.pvp.Alignments;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.mcsg.double0negative.tabapi.TabAPI;

import java.util.*;

/**
 * Created by Giovanni on 23-7-2017.
 */
public class TabMenu {
//    public static void init() {
//
//        TabFrame tabFrame = new TabFrame() {
//            @Override
//            public void function() {
//                this.variables.addAll(Arrays.asList(
//
//                        new Variable("gems") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(Economy.getBalance(player.getUniqueId()));
//                            }
//                        },
//                        new Variable("Alignment") {
//                            @Override
//                            public String getReplacement(Player player) {
//
//                                String alignment = StatsMain.getAlignment(player);
//                                if (alignment.toLowerCase().contains("lawful")) return alignment;
//
//                                return StatsMain.getAlignment(player) + " &7> &c" + StatsMain.getAlignTime(player);
//                            }
//                        },
//                        new Variable("dps") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(TabUtil.getDPS(player));
//                            }
//                        },
//                        new Variable("armor") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(TabUtil.getArmor(player));
//                            }
//                        },
//                        new Variable("hps") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(TabUtil.getHPS(player));
//                            }
//                        },
//                        new Variable("energy") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(TabUtil.getEnergy(player));
//                            }
//                        },
//
//                        new Variable("ps_playerKills") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getPlayerKills());
//                            }
//
//                        },
//
//                        new Variable("ps_deaths") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getDeaths());
//                            }
//                        },
//
//                        new Variable("t1_kills") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getT1Kills());
//                            }
//                        },
//
//
//                        new Variable("t2_kills") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getT2Kills());
//                            }
//                        },
//
//                        new Variable("t3_kills") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getT3Kills());
//                            }
//                        },
//
//                        new Variable("t4_kills") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getT4Kills());
//                            }
//                        },
//
//                        new Variable("t5_kills") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getT5Kills());
//                            }
//                        },
//
//                        new Variable("t6_kills") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getT6Kills());
//                            }
//                        },
//
//                        new Variable("total_kills") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                int totalKills = 0;
//                                GuildPlayer guildPlayer = GuildPlayers.getInstance().get(player.getUniqueId());
//                                totalKills += guildPlayer.getT1Kills();
//                                totalKills += guildPlayer.getT2Kills();
//                                totalKills += guildPlayer.getT3Kills();
//                                totalKills += guildPlayer.getT4Kills();
//                                totalKills += guildPlayer.getT5Kills();
//                                totalKills += guildPlayer.getT6Kills();
//                                return TabUtil.format(totalKills);
//                            }
//                        },
//
//                        new Variable("lootchests") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getLootChestsOpen());
//                            }
//                        },
//
//                        new Variable("ore_mined") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getOreMined());
//                            }
//                        },
//                        new Variable("fish_caught") {
//                            @Override
//                            public String getReplacement(Player player) {
//                                return TabUtil.format(GuildPlayers.getInstance().get(player.getUniqueId()).getFishCaught());
//                            }
//                        }
//
//
//                ));
//                for (int i = 0; i < 17; i++) {
//                    final int finalIndex = i;
//                    variables.add(new Variable("guild." + i) {
//                        @Override
//                        public String getReplacement(Player player) {
//                            GuildPlayer guildPlayer = GuildPlayers.getInstance().get(player.getUniqueId());
//                            if (!guildPlayer.isInGuild()) {
//                                if (finalIndex == 1) {
//                                    return ChatColor.GRAY + "You don't have a guild!";
//                                }
//                                if (finalIndex == 2) {
//                                    return ChatColor.GRAY + "Type /gcreate <name> <tag>";
//                                }
//                                if (finalIndex == 3) {
//                                    return ChatColor.GRAY + "to create a guild!";
//                                }
//                                if (finalIndex == 4) {
//                                    return ChatColor.GRAY + "costs 5,000g";
//                                }
//                                return "";
//                            }
//                            Guild guild = GuildManager.getInstance().get(guildPlayer.getGuildName());
//                            String name = guild.getPlayerString(finalIndex);
//                            return name;
//                        }
//                    });
//                }
//            }
//        };
//
//        tabFrame.function();
//
//        new BukkitRunnable() {
//
//            @Override
//            public void run() {
//                tabFrame.getVariables().forEach(variable -> {
//                    BungeeTabListPlusBukkitAPI.registerVariable(variable);
//                });
//            }
//        }.runTaskLater(20L);
//
//    }

    public static BukkitTask task = null;

    public static String RankTag(Player p) {
        if(Alignments.get(p) == "&cCHAOTIC"){
            return ChatColor.RED + "";
        }if(Alignments.get(p) == "&eNEUTRAL"){
            return ChatColor.YELLOW + "";
        }
        switch (ModerationMechanics.getRank(p)) {
            case DEFAULT:
                return ChatColor.GRAY + "";
            case SUB:
                return ChatColor.GREEN + "" + ChatColor.BOLD + "";
            case SUB1:
                return ChatColor.GOLD + "" + ChatColor.BOLD + "";
            case SUB2:
                return ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "";
            case SUB3:
                return ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "";
            case SUPPORTER:
                return ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "";
            case YOUTUBER:
                return ChatColor.WHITE + "";
            case QUALITY:
                return ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "";
            case BUILDER:
                return ChatColor.AQUA + "" + ChatColor.BOLD + "";
            case PMOD:
                return ChatColor.WHITE + "" + ChatColor.BOLD + "";
            case GM:
                return ChatColor.AQUA + "" + ChatColor.BOLD + "";
            case DEV:
                return ChatColor.RED + "" + ChatColor.BOLD + "";
            case MANAGER:
                return ChatColor.YELLOW + "" + ChatColor.BOLD + "";
        }
        return ChatColor.GRAY + "";
    }

    public static String getRank(Player p) {
        switch (ModerationMechanics.getRank(p)) {
            case DEFAULT:
                return ChatColor.GRAY + "NONE";
            case SUB:
                return ChatColor.GREEN + "" + ChatColor.BOLD + "SUB";
            case SUB1:
                return ChatColor.GOLD + "" + ChatColor.BOLD + "SUB+";
            case SUB2:
                return ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "S++";
            case SUB3:
                return ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "SUPP";
            case SUPPORTER:
                return ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "SUPP";
            case YOUTUBER:
                return ChatColor.WHITE + "" + ChatColor.BOLD + "YT";
            case QUALITY:
                return ChatColor.AQUA + "" + ChatColor.BOLD + "QA";
            case BUILDER:
                return ChatColor.AQUA + "" + ChatColor.BOLD + "BLDR";
            case PMOD:
                return ChatColor.WHITE + "" + ChatColor.BOLD + "PMOD";
            case GM:
                return ChatColor.AQUA + "" + ChatColor.BOLD + "GM";
            case DEV:
                return ChatColor.RED + "" + ChatColor.BOLD + "DEV";
            case MANAGER:
                return ChatColor.YELLOW + "" + ChatColor.BOLD + "MNGR";
        }
        return ChatColor.GRAY + "NONE";
    }

    public static void clearTab(Player p){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 20; j++) {
                TabAPI.setTabString(PracticeServer.plugin, p, j, i, " ");
            }
        }
    }

    public static ArrayList<Player> sortPlayers(Collection<? extends Player> playerlist){
        ArrayList<RankEnum> staff = new ArrayList<RankEnum>(
                Arrays.asList(RankEnum.DEV, RankEnum.MANAGER, RankEnum.GM, RankEnum.BUILDER, RankEnum.PMOD, RankEnum.QUALITY));
        ArrayList<RankEnum> ranks = new ArrayList<RankEnum>(
                Arrays.asList(RankEnum.YOUTUBER, RankEnum.SUPPORTER, RankEnum.SUB3, RankEnum.SUB2, RankEnum.SUB1, RankEnum.SUB, RankEnum.DEFAULT));
        ArrayList<Player> finalList = new ArrayList<>();
        for(RankEnum rank : staff){
            for(Player p : playerlist){
                if(ModerationMechanics.getRank(p) == rank){
                    finalList.add(p);
                }
            }
        }
        Server s = Bukkit.getServer();
        for(String p : Alignments.chaotic.keySet()){
            if(s.getPlayer(p) != null && !finalList.contains(s.getPlayer(p))){
                finalList.add(s.getPlayer(p));
            }
        }
        for(String p : Alignments.neutral.keySet()){
            if(s.getPlayer(p) != null && !finalList.contains(s.getPlayer(p))){
                finalList.add(s.getPlayer(p));
            }
        }
        for(RankEnum rank : ranks) {
            for (Player p : playerlist) {
                if (ModerationMechanics.getRank(p) == rank) {
                    if(!finalList.contains(p)) finalList.add(p);
                }
            }
        }
        return finalList;

    }

    public static void bugFixTab(Player p, int x, int y, String message, int playercount){
//        x += 1;
//        if(x > 19){
//            x = 0;
//            y += 1;
//        }
//        if(y > 3){
//            y = 3;
//            x = 19;
//        }
        TabAPI.setTabString(PracticeServer.plugin, p, x, y, message);
    }

    public static void init2(){

        task = Bukkit.getScheduler().runTaskTimer(PracticeServer.plugin, new Runnable() {
            @Override
            public void run() {
                Collection<? extends Player> playerlist = Bukkit.getOnlinePlayers();
                playerlist = sortPlayers(playerlist);

                int playercount = playerlist.size();

                for(Player p : playerlist) {
                    clearTab(p);


                    //Top
                    for(int y = 0; y < 4; y++){
                        bugFixTab(p, 0, y, ChatColor.BLUE + "------------", playercount);
                    }
                    for(int y = 0; y < 4; y++){
                        int x = 3;
                        if(y == 1) x = 2;
                        bugFixTab(p, x, y, ChatColor.BLUE + "------------", playercount);
                    }
                    bugFixTab(p, 1, 0, "/buy for store", playercount);
                    bugFixTab(p, 1, 3, "    /discord", playercount);
                    bugFixTab(p, 2, 0, "Rank: " + getRank(p), playercount);
                    bugFixTab(p, 1, 1, ChatColor.GREEN + " WELCOME TO", playercount);
                    bugFixTab(p, 2, 2, ChatColor.GREEN + "RETRO REALMS", playercount);
                    bugFixTab(p, 2, 3, "by " + ChatColor.RED + "Disregard", playercount);



                    //Player List
                    int i = 4;
                    for(Player p2 : playerlist){
                        if(VanishCommand.vanished.contains(p2)) continue;
                        if(i < 19){
                            int y = 0;
                            bugFixTab(p, i, y,RankTag(p2) + p2.getName(), playercount);
                            i++;
                        }if(i == 19 && playercount == 16){
                            bugFixTab(p, 19, 0,RankTag(p2) + p2.getName(), playercount);
                        }
                    }
                    if(playercount > 16){
                        bugFixTab(p, 19,0,ChatColor.WHITE + Integer.toString(playercount - 15) + " more....", playercount);
                    }



                    //Guild
                    GuildPlayer guildPlayer = GuildPlayers.getInstance().get(p.getUniqueId());
//                    bugFixTab(p, 4,1,ChatColor.DARK_AQUA + "Guild:", playercount);
//                    if(guildPlayer.getGuildName() == null){
//                        bugFixTab(p, 5,1,ChatColor.DARK_AQUA + "NONE", playercount);
//                        bugFixTab(p, 7,1,ChatColor.DARK_AQUA + "/gcreate", playercount);
//                        bugFixTab(p, 8,1,ChatColor.DARK_AQUA + "to make one", playercount);
//                        bugFixTab(p, 9,1,ChatColor.DARK_AQUA + "Costs 5000g", playercount);
//                    }else{
//                        Guild guild = GuildManager.getInstance().get(guildPlayer.getGuildName());
//                        bugFixTab(p, 5,1, guild.getName(), playercount);
//                        bugFixTab(p, 6,1,ChatColor.DARK_AQUA + "Leader:", playercount);
//                        if(Bukkit.getPlayer(guild.getOwner()).isOnline()){
//                            bugFixTab(p, 7,1,ChatColor.GRAY + Bukkit.getPlayer(guild.getOwner()).getName(), playercount);
//                        }else{
//                            bugFixTab(p, 7,1,ChatColor.GRAY + Bukkit.getOfflinePlayer(guild.getOwner()).getName(), playercount);
//                        }
//                        bugFixTab(p, 8,1,ChatColor.DARK_AQUA + "Officers:", playercount);
//
//                        ArrayList<String> officers = new ArrayList<>();
//                        ArrayList<String> members = new ArrayList<>();
//                        for (UUID uuid : guild.getOnlineList()) {
//                            String playerString = Bukkit.getPlayer(uuid).getName();
//                            if (guild.getPlayerRoleMap().get(uuid) == Role.OFFICER) {
//                                officers.add(playerString);
//                            } else if (guild.getPlayerRoleMap().get(uuid) == Role.MEMBER) {
//                                members.add(playerString);
//
//                            }
//                        }
//                        int slot = 9;
//                        for(String officer : officers){
//                            if(slot < 20){
//                                bugFixTab(p, slot,1,ChatColor.DARK_AQUA + officer, playercount);
//                                slot++;
//                            }
//                        }
//                        if(slot < 20){
//                            bugFixTab(p, slot,1,ChatColor.DARK_AQUA + "Members:", playercount);
//                            slot++;
//                        }
//                        for(String member : members){
//                            if(slot < 20){
//                                bugFixTab(p, slot,1,ChatColor.DARK_AQUA + member, playercount);
//                                slot++;
//                            }
//                        }
//                    }




                    //Stats
                    bugFixTab(p, 4,3,ChatColor.AQUA + "Alignment:", playercount);
                    bugFixTab(p, 5,3, StatsMain.getAlignment(p) + " " + ((StatsMain.getAlignTime(p) > 0) ? Integer.toString(StatsMain.getAlignTime(p)) + "s" : ""), playercount);
                    bugFixTab(p, 6,3,ChatColor.YELLOW + "DPS: " + Integer.toString(TabUtil.getDPS(p)), playercount);
                    bugFixTab(p, 7,3,ChatColor.YELLOW + "ARMOR: " + Integer.toString(TabUtil.getArmor(p)), playercount);
                    bugFixTab(p, 8,3,ChatColor.YELLOW + "ENERGY: " + Integer.toString(TabUtil.getEnergy(p)), playercount);
                    bugFixTab(p, 9,3,ChatColor.YELLOW + "HP/s: " + Integer.toString(TabUtil.getHPS(p)), playercount);
                    bugFixTab(p, 10,3,ChatColor.RED + "PKs: " + Integer.toString(guildPlayer.getPlayerKills()), playercount);
                    bugFixTab(p, 11,3,ChatColor.RED + "Deaths: " + Integer.toString(guildPlayer.getDeaths()), playercount);
                    bugFixTab(p, 12,3,ChatColor.GRAY + "Mined: " + Integer.toString(guildPlayer.getOreMined()), playercount);
                    bugFixTab(p, 13,3,ChatColor.GREEN + "Mob Kills:", playercount);
                    bugFixTab(p, 14,3,ChatColor.GREEN + "T1: " + Integer.toString(guildPlayer.getT1Kills()), playercount);
                    bugFixTab(p, 15,3,ChatColor.GREEN + "T2: " + Integer.toString(guildPlayer.getT2Kills()), playercount);
                    bugFixTab(p, 16,3,ChatColor.GREEN + "T3: " + Integer.toString(guildPlayer.getT3Kills()), playercount);
                    bugFixTab(p, 17,3,ChatColor.GREEN + "T4: " + Integer.toString(guildPlayer.getT4Kills()), playercount);
                    bugFixTab(p, 18,3,ChatColor.GREEN + "T5: " + Integer.toString(guildPlayer.getT5Kills()), playercount);
                    bugFixTab(p, 19,3,ChatColor.GREEN + "T6: " + Integer.toString(guildPlayer.getT6Kills()), playercount);

                    int ping =((CraftPlayer)p).getHandle().ping;
                    bugFixTab(p, 18,2,ChatColor.GREEN + "Ping: " + Integer.toString(ping), playercount);
                    //bugFixTab(p, 4,3,ChatColor.AQUA + "Alignment:", 1, WrappedGameProfile.fromOfflinePlayer(Bukkit.getServer().getOfflinePlayer(UUID.fromString("53dc6122-7281-48c5-82a6-70208db4b501"))));
                    TabAPI.updatePlayer(p);
                }
            }
        }, 0, 40);
    }
}
