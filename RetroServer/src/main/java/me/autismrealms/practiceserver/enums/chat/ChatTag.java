package me.autismrealms.practiceserver.enums.chat;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by Giovanni on 3-5-2017.
 */
public enum ChatTag {

    DEFAULT("$none", false),
    FANATICAL("&e&lFANATICAL&r", true),
    NUTS("&b&lNUTS&r", true),
    PIMP("&b&lPIMP&r", true),
    EZ_PK("&c&lEZBRPK&r", true),
    LOLWTF("&d&lL&a&LO&d&lL&a&lW&d&lT&a&lF&d&l!!!?&r", true),
    TROLL("&9&lTRO&a&lLL&r", true),
    MEMER("&9&lM&9E&9&lM&9E&9&lR&r", true),
    ATLAS("&c&lATLAS&r", true),
    DABBER("&5&l<o/&r", true),
    THINKING("&6&l:THINKING:", true),
    SILVER("&7&lSILVER", true),
    GAY("&d&lG&e&lA&a&lY", true),
    EGIRL("&d&lEGIRL", true),
    DRPRO("&6&lDR&d&lPRO", true),
    CODER("&3&lCODER", true),
    HACKER("&9&lHACKER?!?!", true),
    TRANNY("&a&lTRANNY", true),
    COOLKID("&c&lCOOL&7-&b&lKID", true),
    TRUMP("&e&lTRUMP", true),
    LIBTARD("&b&lL&e&lI&a&lB&c&lT&d&lA&7&lR&6&lD", true),
    BULLY("&7&lBULLY", true),
    iFamasssxD("&6&liFamasssxD", true),
    SUICIDAL("&a&lS&b&lUICIDAL", true),
    SchoolShooter("&c&lSchool-Shooter", true),
    DADDY("&b&lD&ba&b&lD&bd&b&lY", true),
    SHERIFF("&6✪", true),
    EZNAPK("&7&lEZNAPK", true),
    GREASE("&e&lG&6&lR&e&lE&6&lA&e&lS&6&lE", true),
    TUFFY("&c&lT&b&lU&a&lF&e&lF&6&lY", true),
    UGANDA("&6&lUGANDA", true),
    FAVELLA("&a&lF&e&lA&3&lV&a&lE&e&lL&3&lL&a&lA", true),
    YikesYouDied("&d&lYikesYouDied", true),
    MOD("&cM&9O&eD", true);

    private String tag;
    private boolean storeInstance;

    ChatTag(String tag, boolean storeInstance) {
        this.tag = tag;
        this.storeInstance = storeInstance;
    }

    public String getTag() {
        return tag;
    }

    public boolean isStoreInstance() {
        return storeInstance;
    }

    public static Stream<ChatTag> stream() {
        return Arrays.stream(values());
    }
}
