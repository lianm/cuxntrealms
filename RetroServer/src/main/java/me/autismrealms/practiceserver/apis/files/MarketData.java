package me.autismrealms.practiceserver.apis.files;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.utils.AbstractFile;

import java.io.File;

/**
 * Created by Khalid on 8/3/2017.
 */
public class MarketData extends AbstractFile {
    public MarketData(PracticeServer main) {
        super(main, "marketdata.json", false);
    }

    public File getFile() {
        return f;
    }
}
