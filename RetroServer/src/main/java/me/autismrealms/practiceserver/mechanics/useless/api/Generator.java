package me.autismrealms.practiceserver.mechanics.useless.api;

/**
 * Created by Giovanni on 6-5-2017.
 */
public interface Generator<V> {

    V next();
}
