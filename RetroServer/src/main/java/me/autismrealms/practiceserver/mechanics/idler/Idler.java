package me.autismrealms.practiceserver.mechanics.idler;

import me.autismrealms.practiceserver.PracticeServer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;

public class Idler implements Listener {
    public void onEnable() {
        IdlerConfig.setup();
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new IdlerConfig(), PracticeServer.plugin);

        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncRepeatingTask(PracticeServer.plugin, new Runnable() {
            @Override
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()){
                    intervalUpgrade(p, IdlerType.Banker);
                }
            }
        }, 0L, IdlerType.Banker.time);

        scheduler.scheduleSyncRepeatingTask(PracticeServer.plugin, new Runnable() {
            @Override
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()){
                    intervalUpgrade(p, IdlerType.Hunter);
                }
            }
        }, 0L, IdlerType.Hunter.time);

        scheduler.scheduleSyncRepeatingTask(PracticeServer.plugin, new Runnable() {
            @Override
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()){
                    intervalUpgrade(p, IdlerType.Fisherman);
                }
            }
        }, 0L, IdlerType.Fisherman.time);

        scheduler.scheduleSyncRepeatingTask(PracticeServer.plugin, new Runnable() {
            @Override
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()){
                    intervalUpgrade(p, IdlerType.Miner);
                }
            }
        }, 0L, IdlerType.Miner.time);

    }

    public void intervalUpgrade(Player p, IdlerType type){
        int amt = IdlerConfig.get().getInt(p.getUniqueId() + type.path + "amt");
        int level = IdlerConfig.get().getInt(p.getUniqueId() + type.path + "level");
        IdlerConfig.get().set(p.getUniqueId() + type.path + "amt", amt + level);
    }

    public void onDisable(){
        System.out.println("[Idler] Disabled");
    }

}
