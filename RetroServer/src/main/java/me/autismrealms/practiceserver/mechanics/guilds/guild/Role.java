package me.autismrealms.practiceserver.mechanics.guilds.guild;

public enum Role {
    MEMBER, OFFICER, LEADER;
}
