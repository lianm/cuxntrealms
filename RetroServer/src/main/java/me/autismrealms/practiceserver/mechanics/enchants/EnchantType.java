package me.autismrealms.practiceserver.mechanics.enchants;

import me.autismrealms.practiceserver.mechanics.drops.generate.GenerateOrb;
import me.autismrealms.practiceserver.mechanics.item.ItemType;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public enum EnchantType {

    LIFE_STEAL(ItemType.WEAPON),
    POISON_DMG(ItemType.WEAPON),
    PURE_DMG(ItemType.WEAPON),
    CRITICAL_HIT(ItemType.WEAPON),
    ICE_DMG(ItemType.WEAPON),
    FIRE_DMG(ItemType.WEAPON),
    VS_MONSTERS(ItemType.WEAPON),
    VS_PLAYERS(ItemType.WEAPON),
    ACCURACY(ItemType.WEAPON),
    VIT(ItemType.ARMOR),
    DEX(ItemType.ARMOR),
    INT(ItemType.ARMOR),
    STR(ItemType.ARMOR),
    BLOCK(ItemType.ARMOR),
    DODGE(ItemType.ARMOR);

    public String enchName;
    public ItemType itemType;

    EnchantType(ItemType itemType) {
        this.itemType = itemType;
        this.enchName = ChatColor.RED + this.name().replace("_", " ");
    }

    public static EnchantType getRandomEnchant(ItemType type) {
        EnchantType newitem = EnchantType.values()[new Random().nextInt(EnchantType.values().length)];
        if (newitem.itemType.equals(type)) {
            return newitem;
        } else {
            return null;
        }
    }

    public static List<EnchantType> createEnchList(ItemType itemType, boolean leg) {
        List<EnchantType> lore = new ArrayList<>();
        for(EnchantType ench : EnchantType.values()){
            int mult = new Random().nextInt(4);
            if (leg) {
                mult = new Random().nextInt(2);
            }
            if(mult == 1 && itemType == ench.itemType){
                lore.add(ench);
            }
        }
        return lore;
    }

    public static List<String> combine(ItemType itemType, int tier, boolean leg) {
        List<EnchantType> lore = createEnchList(itemType, leg);
        System.out.println(lore);
        List<String> newLore = new ArrayList<>();
        for (EnchantType line : lore) {
            newLore.add(getOrbValue(line, tier, leg));
        }
        return newLore;
    }

    public static String getOrbValue(EnchantType et, int tier, boolean leg) {
        tier += 1;
        switch (et) {
            case ACCURACY:
                return et.enchName + ": " + GenerateOrb.setAccuracy(tier, leg) + "%";
            case DEX:
            case STR:
            case INT:
            case VIT:
                return et.enchName + ": +" + GenerateOrb.setVit(tier, leg);
            case BLOCK:
            case DODGE:
                return et.enchName + ": " + GenerateOrb.setDodge(tier, leg) + "%";
            case FIRE_DMG:
            case PURE_DMG:
            case POISON_DMG:
            case ICE_DMG:
                return et.enchName + ": +" + GenerateOrb.setElement(tier, leg);
            case CRITICAL_HIT:
            case LIFE_STEAL:
            case VS_MONSTERS:
            case VS_PLAYERS:
                return et.enchName + ": " + GenerateOrb.setLifeSteal(tier, leg) + "%";
        }
        return null;
    }
}
