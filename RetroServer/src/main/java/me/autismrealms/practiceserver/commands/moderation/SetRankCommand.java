package me.autismrealms.practiceserver.commands.moderation;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.enums.ranks.RankEnum;
import me.autismrealms.practiceserver.mechanics.donations.Crates.CratesMain;
import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.moderation.ModerationMechanics;
import me.autismrealms.practiceserver.mechanics.money.GemPouches;
import me.autismrealms.practiceserver.mechanics.player.GamePlayer.StaticConfig;
import me.autismrealms.practiceserver.mechanics.pvp.Alignments;
import me.autismrealms.practiceserver.utils.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class SetRankCommand implements CommandExecutor {

    public void incorrectArgs(CommandSender sender) {
        sender.sendMessage(ChatColor.RED.toString() + "/setrank <PLAYER> <RANK>");
        sender.sendMessage(
                ChatColor.RED + "Ranks: " + ChatColor.GRAY + "Default | SUB | SUB+ | SUB++ | SUB+++ | SUPPORTER | PMOD | BUILDER | YOUTUBER");
    }
/*:thinking: F I X E D*/
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof ConsoleCommandSender || sender.isOp()) {
            if (args.length == 2) {
                Player p = Bukkit.getPlayer(args[0]);
                String rankString = args[1].toLowerCase();
                switch (rankString) {
                    case "pmod":
                    case "sub":
                    case "supporter":
                    case "sub+":
                    case "sub++":
                    case "sub+++":
                    case "youtuber":
                    case "quality":
                    case "builder":
                    case "gm":
                    case "manager":
                    case "dev":
                    case "default":
                        if (Bukkit.getServer().getPlayer(p.getUniqueId()) != null) {
                            RankEnum rank = RankEnum.fromString(rankString);
                            ModerationMechanics.rankHashMap.put(p.getUniqueId(), rank);
                            if (sender instanceof Player) {
                                StringUtil.sendCenteredMessage((Player) sender, ChatColor.GREEN + "You have set " + p.getName() + "'s rank to " + rank);
                            }
                            StaticConfig.get().set(p.getUniqueId() + ".Main.Rank", rank.id);
                            StaticConfig.save();
                            switch(rank){
                                case SUB:
                                    p.getInventory().addItem(CratesMain.createCrate(2, false));
                                    break;
                                case SUB1:
                                    p.getInventory().addItem(Items.subPick());
                                    p.getInventory().addItem(GemPouches.gemPouch(6));
                                    p.getInventory().addItem(CratesMain.createCrate(3, false));
                                    p.getInventory().addItem(PracticeServer.buffHandler().newBuffItem(p.getName(), p.getUniqueId(), 50));
                                    break;
                                case SUB2:
                                    p.getInventory().addItem(Items.subplusPick());
                                    p.getInventory().addItem(CratesMain.createCrate(3, false));
                                    p.getInventory().addItem(CratesMain.createCrate(3, false));
                                    p.getInventory().addItem(CratesMain.createCrate(3, false));
                                    p.getInventory().addItem(GemPouches.gemPouch(6));
                                    p.getInventory().addItem(GemPouches.gemPouch(6));
                                    p.getInventory().addItem(PracticeServer.buffHandler().newBuffItem(p.getName(), p.getUniqueId(), 100));
                                    break;
                                case SUPPORTER:
                                    p.getInventory().addItem(Items.supporterPick());
                                    p.getInventory().addItem(CratesMain.createCrate(4, false));
                                    p.getInventory().addItem(CratesMain.createCrate(4, false));
                                    p.getInventory().addItem(GemPouches.gemPouch(6));
                                    p.getInventory().addItem(GemPouches.gemPouch(6));
                                    p.getInventory().addItem(GemPouches.gemPouch(6));
                                    p.getInventory().addItem(PracticeServer.buffHandler().newBuffItem(p.getName(), p.getUniqueId(), 200));
                                    break;
                            }
                            
                            Alignments.updatePlayerAlignment(p);
                        }
                        break;
                    default:
                        if (sender instanceof Player) {
                            incorrectArgs(sender);
                        }
                }
            } else {
                if (sender instanceof Player) {
                    incorrectArgs(sender);
                }
            }
        }
        return false;
    }
}