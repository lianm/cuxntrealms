package me.autismrealms.practiceserver.mechanics.progress;

import me.autismrealms.practiceserver.PracticeServer;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class Progress {

    public void onEnable(){
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new ProgressionConfig(), PracticeServer.plugin);
        ProgressionConfig.setup();
    }

    public void onDisable(){
        ProgressionConfig.save();
    }

}
