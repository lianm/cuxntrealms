package me.autismrealms.practiceserver.commands.items;

import me.autismrealms.practiceserver.mechanics.bosses.rune.FactoryRune;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GiveRune implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;
        int tier = Integer.parseInt(args[0]);
        p.getInventory().addItem(FactoryRune.createRune(tier));
        return false;
    }

}
