package me.autismrealms.practiceserver.mechanics.item.scroll;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.apis.itemapi.ItemAPI;
import me.autismrealms.practiceserver.apis.itemapi.NBTAccessor;
import me.autismrealms.practiceserver.mechanics.money.Money;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Giovanni on 7-5-2017.
 */
public class ScrollGUIHandler implements Listener {

    public void onEnable(){
        Bukkit.getServer().getPluginManager().registerEvents(this, PracticeServer.plugin);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if (!event.getInventory().getName().toLowerCase().contains("dungeoneer")) return;

        event.setCancelled(true);

        ItemStack itemStack = event.getCurrentItem();

        if (itemStack != null && itemStack.getType() != null && itemStack.getType() != Material.AIR && itemStack.getType() == Material.EMPTY_MAP) {
            NBTAccessor nbtAccessor = new NBTAccessor(itemStack).check();

            if (!nbtAccessor.hasKey("guiPrice")) return;

            int price = nbtAccessor.getInteger("guiPrice");
            int tier = nbtAccessor.getInteger("guiTier");

            int tierMessage = tier + 1;

            if (tier == 5 || price == -1) return;

            if (!Money.hasEnoughGems(player, price)) {
                player.closeInventory();
                player.sendMessage(ChatColor.RED + "Not enough funds to purchase a " + ChatColor.BOLD + "T" + tierMessage + " PROTECTION SCROLL!");
                player.sendMessage(ChatColor.RED + "Required funds: " + price);

                return;
            }

            player.closeInventory();
            player.getInventory().addItem(ItemAPI.getScrollGenerator().next(tier));

            Money.takeGems(player, price);

            player.sendMessage(ChatColor.GREEN + "You've purchased a T" + ChatColor.BOLD + tierMessage + " PROTECTION SCROLL!");
            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10F, 0.2F);

        }
    }
}
