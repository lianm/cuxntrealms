package me.autismrealms.practiceserver.apis.tab;

import codecrafter47.bungeetablistplus.api.bukkit.Variable;

import java.util.List;

/**
 * Created by Giovanni on 23-7-2017.
 */
public interface Frame {

    void function();

    List<Variable> getVariables();
}
