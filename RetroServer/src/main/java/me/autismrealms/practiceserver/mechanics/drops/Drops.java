/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package me.autismrealms.practiceserver.mechanics.drops;

import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class Drops {
    public static ItemStack createDrop(int tier, int item) {
        int rarity = randomRarity();
        return CreateDrop.createDrop(tier, item, rarity);

    }
    public static int randomRarity(){
        Random random = new Random();
        int rarity = 0;
        int r = random.nextInt(80);
        if (r < 1) {
            rarity = 3;
        } else if (r < 5) {
            rarity = 2;
        } else if (r < 20) {
            rarity = 1;
        } else if (r < 80) {
            rarity = 0;
        }
        return rarity;
    }

}