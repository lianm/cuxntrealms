package me.autismrealms.practiceserver.mechanics.mobs;

import me.autismrealms.practiceserver.mechanics.mobs.elite.EliteType;
import org.bukkit.ChatColor;

import java.util.Random;

public class NameGenerator {

    public static String create(int tier, String type){

        String name= "";

        if (type.equals("weakSkeletonEntity") || type.equals("weakSkeletonEntity_UV")) {
            int id = new Random().nextInt(3);

            switch (id) {
                case 0:
                    name = ChatColor.RED + "Infernal Skeletal Keeper";
                    break;
                case 1:
                    name = ChatColor.RED + "Skeletal Soul Keeper";
                    break;
                case 2:
                    name = ChatColor.RED + "Skeletal Soul Harvester";
                    break;
                case 3:
                    name = ChatColor.RED + "Infernal Skeletal Soul Harvester";
                    break;
            }
        }

        if (type.equals("skellyDSkeletonGuardian")) {

            int id = new Random().nextInt(2);

            switch (id) {
                case 0:
                    name = ChatColor.LIGHT_PURPLE + "Skeletal Guardian Deadlord";
                    break;
                case 1:
                    name = ChatColor.LIGHT_PURPLE + "Skeletal Guardian Overlord";
                    break;
                case 2:
                    name = ChatColor.LIGHT_PURPLE + "Restless Skeletal Guardian";
                    break;
            }
        }

        if (type.equalsIgnoreCase("prisoner")) {
            int id = new Random().nextInt(2);

            switch (id) {
                case 0:
                    name = ChatColor.RED + "Tortured Prisoner";
                    break;
                case 1:
                    name = ChatColor.RED + "Corrupted Prison Guard";
                    break;
                case 2:
                    name = ChatColor.RED + "Tortmented Guard";
                    break;
            }
        }


        if (type.equalsIgnoreCase("spectralGuard")) {
            name = "The Evil Spectral's Impish Guard";
        }

        //Search MobTypes to find name
        for(MobType mobType : MobType.values()){
            System.out.println(type + ", " + mobType.id);
            if (mobType.id.equalsIgnoreCase(type)){
                name = mobType.names.get(tier) + " " + mobType.suffix;
            }
        }

        //Search Elites to find name from EliteType
        for(EliteType eliteType : EliteType.values()){
            if(type.equalsIgnoreCase(eliteType.id)){
                name = eliteType.name;
            }
        }

        if (type.equals(EliteType.Deathlord.id)) {
            name = ChatColor.DARK_RED + "The Restless Skeleton Deathlord";
        }

        return name;
    }

}
