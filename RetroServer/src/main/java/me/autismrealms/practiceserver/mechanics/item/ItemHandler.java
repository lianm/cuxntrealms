package me.autismrealms.practiceserver.mechanics.item;

import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ItemHandler {

    public static String[] rarity = {"Common", "Uncommon", "Rare", "Unique"};
    public static ChatColor[] rarityColors = {ChatColor.GRAY, ChatColor.GREEN, ChatColor.AQUA, ChatColor.YELLOW, ChatColor.DARK_PURPLE};

    public static String setName(int item, int tier){
        return Tiers.colors[tier] + generateNamePrefix(item, Tiers.prefix[tier]);
    }

    public static boolean isPlussed(String name){
        if (ChatColor.stripColor(name).startsWith("[+")) {
            return true;
        }
        return false;
    }

    public static String setRarityInt(int tier){
        return rarityColors[tier] + rarity[tier];
    }

    public static String nameFactory(Material mat, String itemName, List<String> lore){
        int tier = Tiers.getStringTier(itemName);
        String plus = "";
        if(isPlussed(itemName)){
            String s = itemName.split("\\[+")[1].split("\\]")[0];
            int amt = Integer.parseInt(s);
            plus = ChatColor.RED + "[+" + amt + "] ";
        }
        System.out.println(plus);
        String name = setName(getItemType(mat), tier);

        for(String line : lore){
            if (line.contains("PURE DMG: +")) name = "Pure " + name;
            if (line.contains("ACCURACY: ")) name = "Accurate " + name;
            if (line.contains("LIFE STEAL: ")) name = "Vampyric " + name;
            if (line.contains("CRITICAL HIT: ")) name = "Deadly " + name;
            if (line.contains("ICE DMG: +")) name = String.valueOf(name) + " of Ice";
            if (line.contains("POISON DMG: +")) name = String.valueOf(name) + " of Poison";
            if (line.contains("FIRE DMG: +")) name = String.valueOf(name) + " of Fire";
        }

        name = ChatColor.stripColor(name);

        return plus + Tiers.colors[tier] + name;

    }

    public static int getItemType(Material mat) {
        if (mat.name().contains("_HOE")) {
            return 0;
        }
        if (mat.name().contains("_SPADE")) {
            return 1;
        }
        if (mat.name().contains("_SWORD")) {
            return 2;
        }
        if (mat.name().contains("_AXE")) {
            return 3;
        }
        if (mat.name().contains("_HELMET")) {
            return 4;
        }
        if (mat.name().contains("_CHESTPLATE")) {
            return 5;
        }
        if (mat.name().contains("_LEGGINGS")) {
            return 6;
        }
        if (mat.name().contains("_BOOTS")) {
            return 7;
        }
        return -1;
    }

    public static String generateNamePrefix(int item, String name){
        switch (item) {
            case 0: return name + " Staff";
            case 1: return name + " Polearm";
            case 2: return name + " Sword";
            case 3: return name + " Axe";
            case 4: return name + " Platemail Helmet";
            case 5: return name + " Platemail";
            case 6: return name + " Platemail Leggings";
            case 7: return name + " Platemail Boots";
        }
        return "error in generateNamePrefix()";
    }

    //Set Item Type
    public static String getItem(int item){
        switch (item) {
            case 0: return "HOE";
            case 1: return "SPADE";
            case 2: return "SWORD";
            case 3: return "AXE";
            case 4: return "HELMET";
            case 5: return "CHESTPLATE";
            case 6: return "LEGGINGS";
            case 7: return "BOOTS";
        }
        return "error";
    }

    //Set Items Material
    public static String getMatString(int tier, int item){
        switch (tier) {
            case 1:
                if(item >= 4){
                    return "LEATHER";
                }else{
                    return "WOOD";
                }
            case 2:
                if(item >= 4){
                    return "CHAINMAIL";
                }else{
                    return "STONE";
                }
            case 3: return "IRON";
            case 4: return "DIAMOND";
            case 5: return "GOLD";
            case 6:
                if(item >= 4){
                    return "LEATHER";
                }else{
                    return "DIAMOND";
                }
            case 7:
                if(item >= 4){
                    return "LEATHER";
                }else{
                    return "IRON";
                }
            case 8:
                if(item >= 4){
                    return "LEATHER";
                }else{
                    return "WOOD";
                }
            case 9:
                if(item >= 4){
                    return "LEATHER";
                }else{
                    return "GOLD";
                }
            case 10:
                if(item >= 4){
                    return "LEATHER";
                }else{
                    return "STONE";
                }
        }
        return "error";
    }

    public static ChatColor setColor(int tier){
        return Tiers.colors[tier];
    }

    public static boolean isGear(ItemStack is){
        if(
                is.getType().name().contains("BOOTS") ||
                        is.getType().name().contains("CHESTPLATE") ||
                        is.getType().name().contains("LEGGINGS") ||
                        is.getType().name().contains("BOOTS") ||
                        is.getType().name().contains("SWORD") ||
                        is.getType().name().contains("AXE") ||
                        is.getType().name().contains("HOE") ||
                        is.getType().name().contains("SPADE"))
        {
            return true;
        }
        return false;
    }


}
