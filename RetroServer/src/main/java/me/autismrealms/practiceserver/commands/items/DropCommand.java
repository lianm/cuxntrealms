package me.autismrealms.practiceserver.commands.items;

import me.autismrealms.practiceserver.mechanics.drops.generate.GenerateDrop;
import me.autismrealms.practiceserver.mechanics.moderation.ModerationMechanics;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class DropCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;
        if(!sender.isOp() || sender instanceof ConsoleCommandSender) return false;
        ModerationMechanics.ImportantStaff().forEach(staffMember -> {
            if (!sender.getName().equalsIgnoreCase(staffMember) || !(sender instanceof ConsoleCommandSender)) return;
        });
        if (args.length == 3) {
            try {
                p.getInventory().addItem(new ItemStack(GenerateDrop.createDrop(Integer.parseInt(args[0]),
                        Integer.parseInt(args[1]), Integer.parseInt(args[2]))));
            } catch (Exception e2) {
                e2.printStackTrace();
                p.sendMessage(String.valueOf(ChatColor.RED) + ChatColor.BOLD +
                        "Incorrect Syntax: " + ChatColor.RED +
                        "/createdrop <tier> <item> <rarity>");

            }
        } else {
            p.sendMessage(
                    String.valueOf(ChatColor.RED) + ChatColor.BOLD + "Incorrect Syntax: " +
                            ChatColor.RED + "/drop <tier> <item> <rarity>");
        }

        return false;
    }

}
