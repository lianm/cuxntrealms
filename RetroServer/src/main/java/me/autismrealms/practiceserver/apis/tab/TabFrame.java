package me.autismrealms.practiceserver.apis.tab;

import codecrafter47.bungeetablistplus.api.bukkit.Variable;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giovanni on 23-7-2017.
 */
public abstract class TabFrame implements Frame {

    @Getter /* Declare weak to prevent memory leaks, as in when a variable is registered and unused, it gets garbage collected. */
    protected final List<Variable> variables = new ArrayList<>();
}
