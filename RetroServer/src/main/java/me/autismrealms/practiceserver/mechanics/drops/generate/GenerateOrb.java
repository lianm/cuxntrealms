package me.autismrealms.practiceserver.mechanics.drops.generate;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.enchants.EnchantType;
import me.autismrealms.practiceserver.mechanics.enchants.GenerateEnchant;
import me.autismrealms.practiceserver.mechanics.item.ItemHandler;
import me.autismrealms.practiceserver.mechanics.item.ItemType;
import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import me.autismrealms.practiceserver.utils.Particles;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateOrb implements Listener {

    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, PracticeServer.plugin);
    }

    public enum Type {

        LEGENDARY(ChatColor.YELLOW + "Legendary Orb of Alteration"),
        NORMAL(ChatColor.LIGHT_PURPLE + "Orb of Alteration");

        public String color;
        public String name;

        Type(String name){
            this.name = name;
        }

        public static Type getOrbType(String orbName){
            for(Type orb : Type.values()){
                if(orbName.contains(orb.name)){
                    return orb;
                }
            }
            return null;
        }

    }

    @EventHandler
    public void OrbApplyEvent(final InventoryClickEvent e) {

        ItemStack orb = e.getCursor();
        ItemStack itemStack = e.getCurrentItem();

        if(itemStack != null && itemStack.getType() != Material.AIR && orb.getType() == Material.MAGMA_CREAM) {
            int item = Items.getItemType(itemStack.getType());

            Type orbType = Type.getOrbType(orb.getItemMeta().getDisplayName());
            ItemType itemType = ItemType.getType(item);

            if (orbType != null && itemType != null) {
                e.setCancelled(true);

                boolean leg = false;
                if (orbType == Type.LEGENDARY) {
                    leg = true;
                }

                orbItem(itemStack, leg);

                if (e.getCursor().getAmount() > 1) {
                    e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                } else if (e.getCursor().getAmount() == 1) {
                    e.setCursor(null);
                }

                //Plus 4s
                if(leg) {
                    GenerateEnchant.enchantItem(orbType, itemType, itemStack);
                }
                ItemMeta im = itemStack.getItemMeta();
                im.setDisplayName(ItemHandler.nameFactory(itemStack.getType(), im.getDisplayName(), im.getLore()));
                itemStack.setItemMeta(im);
                e.setCurrentItem(itemStack);
            }
        }
    }

    public static ItemStack orbItem(ItemStack item, boolean legendary) {
        ItemType itemType = ItemType.getType(Items.getItemType(item.getType()));

        ItemMeta itemMeta = item.getItemMeta();

        List<String> oldlore = item.getItemMeta().getLore();

        int rar = Items.getRarityFromItem(item);
        int tier = Tiers.getStringTier(item.getItemMeta().getDisplayName());
        List<String> lore = new ArrayList<>();

        if(itemType == ItemType.WEAPON) {
            lore.add(itemMeta.getLore().get(0));
        }else{
            lore.add(itemMeta.getLore().get(0));
            lore.add(itemMeta.getLore().get(1));
            lore.add(itemMeta.getLore().get(2));
        }

        for (String line : EnchantType.combine(itemType, tier, legendary)) {
            lore.add(line);
        }

        lore.add(ItemHandler.rarityColors[rar] + Items.rarity[rar]);
        itemMeta.setDisplayName(ItemHandler.nameFactory(item.getType(), itemMeta.getDisplayName(), lore));
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);

        return item;
    }

    public static void success(Player p, int newsize, int oldsize){
        if (newsize > oldsize) {
            //create firework
            p.getWorld().playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.25f);
            final Firework fw = (Firework) p.getWorld().spawnEntity(p.getLocation(), EntityType.FIREWORK);
            final FireworkMeta fwm = fw.getFireworkMeta();
            final FireworkEffect effect = FireworkEffect.builder().flicker(false).withColor(Color.YELLOW).withFade(Color.YELLOW).with(FireworkEffect.Type.BURST).trail(true).build();
            fwm.addEffect(effect);
            fwm.setPower(0);
            fw.setFireworkMeta(fwm);
        } else {
            p.getWorld().playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 2.0f, 1.25f);
            Particles.LAVA.display(0.0f, 0.0f, 0.0f, 5.0f, 10, p.getEyeLocation(), 20.0);
        }
    }

    public static boolean isOrb(ItemStack is){
        if(is != null && is.getType() == Material.MAGMA_CREAM) {
            Type orbType = Type.getOrbType(is.getItemMeta().getDisplayName());
            return orbType == Type.NORMAL || orbType == Type.LEGENDARY;
        }
        return false;
    }

    public static int setHps(int t){
        double hps = 7D*(t*t) + (5D*t);
        return new Random().nextInt((int)hps/4) + (int)hps;
    }

    public static int setDps(int t){
        return (int) (-0.05D*(t*t) + (3.5D*t));
    }

    public static int setAccuracy(int tier, boolean leg){
        int orb = (6*tier) +4;
        return roll(orb, leg);
    }

    public static int setEnergy(int t){
        return new Random().nextInt(3) + t +1;
    }

    public static int setDodge(int t, boolean leg){
        double dodge = (-0.01D*(t*t)+(0.25D*t) +0.2D)*10D;
        return roll((int)dodge, leg);
    }

    public static int setVit(int t, boolean leg){
        double vit = (0.1D*(t*t)+(0.01D*t)+0.05D)*100D;
        return roll((int)vit, leg);
    }

    public static int setElement(int t, boolean leg){

        double ele = (0.18D*(t*t) + 0.22D)*10D;
        return roll((int)ele, leg);
    }

    public static int setLifeSteal(int t, boolean leg){
        double life = (13 - t) * 1.8D;
        return roll((int)life, leg);
    }

    public static int setVs(int t, boolean leg){
        double vs = (2D*t)+2.5D;
        return roll((int)vs, leg);
    }

    public static int setThorns(int t){
        return t+1;
    }

    public static int roll(int stat, boolean leg){
        if(leg){
            return new Random().nextInt(stat/3) + (int) (stat/1.2D);
        }else{
            return new Random().nextInt(stat) + 1;
        }
    }

}
