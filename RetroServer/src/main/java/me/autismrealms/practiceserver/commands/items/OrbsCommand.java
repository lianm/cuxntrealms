package me.autismrealms.practiceserver.commands.items;

import me.autismrealms.practiceserver.apis.tab.TabUtil;
import me.autismrealms.practiceserver.enums.ranks.RankEnum;
import me.autismrealms.practiceserver.mechanics.item.Durability;
import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.moderation.ModerationMechanics;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.stream.IntStream;

/**
 * Created by Matthew E on 8/10/2017.
 */
public class OrbsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if ((ModerationMechanics.isDonator(player) || ModerationMechanics.isStaff(player)) || ModerationMechanics.getRank(player) == RankEnum.BUILDER && ModerationMechanics.getRank(player) != RankEnum.SUB)  {
                HashMap<Integer, ? extends ItemStack> diamondOre = player.getInventory().all(Material.DIAMOND_ORE);
                HashMap<Integer, ? extends ItemStack> ironOre = player.getInventory().all(Material.IRON_ORE);
                HashMap<Integer, ? extends ItemStack> goldOre = player.getInventory().all(Material.GOLD_ORE);
                HashMap<Integer, ? extends ItemStack> frozenOre = player.getInventory().all(Material.LAPIS_ORE);
                HashMap<Integer, ? extends ItemStack> ironScrap = player.getInventory().all(Durability.scrap(3));
                HashMap<Integer, ? extends ItemStack> diamondScrap = player.getInventory().all(Durability.scrap(4));
                HashMap<Integer, ? extends ItemStack> goldScrap = player.getInventory().all(Durability.scrap(5));
                HashMap<Integer, ? extends ItemStack> frozenScrap = player.getInventory().all(Durability.scrap(6));
                int orbAmount = 0;
                int ironScrapAmount = 0;
                int diamondScrapAmount = 0;
                int goldScrapAmount = 0;
                int frozenScrapAmount = 0;
                int toRemoveIronOre = 0;
                int toRemoveDiamondOre = 0;
                int toRemoveGoldOre = 0;
                int toRemoveFrozenOre = 0;
                for (ItemStack itemStack : ironOre.values()) {
                    ironScrapAmount += (2 * itemStack.getAmount());
                    toRemoveIronOre += itemStack.getAmount();
                }
                for (ItemStack itemStack : diamondOre.values()) {
                    diamondScrapAmount += (2 * itemStack.getAmount());
                    toRemoveDiamondOre += itemStack.getAmount();
                }
                for (ItemStack itemStack : goldOre.values()) {
                    goldScrapAmount += (2 * itemStack.getAmount());
                    toRemoveGoldOre += itemStack.getAmount();
                }
                for (ItemStack itemStack : frozenOre.values()) {
                    frozenScrapAmount += (2 * itemStack.getAmount());
                    toRemoveFrozenOre += itemStack.getAmount();
                }
                removeAmountOfOre(player, Material.IRON_ORE, toRemoveIronOre);
                removeAmountOfOre(player, Material.DIAMOND_ORE, toRemoveDiamondOre);
                removeAmountOfOre(player, Material.GOLD_ORE, toRemoveGoldOre);
                removeAmountOfOre(player, Material.LAPIS_ORE, toRemoveFrozenOre);
                while (ironScrapAmount >= 120) {
                    ironScrapAmount -= 120;
                    orbAmount++;
                }
                while (diamondScrapAmount >= 60) {
                    diamondScrapAmount -= 60;
                    orbAmount++;
                }
                while (goldScrapAmount >= 20) {
                    goldScrapAmount -= 20;
                    orbAmount++;
                }
                while (frozenScrapAmount >= 10) {
                    frozenScrapAmount -= 10;
                    orbAmount++;
                }
                if (ironScrapAmount > 0) {
                    ItemStack scrap = Durability.scrap(3);
                    scrap.setAmount(ironScrapAmount);
                    player.getInventory().addItem(scrap);
                }
                if (diamondScrapAmount > 0) {
                    ItemStack scrap = Durability.scrap(4);
                    scrap.setAmount(diamondScrapAmount);
                    player.getInventory().addItem(scrap);
                }
                if (goldScrapAmount > 0) {
                    ItemStack scrap = Durability.scrap(5);
                    scrap.setAmount(goldScrapAmount);
                    player.getInventory().addItem(scrap);
                }
                if (frozenScrapAmount > 0) {
                    ItemStack scrap = Durability.scrap(6);
                    scrap.setAmount(frozenScrapAmount);
                    player.getInventory().addItem(scrap);
                }
                IntStream.range(0, orbAmount).forEach(consumer -> {
                    player.getInventory().addItem(Items.orb(false));
                });
                player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.RED + TabUtil.format(toRemoveIronOre) + "x T3 Ore > " + TabUtil.format((toRemoveIronOre * 2) / 120) + "x orbs");
                player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.RED + TabUtil.format(toRemoveDiamondOre) + "x T4 Ore > " + TabUtil.format((toRemoveDiamondOre * 2) / 60) + "x orbs");
                player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.RED + TabUtil.format(toRemoveGoldOre) + "x T5 Ore > " + TabUtil.format((toRemoveGoldOre * 2) / 20) + "x orbs");
                player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.RED + TabUtil.format(toRemoveFrozenOre) + "x Frozen Ore > " + TabUtil.format((toRemoveFrozenOre * 2) / 10) + "x orbs");
                return true;
            } else {
                player.sendMessage(ChatColor.RED + "You must be " + ChatColor.GOLD + ChatColor.BOLD.toString() + "S+ " + ChatColor.RED + " and up to use /orbs");
                return true;
            }
        }
        return true;
    }

    private void removeAmountOfOre(Player p, Material material, int amt) {
        int i = 0;
        while (i < p.getInventory().getSize()) {
            ItemStack is = p.getInventory().getItem(i);
            if (amt > 0) {
                int val;
                if (is != null && is.getType() == material) {
                    if (amt >= is.getAmount()) {
                        amt -= is.getAmount();
                        p.getInventory().setItem(i, null);
                    } else {
                        is.setAmount(is.getAmount() - amt);
                        amt = 0;
                    }
                }
            }
            ++i;
        }
    }
}
