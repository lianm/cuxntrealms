package me.autismrealms.practiceserver.commands.moderation;

import me.autismrealms.practiceserver.apis.tab.TabUtil;
import me.autismrealms.practiceserver.mechanics.guilds.GuildMechanics;
import me.autismrealms.practiceserver.mechanics.guilds.player.GuildPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Matthew E on 8/10/2017.
 */
public class TopPlayersCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.isOp()) {
                broadcast();
                return true;
            }
        } else if (sender instanceof ConsoleCommandSender) {
            broadcast();
            return true;
        }
        return true;
    }

    private void broadcast() {
        List<GuildPlayer> topGuildPlayerList = new ArrayList<>();
        File file = new File(GuildMechanics.getInstance().getDataFolder() + "/", "players.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        for (String uuid : configuration.getConfigurationSection("players").getKeys(false)) {
            ConfigurationSection section = configuration.getConfigurationSection("players." + uuid);
            String guildName = section.getString("guildName");
            int playerKills = section.getInt("stats.kills.player");
            int t1Kills = section.getInt("stats.kills.t1");
            int t2Kills = section.getInt("stats.kills.t2");
            int t3Kills = section.getInt("stats.kills.t3");
            int t4Kills = section.getInt("stats.kills.t4");
            int t5Kills = section.getInt("stats.kills.t5");
            int t6Kills = section.getInt("stats.kills.t6");
            int lootChestsOpen = section.getInt("stats.lootChests");
            int oreMined = section.getInt("stats.oreMined");
            int deaths = section.getInt("stats.deaths");
            int fishCaught = section.getInt("stats.fishCaught");
            if (playerKills > 5 || (t5Kills > 300) || (oreMined > 300)) {
                topGuildPlayerList.add(new GuildPlayer(UUID.fromString(uuid), section.getString("username"), guildName, playerKills, t1Kills, t2Kills, t3Kills, t4Kills, t5Kills, t6Kills, lootChestsOpen, oreMined, deaths, fishCaught));
            }
        }
        List<GuildPlayer> t6KillsList = new ArrayList<>();
        List<GuildPlayer> oreMinedList = new ArrayList<>();
        List<GuildPlayer> playerKillsList = new ArrayList<>();
        t6KillsList.addAll(topGuildPlayerList);
        playerKillsList.addAll(topGuildPlayerList);
        oreMinedList.addAll(topGuildPlayerList);
        t6KillsList.sort((o1, o2) -> o2.getT6Kills() - o1.getT6Kills());
        oreMinedList.sort((o1, o2) -> o2.getOreMined() - o1.getOreMined());
        playerKillsList.sort((o1, o2) -> o2.getPlayerKills() - o1.getPlayerKills());
        List<GuildPlayer> guildPlayers = playerKillsList.subList(0, 3);
        List<GuildPlayer> guildPlayers1 = oreMinedList.subList(0, 3);
        List<GuildPlayer> guildPlayers2 = t6KillsList.subList(0, 3);
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);

        }
        bc("&e===========[Top Players]===========");
        bc("                &b&lT6 Kills           ");
        bc("           &e&l1. &b" + guildPlayers2.get(0).getUsername() + " &e" + TabUtil.format(guildPlayers2.get(0).getT6Kills()));
        bc("           &e&l2. &b" + guildPlayers2.get(1).getUsername() + " &e" + TabUtil.format(guildPlayers2.get(1).getT6Kills()));
        bc("           &e&l3. &b" + guildPlayers2.get(2).getUsername() + " &e" + TabUtil.format(guildPlayers2.get(2).getT6Kills()));
        bc(" ");
        bc("                &b&lPlayer Kills           ");
        bc("           &e&l1. &b" + guildPlayers.get(0).getUsername() + " &e" + TabUtil.format(guildPlayers.get(0).getPlayerKills()));
        bc("           &e&l2. &b" + guildPlayers.get(1).getUsername() + " &e" + TabUtil.format(guildPlayers.get(1).getPlayerKills()));
        bc("           &e&l3. &b" + guildPlayers.get(2).getUsername() + " &e" + TabUtil.format(guildPlayers.get(2).getPlayerKills()));
        bc(" ");
        bc("               &b&lOres Mined           ");
        bc("           &e&l1. &b" + guildPlayers1.get(0).getUsername() + " &e" + TabUtil.format(guildPlayers1.get(0).getOreMined()));
        bc("           &e&l2. &b" + guildPlayers1.get(1).getUsername() + " &e" + TabUtil.format(guildPlayers1.get(1).getOreMined()));
        bc("           &e&l2. &b" + guildPlayers1.get(2).getUsername() + " &e" + TabUtil.format(guildPlayers1.get(2).getOreMined()));
        bc("&e===========[Top Players]===========");


    }

    private void bc(String message) {
        Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
    }
}

