package me.autismrealms.practiceserver.mechanics.idler;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.donations.Crates.CratesMain;
import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.money.Money;
import me.autismrealms.practiceserver.mechanics.player.Speedfish;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class IdlerConfig implements Listener {

    //save players idler stats in a file

    //get players data

    public void onEnable(){
        setup();
    }

    public void onDisable(){
        save();
    }

    @EventHandler
    public void setupConfigOnJoin(PlayerJoinEvent e) {

        Player p = e.getPlayer();

        if (!p.hasPlayedBefore()) {
            for(IdlerType idler : IdlerType.values()) {
                get().set(p.getUniqueId() + idler.path + "level", 0);
                get().set(p.getUniqueId() + idler.path + "amt", 0);
                save();
            }
        }

    }

    static FileConfiguration gameFile;
    static File gpfile;

    public static void setup() {
        gpfile = new File(PracticeServer.plugin.getDataFolder(), "idler.yml");

        if (!gpfile.exists()) {
            try {
                gpfile.createNewFile();
            } catch (IOException e) {
            }
        }

        gameFile = YamlConfiguration.loadConfiguration(gpfile);
    }

    public static FileConfiguration get() {
        return gameFile;
    }

    public static void save() {
        try {
            gameFile.save(gpfile);
        } catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save idler.yml!");
        }
    }

    public static void reload() {
        gameFile = YamlConfiguration.loadConfiguration(gpfile);
    }

    @EventHandler
    public void onMerchantClick(PlayerInteractEntityEvent e) {
        if ((e.getRightClicked() instanceof HumanEntity)) {
            HumanEntity p = (HumanEntity) e.getRightClicked();
            for(IdlerType type : IdlerType.values()){
                boolean killedElite = IdlerType.hasRequirement(type.requirement, e.getPlayer());
                if (p.getName().equals(type.name)) {
                    if(killedElite) {
                        openUpgrade(e.getPlayer(), type);
                    }else{
                        e.getPlayer().sendMessage(ChatColor.GRAY + "You have to kill " + ChatColor.WHITE + type.requirement.name + ChatColor.GRAY + " to unlock this Idler");
                        return;
                    }
                }
            }
        }
    }

    @EventHandler
    public void confirmMerchant(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        for(IdlerType type : IdlerType.values()) {
            if (e.getInventory().getTitle().equals(type.name)) {
                e.setCancelled(true);
                System.out.println(e.getCurrentItem().getType() + " " + type.item.getType());
                if (e.getCurrentItem().getType() == type.item.getType() && e.getCurrentItem().getType() != null) {
                    if (e.getClick().isRightClick()) {
                        ItemStack item = e.getCurrentItem();
                        upgrade(p, e.getCurrentItem(), type);
                        p.closeInventory();
                        return;
                    } else if (e.getClick().isLeftClick()) {
                        collect(p, type);
                        e.setCancelled(true);
                        p.closeInventory();
                        return;
                    }
                }

            }
        }
    }

    private void openUpgrade(Player player, IdlerType idlerType)
    {
        Inventory inv = Bukkit.createInventory(null, InventoryType.HOPPER, idlerType.name);

        int level = IdlerConfig.get().getInt(player.getUniqueId() + idlerType.path + "level");
        int amt = IdlerConfig.get().getInt(player.getUniqueId() + idlerType.path + "amt");
        int cost = costMultiplier(level + 1, idlerType.cost_scale);

        ItemStack em = idlerType.item;
        ItemMeta mem = em.getItemMeta();
        int time = (int) idlerType.time / 20;
        mem.setDisplayName(ChatColor.GREEN + idlerType.name);
        mem.setLore(
                Arrays.asList(new String[] {
                        ChatColor.WHITE + "Level: " + ChatColor.GRAY  + level,
                        ChatColor.WHITE + "Delay: " + ChatColor.GRAY + time + "s",
                        ChatColor.WHITE + "Amount: " + ChatColor.YELLOW + amt,
                        ChatColor.GREEN + "Upgrade: " + ChatColor.WHITE + cost + "g",
                        ChatColor.GRAY + "Right-Click to upgrade.",
                        ChatColor.GRAY + "Left-Click to collect."}));
        em.setItemMeta(mem);
        inv.setItem(2, em);
        player.openInventory(inv);
    }

    public static int costMultiplier(int level, int cost_scale){
        //i.e. If the Orb Miner is Level 3 - (3*30)^2
        //i.e. If the Crate Miner is Level 2 - (2*50)^2
        return (int) Math.pow(level * cost_scale, 2);
    }

    public static int getPrice(List<String> lore){
        System.out.println(lore);
        String name = ChatColor.stripColor(lore.get(3));
        System.out.println(name);
        return Integer.parseInt(name.split("Upgrade: ")[1].split("g")[0]);
    }

    public void upgrade(Player p, ItemStack item, IdlerType type){
        int amt = IdlerConfig.get().getInt(p.getUniqueId() + type.path + "level");
        int cost  = getPrice(item.getItemMeta().getLore());
        if(amt == type.max_upgrades){
            p.sendMessage(ChatColor.GRAY + "Maxed out upgrades");
            return;
        }
        if (Money.hasEnoughGems(p, cost)) {
            int amount = amt + 1;
            p.sendMessage(ChatColor.GRAY + "You have upgraded " + type.name + " to level " + amount);
            Money.takeGems(p, cost);
            get().set(p.getUniqueId() + type.path + "level", amt + 1);
            save();
            return;
        }else{
            p.sendMessage(ChatColor.RED + "You do not have enough gems!");
            return;
        }
    }

    public void collect(Player p, IdlerType type){
        ItemStack reward = null;
        int amt = IdlerConfig.get().getInt(p.getUniqueId() + type.path + "amt");
        int level = IdlerConfig.get().getInt(p.getUniqueId() + type.path + "level");
        switch (type){
            case Miner:
                reward = Items.orb(false);
                reward.setAmount(amt);
                break;
            case Banker:
                reward = Money.createBankNote(amt);
                break;
            case Fisherman:
                reward = Speedfish.fish(level, false);
                reward.setAmount(amt);
                break;
            case Hunter:
                reward = CratesMain.createCrate(level, false);
                reward.setAmount(amt);
                break;
        }
        if(amt > 0) {

            p.getInventory().addItem(reward);
            p.sendMessage(ChatColor.GRAY + "You have received " + ChatColor.YELLOW + amt + " " + type.itemName);
            get().set(p.getUniqueId() + type.path + "amt", 0);
            save();
        }else{
            p.sendMessage(ChatColor.GRAY + "You dont have any " + ChatColor.YELLOW + type.itemName+ " to collect.");
        }
    }

}
