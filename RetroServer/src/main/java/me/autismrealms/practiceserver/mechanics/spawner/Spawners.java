package me.autismrealms.practiceserver.mechanics.spawner;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.commands.moderation.DeployCommand;
import me.autismrealms.practiceserver.commands.moderation.ToggleMobsCommand;
import me.autismrealms.practiceserver.mechanics.bosses.BossRooms;
import me.autismrealms.practiceserver.mechanics.drops.Drops;
import me.autismrealms.practiceserver.mechanics.mobs.MobType;
import me.autismrealms.practiceserver.mechanics.mobs.Mobs;
import me.autismrealms.practiceserver.mechanics.mobs.NameGenerator;
import me.autismrealms.practiceserver.mechanics.mobs.SkullTextures;
import me.autismrealms.practiceserver.mechanics.mobs.elite.EliteType;
import me.autismrealms.practiceserver.mechanics.mobs.elite.GolemElite;
import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import me.autismrealms.practiceserver.utils.Particles;
import me.autismrealms.practiceserver.utils.Util;
import org.bukkit.*;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class Spawners implements Listener {

	public static ConcurrentHashMap<Location, String> spawners;
	public static ConcurrentHashMap<LivingEntity, Location> mobs;
	public static ConcurrentHashMap<Location, Long> respawntimer;
	private static Spawners instance;

	static {
		Spawners.spawners = new ConcurrentHashMap<Location, String>();
		Spawners.mobs = new ConcurrentHashMap<LivingEntity, Location>();
		Spawners.respawntimer = new ConcurrentHashMap<Location, Long>();
	}

	HashMap<String, Location> creatingspawner;

	public Spawners() {
		instance = this;

		CreatureSpawnEvent.SpawnReason spawnReason;
		this.creatingspawner = new HashMap<String, Location>();
	}

	public static ConcurrentHashMap<LivingEntity, Location> getMobs() {
		return mobs;
	}

	public static Spawners getInstance() {
		return instance;
	}

	static boolean isPlayerNearby(final Location loc) {
		for (final Player p : Bukkit.getOnlinePlayers()) {
			if (p.getWorld() == loc.getWorld()) {
				if (p.getLocation().distanceSquared(loc) < 640.0) {
					return true;
				}
			}
		}
		return false;
	}

	public static int getHp(final ItemStack is) {
		if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()) {
			final List<String> lore = is.getItemMeta().getLore();
			if (lore.size() > 1 && lore.get(1).contains("HP")) {
				try {
					return Integer.parseInt(lore.get(1).split(": +")[1]);
				} catch (Exception e) {
					return 0;
				}
			}
		}
		return 0;
	}



	public static int getMobTier(final LivingEntity e) {
		if (e.getEquipment().getItemInMainHand() != null) {
			if (e.getEquipment().getItemInMainHand().getType().name().contains("WOOD_")) {
				return 1;
			}
			if (e.getEquipment().getItemInMainHand().getType().name().contains("STONE_")) {
				return 2;
			}
			if (e.getEquipment().getItemInMainHand().getType().name().contains("IRON_")) {
				return 3;
			}
			if (e.getEquipment().getItemInMainHand().getType().name().contains("DIAMOND_")
					&& !e.getEquipment().getItemInMainHand().getType().name().contains("DIAMOND_") && e.getEquipment()
							.getItemInMainHand().getItemMeta().getDisplayName().contains(ChatColor.BLUE.toString())) {
				return 4;
			}
			if (e.getEquipment().getItemInMainHand().getType().name().contains("GOLD_")) {
				return 5;
			}
			if (e.getEquipment().getItemInMainHand().getType().name().contains("DIAMOND_")) {
				return 6;
			}
		}
		return 0;
	}

	public static boolean isElite(final LivingEntity e) {
		return e.getEquipment().getItemInMainHand() != null
				&& e.getEquipment().getItemInMainHand().getType() != Material.AIR
				&& e.getEquipment().getItemInMainHand().getItemMeta().hasEnchants();
	}

	public static int hpCheck(final LivingEntity s) {
		int a = 0;
		ItemStack[] armorContents;
		for (int length = (armorContents = s.getEquipment().getArmorContents()).length, i = 0; i < length; ++i) {
			final ItemStack is = armorContents[i];
			if (is != null && is.getType() != Material.AIR && is.hasItemMeta() && is.getItemMeta().hasLore()) {
				final int health = getHp(is);
				a += health;
			}
		}
		return a;
	}

	public static MobType checkMob(String type){
		for(EliteType eliteType : EliteType.values()){
			if((eliteType.id).equalsIgnoreCase(type)){
				return eliteType.type;
			}
		}
		for(MobType mobType : MobType.values()){
			if((mobType.id).equalsIgnoreCase(type)){
				return mobType;
			}
		}
		return MobType.Skeleton;
	}

	public static LivingEntity mob(final Location loc, final String type) {
		MobType mobType = checkMob(type);
		if (type.toLowerCase().contains(MobType.Skeleton.id) && !type.equalsIgnoreCase(MobType.WitherSkeleton.id)
				|| mobType.equals(MobType.Skeleton))  {
			final Skeleton skeleton = (Skeleton) loc.getWorld().spawnEntity(loc, EntityType.SKELETON);
			new CreatureSpawnEvent(skeleton, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return skeleton;
		}
		if (type.equalsIgnoreCase(MobType.WitherSkeleton.id) || checkMob(type).equals(MobType.WitherSkeleton)) {
			final Skeleton skeleton = (Skeleton) loc.getWorld().spawnEntity(loc, EntityType.SKELETON);
			skeleton.setSkeletonType(Skeleton.SkeletonType.WITHER);
			new CreatureSpawnEvent(skeleton, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return skeleton;
		}
		if (type.equalsIgnoreCase(MobType.Zombie.id) || checkMob(type).equals(MobType.Zombie)) {
			Zombie zombie = (Zombie) loc.getWorld().spawnEntity(loc, EntityType.ZOMBIE);
			new CreatureSpawnEvent(zombie, CreatureSpawnEvent.SpawnReason.CUSTOM);
			zombie.setBaby(false);
			return zombie;
		}
		if (type.equalsIgnoreCase(MobType.SilverFish.id) || checkMob(type).equals(MobType.SilverFish)) {
			Silverfish fish = (Silverfish) loc.getWorld().spawnEntity(loc, EntityType.SILVERFISH);
			new CreatureSpawnEvent(fish, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return fish;
		}
		if (type.equalsIgnoreCase(MobType.MagmaCube.id) || checkMob(type).equals(MobType.MagmaCube)) {
			MagmaCube cube = (MagmaCube) loc.getWorld().spawnEntity(loc, EntityType.MAGMA_CUBE);
			new CreatureSpawnEvent(cube, CreatureSpawnEvent.SpawnReason.CUSTOM);
			cube.setSize(3);
			return cube;
		}
		if (type.equalsIgnoreCase(MobType.Spider.id)  || checkMob(type).equals(MobType.Spider)) {
			final Spider spider = (Spider) loc.getWorld().spawnEntity(loc, EntityType.SPIDER);
			new CreatureSpawnEvent(spider, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return spider;
		}
		if (type.equalsIgnoreCase(MobType.CaveSpider.id)  || checkMob(type).equals(MobType.CaveSpider)) {
			final CaveSpider cspider = (CaveSpider) loc.getWorld().spawnEntity(loc, EntityType.CAVE_SPIDER);
			new CreatureSpawnEvent(cspider, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return cspider;
		}
		if (type.equalsIgnoreCase(MobType.Daemon.id)  || checkMob(type).equals(MobType.Daemon)) {
			final PigZombie daemon = (PigZombie) loc.getWorld().spawnEntity(loc, EntityType.PIG_ZOMBIE);
			daemon.setAngry(true);
			daemon.setBaby(false);
			new CreatureSpawnEvent(daemon, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return daemon;
		}
		if (type.equalsIgnoreCase(MobType.Imp.id)  || checkMob(type).equals(MobType.Imp)) {
			final PigZombie imp = (PigZombie) loc.getWorld().spawnEntity(loc, EntityType.PIG_ZOMBIE);
			imp.setAngry(true);
			imp.setBaby(true);
			new CreatureSpawnEvent(imp, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return imp;
		}
		if (type.equalsIgnoreCase("turkey") ) {
			final Chicken turkey = (Chicken) loc.getWorld().spawnEntity(loc, EntityType.CHICKEN);
			new CreatureSpawnEvent(turkey, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return turkey;
		}
		if (type.equalsIgnoreCase("giant")) {
			final Giant giant = (Giant) loc.getWorld().spawnEntity(loc, EntityType.GIANT);
			new CreatureSpawnEvent(giant, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return giant;
		}
		if (type.equalsIgnoreCase(MobType.Golem.id) || checkMob(type).equals(MobType.Golem)) {
			final Golem golem = (Golem) loc.getWorld().spawnEntity(loc, EntityType.IRON_GOLEM);
			new CreatureSpawnEvent(golem, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return golem;
		}
		if (type.equalsIgnoreCase("wither")) {
			final Wither wither = (Wither) loc.getWorld().spawnEntity(loc, EntityType.WITHER);
			new CreatureSpawnEvent(wither, CreatureSpawnEvent.SpawnReason.CUSTOM);
			return wither;
		}
		return null;
	}

	public static LivingEntity spawnMob(final Location loc, final String type, final int tier, final boolean elite) {
		final int randX = Util.random.nextInt(7) - 3;
		final int randZ = Util.random.nextInt(7) - 3;
		Location sloc = new Location(loc.getWorld(), loc.getX() + randX + 0.5, loc.getY() + 2.0,
				loc.getZ() + randZ + 0.5);
		if (sloc.getWorld().getBlockAt(sloc).getType() != Material.AIR
				|| sloc.getWorld().getBlockAt(sloc.add(0.0, 1.0, 0.0)).getType() != Material.AIR) {
			sloc = loc.clone().add(0.0, 1.0, 0.0);
		} else {
			sloc.subtract(0.0, 1.0, 0.0);
		}
		final LivingEntity s = mob(sloc, type);
		String name = "";
		int gearcheck = Util.random.nextInt(3) + 1;
		if (tier == 3) {
			final int m_type = Util.random.nextInt(2);
			if (m_type == 0) {
				gearcheck = 3;
			}
			if (m_type == 1) {
				gearcheck = 4;
			}
		}
		if (tier >= 4 || elite) {
			gearcheck = 4;
		}
		int held = Util.random.nextInt(4);
		boolean customElite = Mobs.isCustomElite(type);

		if(customElite){
			EliteType eliteType = EliteType.getEliteType(type);
			held = eliteType.item;
		}

//		if (s.getType() == EntityType.SKELETON || s.getType() == EntityType.ZOMBIE) {
//			held = Util.random.nextInt(4);
//		}

		final ItemStack hand = Drops.createDrop(tier, held);
		if (elite) {
			hand.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 1);
		}
		ItemStack head = null;
		ItemStack chest = null;
		ItemStack legs = null;
		ItemStack boots = null;
		int a_type = 0;
		while (gearcheck > 0) {
			a_type = Util.random.nextInt(4) + 1;
			if (a_type == 1 && head == null) {
				head = Drops.createDrop(tier, 4);
				if (elite) {
					head.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 1);
				}
				--gearcheck;
			}
			if (a_type == 2 && chest == null) {
				chest = Drops.createDrop(tier, 5);
				if (elite) {
					chest.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 1);
				}
				--gearcheck;
			}
			if (a_type == 3 && legs == null) {
				legs = Drops.createDrop(tier, 6);
				if (elite) {
					legs.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 1);
				}
				--gearcheck;
			}
			if (a_type == 4 && boots == null) {
				boots = Drops.createDrop(tier, 7);
				if (elite) {
					boots.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 1);
				}
				--gearcheck;
			}
		}

		/**
		 * Halloween int randomHelmetChance =
		 * ThreadLocalRandom.current().nextInt(100); if(randomHelmetChance > 65
		 * && head != null) { head = new
		 * ItemGenerator(Material.JACK_O_LANTERN).setName(head.getItemMeta().getDisplayName()).setLore(head.getItemMeta().getLore()).build();
		 * }
		 */
		s.setCanPickupItems(false);
		s.setRemoveWhenFarAway(false);

		name = NameGenerator.create(tier-1, type);
		String color = Tiers.colors[tier-1].toString();

		s.setCustomName(String.valueOf(color) + name);
		s.setCustomNameVisible(true);

		s.setMetadata("name", new FixedMetadataValue(PracticeServer.plugin, String.valueOf(color) + name));
		s.setMetadata("type", new FixedMetadataValue(PracticeServer.plugin, type));

		if (elite && !type.equalsIgnoreCase(EliteType.FrostKing.id)) {
			s.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
		}

		if (tier > 2 && !type.equalsIgnoreCase(EliteType.FrostKing.id)) {
			if (s.getEquipment().getItemInMainHand() != null
					&& s.getEquipment().getItemInMainHand().getType().name().contains("_HOE")) {
				s.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 1));
			} else {
				s.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
			}
		}
		if (type.equalsIgnoreCase(EliteType.FrostKing.id)) {
			s.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
		}

		if (type.equals("weakSkeletonEntity"))
			s.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));

		s.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 1));
		s.getEquipment().clear();
		s.getEquipment().setItemInMainHand(hand);

		if (type.equals(EliteType.Deathlord.id)) {
			s.getEquipment().setHelmet(SkullTextures.WITHER_KING.getSkullByURL());
		} else
			s.getEquipment().setHelmet(head);
		s.getEquipment().setChestplate(chest);
		s.getEquipment().setLeggings(legs);
		s.getEquipment().setBoots(boots);
		if (s.getType().equals(EntityType.SKELETON)
				&& ((Skeleton) s).getSkeletonType().equals(Skeleton.SkeletonType.WITHER)) {
			s.getEquipment().setHelmet(null);
		}
		int hp = hpCheck(s);
		if (elite) {
			for(int i = 1; i < PracticeServer.tier; i++){
				hp = (int) (hp * (i+1));
			}
			if (type.equalsIgnoreCase(EliteType.Warden.id)) {
				hp = 85000;
			}
			if (s.hasMetadata("infernalType")) {
				hp = hp * 2;
			}
			if (type.equalsIgnoreCase(EliteType.Deathlord.id)) {
				hp = 115000;
			}
			if (type.equalsIgnoreCase(EliteType.ExiledKing.id)) {
				hp = 100000;
			}
			if (type.equalsIgnoreCase(EliteType.Conquerer.id)) {
				hp = 150000;
			}
			if (type.equalsIgnoreCase(EliteType.Crypt.id)) {
				hp = 200000;
			}
		} else {
			hp = (int) (tier/2.2D);
		}
		if (hp < 1) {
			hp = 1;
		}
		s.setMetadata("customName", new FixedMetadataValue(PracticeServer.getInstance(), type));

		s.setRemoveWhenFarAway(false);
		s.setMaxHealth((double) hp);
		s.setHealth((double) hp);
		new BukkitRunnable() {
			public void run() {
				if (type.equalsIgnoreCase(EliteType.Crypt.id))
					GolemElite.golems.put(s, 0);
				Spawners.mobs.put(s, loc);
			}
		}.runTaskLaterAsynchronously(PracticeServer.plugin, 1L);
		return s;
	}

	public void onEnable() {
		PracticeServer.log.info("[Spawners] has been enabled.");
		Bukkit.getServer().getPluginManager().registerEvents(this, PracticeServer.plugin);
		final File file = new File(PracticeServer.plugin.getDataFolder(), "spawners.yml");
		final YamlConfiguration config = new YamlConfiguration();
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		try {
			config.load(file);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		for (final String key : config.getKeys(false)) {
			final String val = config.getString(key);
			final String[] str = key.split(",");
			final World world = Bukkit.getWorld(str[0]);
			final double x = Double.valueOf(str[1]);
			final double y = Double.valueOf(str[2]);
			final double z = Double.valueOf(str[3]);
			final Location loc = new Location(world, x, y, z);
			Spawners.spawners.put(loc, val);
		}
		Bukkit.getServer().getWorld("drmap").getEntities().stream().filter(e3 -> (e3 instanceof LivingEntity))
				.forEach(Entity::remove);
		Bukkit.getServer().getWorlds().get(0).getEntities().stream()
				.filter(e3 -> (e3 instanceof LivingEntity && !(e3 instanceof Player)) || e3 instanceof Item
						|| e3 instanceof EnderCrystal)
				.forEach(Entity::remove);
		new BukkitRunnable() {
			public void run() {
				// if (DeployCommand.patchlockdown) return;
				for (final Entity e : Bukkit.getWorlds().get(0).getEntities()) {
					if (e instanceof LivingEntity) {
						final LivingEntity s = (LivingEntity) e;
						if (!Spawners.mobs.containsKey(s)) {
							continue;
						}
						final Location loc = Spawners.mobs.get(s);
						final Location newloc = s.getLocation();
						if (loc.distance(newloc) <= (Mobs.isGolemBoss(s) ? 150 : 30)) {
							continue;
						}
						final Random r = new Random();
						final int randX = Util.random.nextInt(7) - 3;
						final int randZ = Util.random.nextInt(7) - 3;
						Location sloc = new Location(loc.getWorld(), loc.getX() + randX + 0.5, loc.getY() + 2.0,
								loc.getZ() + randZ + 0.5);
						if (sloc.getWorld().getBlockAt(sloc).getType() != Material.AIR
								|| sloc.getWorld().getBlockAt(sloc.add(0.0, 1.0, 0.0)).getType() != Material.AIR) {
							sloc = loc.clone().add(0.0, 1.0, 0.0);
						} else {
							sloc.subtract(0.0, 1.0, 0.0);
						}
						s.setFallDistance(0.0f);
						double range = 20.0;
						if (Mobs.isGolemBoss(e))
							range = 100;
						Particles.SPELL.display(0.0f, 0.0f, 0.0f, 0.5f, 80, s.getLocation().add(0.0, 0.15, 0.0), 20);
						s.teleport(loc);


						if (!s.hasMetadata("name")) {
							continue;
						}
						s.setCustomName(s.getMetadata("name").get(0).asString());
						s.setCustomNameVisible(true);
					}
				}
				Spawners.mobs.keySet().stream().filter(l -> l == null || l.isDead()).forEach(l -> {
					Spawners.mobs.remove(l);
				});
			}
		}.runTaskTimer(PracticeServer.plugin, 1, 1);
		new BukkitRunnable() {
			public void run() {
				// if (DeployCommand.patchlockdown) return;
				for (final Location loc : Spawners.spawners.keySet()) {
					if (Spawners.isPlayerNearby(loc) && loc.getChunk().isLoaded() && !Spawners.mobs.containsValue(loc)
							&& (!Spawners.respawntimer.containsKey(loc)
									|| System.currentTimeMillis() > Spawners.respawntimer.get(loc))) {
						final String data = Spawners.spawners.get(loc);
						if (!SpawnerHandler.isCorrectFormat(data)) {
							continue;

						}
						if (data.contains(",")) {
							String[] split;
							for (int length = (split = data.split(",")).length, k = 0; k < length; ++k) {
								final String s = split[k];
								final String type = s.split(":")[0];
								int tier = Integer.parseInt(s.split(":")[1].split("@")[0]);
								final boolean elite = Boolean.parseBoolean(s.split("@")[1].split("#")[0]);
								for (int amt = Integer.parseInt(s.split("#")[1]), i = 0; i < amt; ++i) {
									if (!DeployCommand.patchlockdown && ToggleMobsCommand.togglespawners)
										Spawners.this.spawnMob(loc, type, tier, elite);
								}
							}
						} else {
							final String type2 = data.split(":")[0];
							int tier2 = Integer.parseInt(data.split(":")[1].split("@")[0]);
							final boolean elite2 = Boolean.parseBoolean(data.split("@")[1].split("#")[0]);
							for (int amt2 = Integer.parseInt(data.split("#")[1]), j = 0; j < amt2; ++j) {
								if (!DeployCommand.patchlockdown && ToggleMobsCommand.togglespawners)
									Spawners.this.spawnMob(loc, type2, tier2, elite2);
							}
						}
					}
				}
			}
		}.runTaskTimer(PracticeServer.plugin, 100L, 20L);
		new BukkitRunnable() {
			public void run() {
				for (final Entity e : Bukkit.getWorlds().get(0).getEntities()) {
					if (e instanceof LivingEntity && !(e instanceof Player)
							&& !Spawners.isPlayerNearby(e.getLocation()) && BossRooms.roomMap == null) {
						e.remove();
						Spawners.mobs.remove(e);
					}
				}
			}
		}.runTaskTimer(PracticeServer.plugin, 2000L, 2000L);
		new BukkitRunnable() {
			public void run() {
				for (final Entity e : Bukkit.getWorlds().get(0).getEntities()) {
					if (e instanceof LivingEntity && !(e instanceof Player)) {
						e.remove();
					}
				}
				Spawners.mobs.clear();
				Spawners.respawntimer.clear();
			}
		}.runTaskLater(PracticeServer.plugin, 72000L);
	}

	public void thanksgiving(Location loc, int tier) {
		Random r = new Random();
		int thanksgiving = r.nextInt(300);
		if (thanksgiving == 0) {
			LivingEntity turkey = mob(loc, "turkey");
			turkey.setCustomName(ChatColor.GOLD + "Thanksgiving Turkey");
			turkey.setRemoveWhenFarAway(false);
			turkey.setCustomNameVisible(true);
			turkey.setMetadata("type", new FixedMetadataValue(PracticeServer.plugin, "turkey"));
			turkey.setMetadata("name",
					new FixedMetadataValue(PracticeServer.plugin, ChatColor.GOLD + "Thanksgiving Turkey"));
			turkey.setMetadata("tier", new FixedMetadataValue(PracticeServer.plugin, Integer.toString(tier)));
			turkey.getEquipment().setItemInMainHand(new ItemStack(Material.FEATHER));
			turkey.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 10));
			turkey.setMaxHealth((double) 35 * tier * tier * tier);
			turkey.setHealth((double) 35 * tier * tier * tier);
		}
	}

	public void onDisable() {
		PracticeServer.log.info("[Spawners] has been disabled.");
		final File file = new File(PracticeServer.plugin.getDataFolder(), "spawners.yml");
		if (file.exists()) {
			file.delete();
		}
		final YamlConfiguration config = new YamlConfiguration();
		if (!spawners.isEmpty()) {
			for (final Location loc : Spawners.spawners.keySet()) {
				final String s = String.valueOf(loc.getWorld().getName()) + "," + (int) loc.getX() + ","
						+ (int) loc.getY() + "," + (int) loc.getZ();
				config.set(s, Spawners.spawners.get(loc));
				try {
					config.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		for (final Entity e2 : Bukkit.getServer().getWorlds().get(0).getEntities()) {
			if ((e2 instanceof LivingEntity && !(e2 instanceof Player)) || e2 instanceof Item
					|| e2 instanceof EnderCrystal) {
				if (e2 instanceof EnderCrystal) {
					e2.getLocation().getWorld().getBlockAt(e2.getLocation().subtract(0.0, 1.0, 0.0))
							.setType(Material.CHEST);
				}
				e2.remove();
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onSpawnerCreate(final AsyncPlayerChatEvent e) {
		final Player p = e.getPlayer();
		if (p.isOp() && this.creatingspawner.containsKey(p.getName())) {
			e.setCancelled(true);
			if (e.getMessage().equalsIgnoreCase("cancel")) {
				p.sendMessage(new StringBuilder().append(ChatColor.RED).append(ChatColor.BOLD)
						.append("     *** SPAWNER CREATION CANCELLED ***").toString());
				this.creatingspawner.remove(p.getName());
			} else if (SpawnerHandler.isCorrectFormat(e.getMessage())) {
				p.sendMessage(ChatColor.GRAY + "Spawner with data '" + ChatColor.YELLOW + e.getMessage()
						+ ChatColor.GRAY + "' created at " + ChatColor.YELLOW
						+ this.creatingspawner.get(p.getName()).toVector());
				Spawners.spawners.put(this.creatingspawner.get(p.getName()), e.getMessage());
				this.creatingspawner.remove(p.getName());
				final File file = new File(PracticeServer.plugin.getDataFolder(), "spawners.yml");
				if (file.exists()) {
					file.delete();
				}
				final YamlConfiguration config = new YamlConfiguration();
				for (final Location loc : Spawners.spawners.keySet()) {
					final String s = String.valueOf(loc.getWorld().getName()) + "," + (int) loc.getX() + ","
							+ (int) loc.getY() + "," + (int) loc.getZ();
					config.set(s, Spawners.spawners.get(loc));
					try {
						config.save(file);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			} else {
				p.sendMessage(new StringBuilder().append(ChatColor.RED).append(ChatColor.BOLD)
						.append("     *** INCORRECT FORMAT ***").toString());
				p.sendMessage(" ");
				p.sendMessage(ChatColor.YELLOW + "FORMAT: " + ChatColor.GRAY + "mobtype:tier@elite#amount");
				p.sendMessage(ChatColor.YELLOW + "EX: " + ChatColor.GRAY
						+ "skeleton:5@true#1,zombie:4@true#1,magmacube:4@false#5");
				p.sendMessage(" ");
				p.sendMessage(new StringBuilder().append(ChatColor.RED).append(ChatColor.BOLD)
						.append("     *** INCORRECT FORMAT ***").toString());
			}
		}
	}

	@EventHandler
	public void onBlockPlace(final BlockPlaceEvent e) {
		final Player p = e.getPlayer();
		if (p.isOp() && e.getBlock().getType().equals(Material.MOB_SPAWNER)) {
			p.sendMessage(new StringBuilder().append(ChatColor.GREEN).append(ChatColor.BOLD)
					.append("     *** SPAWNER CREATION STARTED ***").toString());
			p.sendMessage(" ");
			p.sendMessage(ChatColor.YELLOW + "FORMAT: " + ChatColor.GRAY + "mobtype:tier@elite#amount");
			p.sendMessage(ChatColor.YELLOW + "EX: " + ChatColor.GRAY
					+ "skeleton:5@true#1,zombie:4@true#1,magmacube:4@false#5");
			p.sendMessage(" ");
			p.sendMessage(new StringBuilder().append(ChatColor.GREEN).append(ChatColor.BOLD)
					.append("     *** SPAWNER CREATION STARTED ***").toString());
			this.creatingspawner.put(p.getName(), e.getBlock().getLocation());
		}
	}

	@EventHandler
	public void onBlockBreak(final BlockBreakEvent e) {
		final Player p = e.getPlayer();
		if (p.isOp() && e.getBlock().getType().equals(Material.MOB_SPAWNER)) {
			if (Spawners.spawners.containsKey(e.getBlock().getLocation())) {
				p.sendMessage(ChatColor.GRAY + "Spawner with data '" + ChatColor.YELLOW
						+ Spawners.spawners.get(e.getBlock().getLocation()) + ChatColor.GRAY + "' removed at "
						+ ChatColor.YELLOW + e.getBlock().getLocation().toVector());
				Spawners.spawners.remove(e.getBlock().getLocation());
				PracticeServer.log.info("[Spawners] a spawner has been destroyed.");
				final File file = new File(PracticeServer.plugin.getDataFolder(), "spawners.yml");
				if (file.exists()) {
					file.delete();
				}
				final YamlConfiguration config = new YamlConfiguration();
				for (final Location loc : Spawners.spawners.keySet()) {
					final String s = String.valueOf(loc.getWorld().getName()) + "," + (int) loc.getX() + ","
							+ (int) loc.getY() + "," + (int) loc.getZ();
					config.set(s, Spawners.spawners.get(loc));
					try {
						config.save(file);
					} catch (IOException ee) {
						ee.printStackTrace();
					}
				}
			}
			if (this.creatingspawner.containsValue(e.getBlock().getLocation())) {
				for (final String s : this.creatingspawner.keySet()) {
					if (this.creatingspawner.get(s).equals(e.getBlock().getLocation())) {
						p.sendMessage(new StringBuilder().append(ChatColor.RED).append(ChatColor.BOLD)
								.append("     *** SPAWNER CREATION CANCELLED ***").toString());
						this.creatingspawner.remove(s);
					}
				}
			}

		}
	}

	@EventHandler
	public void onBlockClick(final PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		if (p.isOp() && e.getAction() == Action.RIGHT_CLICK_BLOCK
				&& e.getClickedBlock().getType().equals(Material.MOB_SPAWNER)
				&& Spawners.spawners.containsKey(e.getClickedBlock().getLocation())) {
			p.sendMessage(ChatColor.GRAY + "Spawner with data '" + ChatColor.YELLOW
					+ Spawners.spawners.get(e.getClickedBlock().getLocation()) + ChatColor.GRAY + "' at "
					+ ChatColor.YELLOW + e.getClickedBlock().getLocation().toVector());
		}
	}

	@EventHandler
	public void onChunkUnload(final ChunkUnloadEvent e) {
		Entity[] entities;
		for (int length = (entities = e.getChunk().getEntities()).length, i = 0; i < length; ++i) {
			final Entity ent = entities[i];
			if (ent instanceof LivingEntity && !(ent instanceof Player) && !(ent instanceof EnderCrystal)) {
				if (Spawners.mobs.containsKey(ent)) {
					Spawners.mobs.remove(ent);
				}
				ent.remove();
			}
		}
	}

	@EventHandler
	public void onChunkLoad(final ChunkLoadEvent e) {
		Entity[] entities;
		for (int length = (entities = e.getChunk().getEntities()).length, i = 0; i < length; ++i) {
			final Entity ent = entities[i];
			if ((ent instanceof LivingEntity && !(ent instanceof Player)) || ent instanceof EnderCrystal) {
				if (Spawners.mobs.containsKey(ent)) {
					Spawners.mobs.remove(ent);
				}
				if (ent instanceof EnderCrystal) {
					ent.getLocation().getWorld().getBlockAt(ent.getLocation().subtract(0.0, 1.0, 0.0))
							.setType(Material.CHEST);
				}
				ent.remove();
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDeathd(final EntityDamageEvent e) {
		if (e.getEntity() instanceof LivingEntity) {
			final LivingEntity s = (LivingEntity) e.getEntity();
			if (e.getDamage() >= s.getHealth() && Spawners.mobs.containsKey(s)) {
				long time = 40L;
				time *= getMobTier(s);
				time *= isElite(s) ? 1200L : 1000L;
				time += System.currentTimeMillis();
				if (!Spawners.respawntimer.containsKey(Spawners.mobs.get(s))
						|| Spawners.respawntimer.get(Spawners.mobs.get(s)) < time) {
					Spawners.respawntimer.put(Spawners.mobs.get(s), time);
				}
				Spawners.mobs.remove(s);
			}
		}
	}
}
