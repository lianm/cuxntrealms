package me.autismrealms.practiceserver.mechanics.bosses.mobs;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.bosses.BossRooms;
import me.autismrealms.practiceserver.mechanics.bosses.BossTypes;
import me.autismrealms.practiceserver.mechanics.drops.CreateDrop;
import me.autismrealms.practiceserver.mechanics.drops.generate.FactoryBossDrop;
import me.autismrealms.practiceserver.mechanics.item.Rarity;
import me.autismrealms.practiceserver.mechanics.spawner.Spawners;
import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class MobHandler {

    public static LivingEntity spawnMobs(BossRooms room, BossTypes boss){
        int tier = boss.tier;
        String type = boss.type.id;
        Rarity rarity = Rarity.getRarityFromString(room.toString());

        //Initialize entity
        final LivingEntity s = Spawners.mob(BossRooms.getRandomLocation(room), type);

        s.setCanPickupItems(false);
        s.setRemoveWhenFarAway(false);

        String color = Tiers.colors[tier-1].toString();

        s.setCustomName(color + boss.minion_name);
        s.setCustomNameVisible(true);

        ItemStack hand = CreateDrop.createDrop(tier, new Random().nextInt(4), new Random().nextInt(4));

        s.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 1));
        s.getEquipment().clear();
        s.getEquipment().setItemInMainHand(hand);

        //Set Mob Armor
        s.getEquipment().setHelmet(CreateDrop.createDrop(tier, 4, new Random().nextInt(3) +1));
        s.getEquipment().setChestplate(CreateDrop.createDrop(tier, 5, new Random().nextInt(3) +1));
        s.getEquipment().setLeggings(CreateDrop.createDrop(tier, 6, new Random().nextInt(3) +1));
        s.getEquipment().setBoots(CreateDrop.createDrop(tier, 7, new Random().nextInt(3) +1));

        int hp = Spawners.hpCheck(s) * 4;

        s.setMetadata("type", new FixedMetadataValue(PracticeServer.getInstance(), type));
        s.setMetadata("minion", new FixedMetadataValue(PracticeServer.getInstance(), true));

        s.setRemoveWhenFarAway(false);
        s.setMaxHealth((double) hp);
        s.setHealth((double) hp);

        return s;
    }

    public static LivingEntity spawnBoss(BossRooms room, BossTypes boss){
        int tier = boss.tier;
        String type = boss.id;

        LivingEntity s = spawnMobs(room, boss);

        String name = boss.name;
        String color = "" + ChatColor.GOLD + ChatColor.BOLD;

        s.setCustomName(color + name);
        s.setCustomNameVisible(true);

        s.setMetadata("type", new FixedMetadataValue(PracticeServer.getInstance(), boss.id));
        s.setCustomName("" + ChatColor.BOLD + ChatColor.LIGHT_PURPLE + s.getCustomName());
        s.getEquipment().setItemInMainHand(FactoryBossDrop.setDrop(boss, boss.item));
        s.getEquipment().setHelmet(FactoryBossDrop.setDrop(boss, 4));
        s.getEquipment().setChestplate(FactoryBossDrop.setDrop(boss, 5));
        s.getEquipment().setLeggings(FactoryBossDrop.setDrop(boss, 6));
        s.getEquipment().setBoots(FactoryBossDrop.setDrop(boss, 7));

        int hp = Spawners.hpCheck(s) * 5;

        s.setMaxHealth((double) hp);
        s.setHealth((double) hp);
        return s;

    }

    public static void spawnRound(BossRooms room, BossTypes bossTypes){
        int wave = BossRooms.wave.get(room) +1;
        int mobAmt = wave * 12;
        System.out.println(wave + " " + room.waves);
        if(wave == room.waves+1){
            spawnBoss(room, bossTypes);
        }
        while(mobAmt > 0){
            spawnMobs(room, bossTypes);
            mobAmt--;
        }
    }

}
