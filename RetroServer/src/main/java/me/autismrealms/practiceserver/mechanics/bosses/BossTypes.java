package me.autismrealms.practiceserver.mechanics.bosses;

import me.autismrealms.practiceserver.mechanics.enchants.EnchantType;
import me.autismrealms.practiceserver.mechanics.mobs.MobType;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public enum BossTypes {
    //Strength, MobType, Name, Tier, DPS, Item, Energy
    Mayel(54, MobType.WitherSkeleton, "Mayel The Cruel", 1, 1, 3, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT))),
    DustDevil(15, MobType.Skeleton, "Dust Devil", 1, 1, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.ACCURACY, EnchantType.VS_PLAYERS, EnchantType.INT, EnchantType.VIT, EnchantType.BLOCK, EnchantType.DEX))),
    Athena(82, MobType.Zombie, "Wise Athena", 1, 2, 1, 2, new ArrayList<>(Arrays.asList(EnchantType.ICE_DMG, EnchantType.LIFE_STEAL, EnchantType.VS_MONSTERS, EnchantType.ACCURACY, EnchantType.DODGE, EnchantType.INT, EnchantType.VIT))),
    Hobgoblin(22, MobType.Zombie, "Rich Hobgoblin", 2, 2, 3, 2, new ArrayList<>(Arrays.asList(EnchantType.POISON_DMG, EnchantType.LIFE_STEAL, EnchantType.PURE_DMG, EnchantType.BLOCK, EnchantType.DEX, EnchantType.DODGE))),
    Pete(75, MobType.WitherSkeleton, "Pete The Pirate", 2, 1, 2, 1, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.CRITICAL_HIT, EnchantType.INT, EnchantType.VIT, EnchantType.DEX))),
    InfernalMage(34, MobType.Skeleton, "Infernal Mage", 3, 1, 0, 2, new ArrayList<>(Arrays.asList(EnchantType.POISON_DMG, EnchantType.VS_PLAYERS, EnchantType.VIT, EnchantType.BLOCK))),
    Illidan(96, MobType.Golem, "Illidan", 4, 2, 2, 2, new ArrayList<>(Arrays.asList(EnchantType.POISON_DMG, EnchantType.VS_MONSTERS, EnchantType.LIFE_STEAL, EnchantType.STR, EnchantType.VIT, EnchantType.BLOCK))),
    Ragnoros(100, MobType.Skeleton, "Ragnoros", 4, 1, 3, 2, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.CRITICAL_HIT, EnchantType.STR, EnchantType.INT, EnchantType.VIT, EnchantType.BLOCK))),
    AbyssalDemon(99, MobType.Skeleton, "Abyssal Demon", 4, 2, 1, 1, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.DODGE, EnchantType.VIT, EnchantType.BLOCK))),
    EartWarrior(99, MobType.Skeleton, "Earth Warrior", 5, 2, 1, 2, new ArrayList<>(Arrays.asList(EnchantType.ICE_DMG, EnchantType.VS_MONSTERS, EnchantType.STR, EnchantType.VIT))),
    Soulgazer(99, MobType.Skeleton, "Soulgazer", 5, 2, 2, 1, new ArrayList<>(Arrays.asList( EnchantType.CRITICAL_HIT, EnchantType.VS_MONSTERS, EnchantType.LIFE_STEAL, EnchantType.STR, EnchantType.DODGE))),
    Duranor(27, MobType.Skeleton, "Duranor The Daemon", 5, 1, 1, 1, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.VIT))),
    Valakas(12, MobType.MagmaCube, "Valakas", 6, 1, 2, 2, new ArrayList<>(Arrays.asList(EnchantType.POISON_DMG, EnchantType.CRITICAL_HIT, EnchantType.INT, EnchantType.VIT, EnchantType.BLOCK))),
    Zues(45, MobType.Golem, "Zues", 6, 2, 3, 1, new ArrayList<>(Arrays.asList(EnchantType.FIRE_DMG, EnchantType.VS_MONSTERS, EnchantType.PURE_DMG, EnchantType.BLOCK, EnchantType.DODGE, EnchantType.VIT))),
    Poseidon(100, MobType.Zombie, "Poseidon", 6, 1, 0, 2, new ArrayList<>(Arrays.asList(EnchantType.ICE_DMG, EnchantType.VS_PLAYERS, EnchantType.LIFE_STEAL, EnchantType.BLOCK, EnchantType.INT))),
    ;

    public String name;
    public int strength;
    public int armdps;
    public ArrayList<EnchantType> enchants;
    public int tier;
    public int nrghp;
    public String id;
    public String minion_name;
    public int item;
    public MobType type;

    BossTypes(int strength, MobType type, String name, int tier, int armdps, int item, int nrghp, ArrayList<EnchantType> enchants) {
        this.strength = strength;
        this.name = name;
        this.minion_name = this.toString() + "'s Minion";
        this.armdps = armdps;
        this.type = type;
        this.nrghp = nrghp;
        this.item = item;
        this.id = this.toString().toLowerCase();
        this.enchants = enchants;
        this.tier = tier;
    }

    public static BossTypes getRandom(int tier){
        List<BossTypes> list = new ArrayList<>();
        for(BossTypes bossTypes : BossTypes.values()){
            if(tier  == bossTypes.tier){
                list.add(bossTypes);
            }
        }
        return list.get(new Random().nextInt(list.size()));
    }

    public static BossTypes getType(ItemStack rune){
        String line = rune.getItemMeta().getDisplayName();
        for(BossTypes boss : BossTypes.values()) {
            if (ChatColor.stripColor(line).contains(boss.toString())){
                return boss;
            }
        }
        return null;
    }

    public static boolean isBoss(String type){
        for(BossTypes bossTypes : BossTypes.values()) {
            if(bossTypes.id == type){
                return true;
            }
        }
        return false;

    }
}
