package me.autismrealms.practiceserver.mechanics.spawner;

import me.autismrealms.practiceserver.mechanics.mobs.MobType;
import me.autismrealms.practiceserver.mechanics.mobs.elite.EliteType;

public class SpawnerHandler {

    public static boolean isCorrectFormat(final String data) {
        if (!data.contains(":") || !data.contains("@") || !data.contains("#")) {
            return false;
        }
        if (data.contains(",")) {
            String[] split;
            for (int length = (split = data.split(",")).length, i = 0; i < length; ++i) {
                final String s = split[i];
                if (!s.contains(":") || !s.contains("@") || !s.contains("#")) {
                    return false;
                }
                final String type = s.split(":")[0];
                if (type.equalsIgnoreCase("spectralGuard")
                        && !type.equalsIgnoreCase("weakSkeletonEntity")
                        && !type.equalsIgnoreCase("spectralKnight")) {
                    return false;
                }

                for (EliteType eliteType : EliteType.values()) {
                    if (!type.equalsIgnoreCase(eliteType.id)) {
                        return false;
                    }
                }

                for (MobType mobType : MobType.values()) {
                    if (!type.equalsIgnoreCase(mobType.id)) {
                        return false;
                    }
                }

                try {
                    final int tier = Integer.parseInt(s.split(":")[1].split("@")[0]);
                    if (tier < 1 || tier > 10) {
                        return false;
                    }
                } catch (Exception e) {
                    return false;
                }
                final String elite = s.split("@")[1].split("#")[0];
                if (!elite.equalsIgnoreCase("true") && !elite.equalsIgnoreCase("false")) {
                    return false;
                }
                try {
                    final int amt = Integer.parseInt(s.split("#")[1]);
                    if (amt < 1 || amt > 10) {
                        return false;
                    }
                } catch (Exception e2) {
                    return false;
                }
            }
            return true;
        }
        final String type2 = data.split(":")[0];
        try {
            final int tier3 = Integer.parseInt(data.split(":")[1].split("@")[0]);
            if (tier3 < 1 || tier3 > 6) {
                return false;
            }
        } catch (Exception e3) {
            return false;
        }
        final String elite2 = data.split("@")[1].split("#")[0];
        if (!elite2.equalsIgnoreCase("true") && !elite2.equalsIgnoreCase("false")) {
            return false;
        }
        try {
            final int amt2 = Integer.parseInt(data.split("#")[1]);
            if (amt2 < 1 || amt2 > 100) {
                return false;
            }
        } catch (Exception e4) {
            return false;
        }
        final int tier4 = Integer.parseInt(data.split(":")[1].split("@")[0]);
        final boolean iselite2 = Boolean.parseBoolean(elite2);
        return true;
    }
}
