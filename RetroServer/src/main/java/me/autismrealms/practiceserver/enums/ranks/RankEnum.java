package me.autismrealms.practiceserver.enums.ranks;

import lombok.Getter;

/**
 * Created by Jaxon on 8/13/2017.
 */
public enum RankEnum {
    DEFAULT("", "default"),
    SUB("&a&lS", "sub"),
    SUB1("&6&lS+", "sub+"),
    SUB2("&3&lS++", "sub++"),
    SUB3("&3&lS++", "sub+++"),
    SUPPORTER("&d&lSUPPORTER", "supporter"),
    QUALITY("&5&lQA", "quality"),
    BUILDER("&b&lBUILDER", "builder"),
    YOUTUBER("&c&lSOUNDCLOUD", "youtuber"),
    PMOD("&f&lPMOD", "pmod"),
    GM("&b&lGM", "gm"),
    MANAGER("&e&lMANAGER", "manager"),
    DEV("&c&lDEV", "dev");

    @Getter
    public String tag;
    public String id;

    RankEnum(String tag, String id) {
        this.tag = tag;
        this.id = id;
    }


    public static String enumToString(RankEnum rankEnum) {
        switch (rankEnum) {
            case DEFAULT:
                return "default";
            case SUB:
                return "sub";
            case SUB1:
                return "sub+";
            case SUB2:
                return "sub++";
            case SUB3:
                return "sub+++";
            case SUPPORTER:
                return "supporter";
            case QUALITY:
                return "quality";
            case BUILDER:
                return "builder";
            case YOUTUBER:
                return "youtuber";
            case PMOD:
                return "pmod";
            case GM:
                return "gm";
            case MANAGER:
                return "manager";
            case DEV:
                return "dev";
            default:
                return "default";
        }
    }

    public static RankEnum fromString(String rank) {
        switch (rank.toLowerCase()) {
            case "default":
                return DEFAULT;
            case "sub":
                return SUB;
            case "sub+":
                return SUB1;
            case "sub++":
                return SUB2;
            case "sub+++":
                return SUB3;
            case "supporter":
                return SUPPORTER;
            case "youtuber":
                return YOUTUBER;
            case "quality":
                return QUALITY;
            case "builder":
                return BUILDER;
            case "pmod":
                return PMOD;
            case "gm":
                return GM;
            case "manager":
                return MANAGER;
            case "dev":
                return DEV;
            default:
                return DEFAULT;
        }
    }


}
