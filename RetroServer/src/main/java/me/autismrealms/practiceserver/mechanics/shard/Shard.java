package me.autismrealms.practiceserver.mechanics.shard;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.apis.nbt.NBTAccessor;
import me.autismrealms.practiceserver.utils.ServerUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by Jaxon on 8/20/2017.
 */
public class Shard implements Listener {

    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, PracticeServer.getInstance());
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(event.getInventory().getTitle().equalsIgnoreCase("RETRO REALMS - SERVERS")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType() == null || event.getCurrentItem().getType() == Material.AIR || !event.getCurrentItem().getItemMeta().hasDisplayName()) return;
            NBTAccessor nbtAccessor = new NBTAccessor(event.getCurrentItem());
            ServerUtil.sendToServer(event.getWhoClicked().getName(), nbtAccessor.getString("server"));
            event.getWhoClicked().closeInventory();

        }
    }
}
