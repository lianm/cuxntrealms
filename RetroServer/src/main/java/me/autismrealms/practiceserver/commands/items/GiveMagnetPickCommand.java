package me.autismrealms.practiceserver.commands.items;

import me.autismrealms.practiceserver.mechanics.item.Items;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Disregard on 11-9-2017.
 */

public class GiveMagnetPickCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args){
        if(!sender.isOp()) return false;
        Player p = Bukkit.getPlayer(args[0]);
        if(p != null && p.isOnline()){
            p.getInventory().addItem(Items.subplusPick());
        }
        return false;
    }
}
