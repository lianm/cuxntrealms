package me.autismrealms.practiceserver.commands.moderation;

import me.autismrealms.practiceserver.enums.ranks.RankEnum;
import me.autismrealms.practiceserver.mechanics.moderation.ModerationMechanics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TellAllCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if ((sender instanceof Player && ModerationMechanics.getRank((Player) sender) == RankEnum.BUILDER) || (sender.isOp())) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < args.length; i++) {
                    sb.append(args[i]).append(" ");
                }
                String allArgs = sb.toString().trim();
                Bukkit.broadcastMessage(ChatColor.AQUA.toString() + ChatColor.BOLD + ">>> " + ChatColor.AQUA + allArgs);
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    //        TTA_Methods.sendActionBar(pl, ChatColor.AQUA + allArgs, 140);
                    pl.playSound(pl.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                    pl.playSound(pl.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
                }
            }
        return false;
    }
}
