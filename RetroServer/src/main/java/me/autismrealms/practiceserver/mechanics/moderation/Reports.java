package me.autismrealms.practiceserver.mechanics.moderation;

import me.autismrealms.practiceserver.PracticeServer;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Reports implements Listener {
    public static ArrayList<Bug> bugs = new ArrayList<>();

    private static Reports instance;

    public static Reports getInstance() {
        return instance;
    }

    public Reports() {
        instance = this;
    }

    public void onEnable() {
        PracticeServer.log.info("[Reports] has been enabled.");
        Bukkit.getServer().getPluginManager().registerEvents(this, PracticeServer.plugin);
        final File f = new File(PracticeServer.plugin.getDataFolder(), "reports.yml");
        final YamlConfiguration config = new YamlConfiguration();

        if (f.exists()) {
            try {
                config.load(f);

                if (config.contains("bugs_inc"))
                    Bug.inc = new AtomicInteger(config.getInt("bugs_inc"));

                if (config.contains("bugs")) {
                    for (String s : config.getStringList("bugs")) {
                        try {
                            String[] split = s.split(",");
                            int i = Integer.parseInt(split[0]);
                            OfflinePlayer p = Bukkit.getOfflinePlayer(
                                    UUID.fromString(split[1]));
                            boolean b = Boolean.parseBoolean(split[2]);
                            String r = String.join(",", Arrays.copyOfRange(split, 3, split.length));
                            bugs.add(new Bug(i, p, b, r));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }
        }
    }

    public void onDisable() {
        PracticeServer.log.info("[Reports] has been disabled.");
        final File f = new File(PracticeServer.plugin.getDataFolder(), "reports.yml");
        final YamlConfiguration config = new YamlConfiguration();

        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            config.set("bugs_inc", Bug.inc.get());
            config.set("bugs", bugs.stream().map(b ->
                    b.id + "," + b.player.getUniqueId().toString()
                            + "," + b.accepted + "," + b.report)
                    .collect(Collectors.toList()));
            config.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Bug getBug(int id) {
        return bugs.stream().filter(b -> b.id == id).findAny().orElse(null);
    }

    public static class Bug {
        static AtomicInteger inc = new AtomicInteger();

        public int id;
        public OfflinePlayer player;
        public boolean accepted = false;
        public String report;

        private Bug(int id, OfflinePlayer player, boolean accepted, String report) {
            this.id = id;
            this.player = player;
            this.accepted = accepted;
            this.report = report;
        }

        public Bug(OfflinePlayer player, String report) {
            this(inc.incrementAndGet(), player, false, report);
        }
    }
}
