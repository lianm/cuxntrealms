package me.autismrealms.practiceserver.mechanics.item;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public enum Rarity {
    Common(ChatColor.GRAY),
    Uncommon(ChatColor.GREEN),
    Rare(ChatColor.AQUA),
    Unique(ChatColor.YELLOW);

    public ChatColor color;
    public String line;

    Rarity(ChatColor color){
        this.color = color;
        this.line = color + this.toString();

    }

    public static Rarity getRarity(ItemStack itemStack){
        List<String> lore = itemStack.getItemMeta().getLore();
        for(String line : lore){
            for(Rarity rar : Rarity.values()) {
                if (ChatColor.stripColor(line).contains(rar.toString())){
                    return rar;
                }
            }
        }
        return null;
    }

    public static Rarity getRarityFromString(String string){
        for(Rarity rar : Rarity.values()) {
            if (ChatColor.stripColor(string).contains(rar.toString())){
                return rar;
            }
        }
        return null;
    }
}
