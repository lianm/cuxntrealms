package me.autismrealms.practiceserver.mechanics.bosses;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.bosses.mobs.MobHandler;
import me.autismrealms.practiceserver.mechanics.teleport.TeleportBooks;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class BossHandler {

    //All Initializers in here
    public static void startRune(Player p, BossRooms room, BossTypes boss){
        if(!BossRooms.inProgress(room)) {

            //Set variables

            BossRooms.roomMap.put(p, room);
            BossRooms.bossMap.put(p, boss);

            waveUp(p, room, boss);

            p.teleport(BossRooms.getRandomLocation(room));

        }else{
            p.sendMessage(ChatColor.GRAY + "Boss's room is currently being used!");
        }
    }

    public static void endRune(Player p){
        BossRooms.wave.remove(BossRooms.roomMap.get(p));
        BossRooms.kills.remove(BossRooms.roomMap.get(p));
        BossRooms.roomMap.remove(p);
        BossRooms.bossMap.remove(p);
        p.teleport(TeleportBooks.Deadpeaks_Mountain_Camp);
    }

    public static void completeRune(Player p){
        p.sendMessage("" + ChatColor.WHITE + ChatColor.BOLD + "BOSS ROOM COMPLETE");
        p.getInventory().addItem(BossRewards.getReward(BossRewards.Gems, BossRooms.bossMap.get(p).tier));
        p.getInventory().addItem(BossRewards.getReward(BossRewards.LegendaryOrbs, BossRooms.bossMap.get(p).tier));
        p.getInventory().addItem(BossRewards.getReward(BossRewards.Enchant, BossRooms.bossMap.get(p).tier));
        endRune(p);
    }

    public static void waveUp(Player p, BossRooms room, BossTypes boss){
        //initialize boss room wave
        if(!BossRooms.wave.containsKey(room)) {
            BossRooms.wave.put(room, 1);
        }

        int w = BossRooms.wave.get(room);

        p.sendMessage("" + ChatColor.WHITE + ChatColor.BOLD + "WAVE " + w + " BEGINNING IN 5 SECONDS");

        new BukkitRunnable() {
            @Override
            public void run() {
                int maxWave = room.waves;
                if(BossRooms.wave.get(room) != maxWave){
                    int waves = BossRooms.wave.get(room);
                    int objective = (waves + 1) * 12;
                    MobHandler.spawnRound(room, boss);
                    p.sendMessage("" + ChatColor.WHITE + ChatColor.BOLD + "WAVE " + waves + " IS COMMENCING!");
                    p.sendMessage("" + ChatColor.UNDERLINE + ChatColor.GREEN + "Objective: " + ChatColor.WHITE +
                            "Kill " + objective + " of " + boss.name + "'s minions.");
                    BossRooms.wave.put(room, waves + 1);
                }else if(BossRooms.wave.containsKey(room) && BossRooms.wave.get(room) == maxWave){
                    MobHandler.spawnBoss(room, boss);
                }
            }

        }.runTaskLater(PracticeServer.plugin, 100);
    }

}
