package me.autismrealms.practiceserver.mechanics.drops.generate;

import me.autismrealms.practiceserver.mechanics.drops.Mobdrops;
import me.autismrealms.practiceserver.mechanics.item.ItemHandler;
import me.autismrealms.practiceserver.mechanics.item.ItemType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateDrop {

    public static ItemStack createDrop(int tier, int item, int rarity) {

        Random r = new Random();

        //Initialize Variables
        String name = ItemHandler.setName(item, tier-1);
        List<String> itemLore = new ArrayList<>();
        String item_string = ItemHandler.getItem(item);
        String mat_string = ItemHandler.getMatString(tier, item);
        String rarity_string = ItemHandler.setRarityInt(rarity);
        int hps = r.nextInt(2);
        int hpsAmt = GenerateOrb.setHps(tier);
        int hp = createHpBase(tier, rarity, item);
        int min = createDamage(tier, rarity, item, false);
        int max = createDamage(tier, rarity, item, true);
        int dps = GenerateOrb.setDps(tier);
        int nrg = GenerateOrb.setEnergy(tier);

        String dmg = ChatColor.RED + "DMG: " + min + " - " + max;
        ItemStack newItem;

        int randomDps = r.nextInt(2) + 1;
        newItem = new ItemStack(Material.getMaterial(mat_string + "_" + item_string));
        ItemMeta newItemMeta = newItem.getItemMeta();

        ItemType itemType = null;


        if(item <= 3) {
            itemLore.add(dmg);
            itemType = ItemType.WEAPON;
        }
        else{
            itemType = ItemType.ARMOR;
            if(randomDps == 1) {
                itemLore.add(ChatColor.RED + "DPS: " + dps + " - " + dps + "%");
            }else{
                itemLore.add(ChatColor.RED + "ARMOR: " + dps + " - " + dps + "%");
            }
            itemLore.add(ChatColor.RED + "HP: +" + hp);
            if(hps == 0)
                itemLore.add(ChatColor.RED + "ENERGY REGEN: +" +  nrg + "%");
            else
                itemLore.add(ChatColor.RED + "HP REGEN: +" +  hpsAmt + " HP/s");
        }
        itemLore.add(ChatColor.ITALIC + rarity_string);
        for (ItemFlag itemFlag : ItemFlag.values()) {
            newItemMeta.addItemFlags(itemFlag);
        }

        if(item >= 4 && tier == 6) return Mobdrops.setItemBlueLeather(newItem);

        newItemMeta.setLore(itemLore);
        newItemMeta.setDisplayName(name);
        newItem.setItemMeta(newItemMeta);
        return GenerateOrb.orbItem(newItem, false);
    }

    private static int getDamage(int t, int i){
        int dmg = (int) ((4.5 * Math.pow(t, 2)) - t);
        for(int j = 0; i < i; j++) {
            dmg *= (1 + (10/j));
        }
        return dmg;
    }

    private static int[][] damage = {
            {3, 10, 17, 24, 31, 38},
            {15, 22, 37, 48, 59, 70},
            {33, 52, 72, 92, 112, 132},
            {65, 102, 138, 173, 212, 251},
            {124, 194, 262, 331, 403, 475},
            {236, 369, 502, 635, 768, 901},
            {452, 678, 904, 1130, 1358, 1586},
            {813, 1302, 1791, 2280, 2771, 3262},
            {1612, 2525, 3438, 4351, 5265, 6179},
            {3094, 4821, 6548, 8275, 10003, 11731}};

    private static int[][] minHp = {
            {10, 30, 50, 75, 105},
            {65, 110, 160, 215, 275},
            {180, 330, 470, 600, 750},
            {650, 1110, 1572, 2030, 2500},
            {1300, 2350, 3400, 4450, 5500},
            {3500, 5375, 7250, 9125, 11000},
            {7500, 11125, 14750, 18375, 22000},
            {16000, 23750, 31500, 42000, 47000},
            {35000, 53125, 71250, 89375, 107500},
            {80000, 116250, 188750, 225000, 225000}
    };

    private static int[][] maxHp = {
            {25, 45, 70, 100, 130},
            {90, 140, 195, 255, 320} ,
            {250, 390, 540, 680, 840},
            {850, 1310, 1770, 2230, 2700} ,
            {1800, 2850, 3900, 4950, 6000} ,
            {4500, 6375, 8250, 10125, 12000},
            {10500, 14125, 17750, 21375, 25000},
            {21000, 28750, 36500, 44250, 52000},
            {42500, 60625, 78750, 96875, 115000},
            {95000, 131250, 167500, 203750, 240000}
    };

    public static int createDamage(int tier, int rar, int item, boolean max){
        tier-=1;
        int base = damage[tier][rar + (max ? 1 : 0)];
        double diff = (damage[tier][rar+1] - damage[tier][rar])/1.1D;
        base = new Random().nextInt((int)diff) + base;
        return (int) minifyWepStat(item, base);
    }

/**
    public static int createDamage(int t, int r, int i, boolean big){
            t-=1;
            int base = getDamage(t, i);
            int differnce = getDamage(t,i+1);
            return new Random().nextInt(differnce) + base;
    }**/

    public static int createHpBase(int tier, int rar, int item){
        tier -=1;
        double diff = (maxHp[tier][rar] - minHp[tier][rar]);
        int hp = new Random().nextInt((int)diff) + minHp[tier][rar];
        hp = minifyArmourStat(item, (double) hp);
        return hp;
    }

    public static double minifyWepStat(int item, int stat){
        if(item == 0 || item == 1) {
            return stat/1.8D;
        }
        return stat;
    }

    public static int minifyArmourStat(int item, double stat){
        if(item == 4 || item == 7) {
            stat =  stat/1.5D;
        }
        return (int) stat;
    }

}
