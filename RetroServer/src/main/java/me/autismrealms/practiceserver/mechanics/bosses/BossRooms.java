package me.autismrealms.practiceserver.mechanics.bosses;

import me.autismrealms.practiceserver.PracticeServer;
import me.autismrealms.practiceserver.mechanics.item.Rarity;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Random;

public enum BossRooms {

    Common(new Location(PracticeServer.world,555, 75, -981),
           new Location(PracticeServer.world,-575, 75, -956), 3);

    public Location loc1;
    public Location loc2;
    public int waves;
    public Location center;

    public static HashMap<Player, BossRooms> roomMap = new HashMap();
    public static HashMap<Player, BossTypes> bossMap = new HashMap();
    public static HashMap<BossRooms, Integer> wave = new HashMap();
    public static HashMap<BossRooms, Integer> kills = new HashMap();

    BossRooms(Location loc1, Location loc2, int waves){
        this.loc1 = loc1;
        this.loc2 = loc2;
        this.waves = waves;
        this.center = getCenter(loc1, loc2);
     }

    public static BossRooms getRandom(){
        BossRooms room = BossRooms.values()[new Random().nextInt(BossRooms.values().length)];
        return room;
    }

    public static boolean inProgress(BossRooms room){
        if(roomMap.containsValue(room)){
            return true;
        }
        return false;
    }

    public static BossRooms getRoom(ItemStack rune){
        Rarity rarity = Rarity.getRarity(rune);
        return BossRooms.valueOf(rarity.toString());
    }

    public static Location getCenter(Location loc1, Location loc2){
        double middleX = (loc1.getX() + loc2.getX()) /2;
        double middleZ = (loc2.getZ() + loc1.getZ()) /2;
        return new Location(PracticeServer.world, middleX, loc1.getY(), middleZ);
    }

    public static Location getRandomLocation(BossRooms room){
        double diffX = Math.abs(room.loc1.getX() - room.loc2.getX());
        double diffZ = Math.abs(room.loc1.getZ() - room.loc2.getZ());

        return new Location(PracticeServer.world,
                room.loc1.getX() + new Random().nextInt((int)diffX),
                room.loc1.getY() + 5,
                room.loc1.getZ() + new Random().nextInt((int)diffZ));
    }

}
