package me.autismrealms.practiceserver;

import me.autismrealms.practiceserver.apis.API;
import me.autismrealms.practiceserver.apis.files.MarketData;
import me.autismrealms.practiceserver.apis.files.PlayerData;
import me.autismrealms.practiceserver.apis.itemapi.ItemAPI;
import me.autismrealms.practiceserver.apis.tab.TabMenu;
import me.autismrealms.practiceserver.commands.bosses.BossCommand;
import me.autismrealms.practiceserver.commands.bosses.BossLeave;
import me.autismrealms.practiceserver.commands.buddy.AddCommand;
import me.autismrealms.practiceserver.commands.buddy.DeleteCommand;
import me.autismrealms.practiceserver.commands.chat.ChatTagCommand;
import me.autismrealms.practiceserver.commands.chat.GlobalCommand;
import me.autismrealms.practiceserver.commands.chat.MessageCommand;
import me.autismrealms.practiceserver.commands.chat.ReplyCommand;
import me.autismrealms.practiceserver.commands.duels.*;
import me.autismrealms.practiceserver.commands.guilds.GuildWipeAllCommand;
import me.autismrealms.practiceserver.commands.items.*;
import me.autismrealms.practiceserver.commands.misc.*;
import me.autismrealms.practiceserver.commands.moderation.*;
import me.autismrealms.practiceserver.commands.party.*;
import me.autismrealms.practiceserver.commands.toggles.*;
import me.autismrealms.practiceserver.manager.ManagerHandler;
import me.autismrealms.practiceserver.mechanics.altar.Altar;
import me.autismrealms.practiceserver.mechanics.bosses.BossEnchant;
import me.autismrealms.practiceserver.mechanics.bosses.rune.RuneHandler;
import me.autismrealms.practiceserver.mechanics.chat.ChatMechanics;
import me.autismrealms.practiceserver.mechanics.chat.gui.ChatTagGUIHandler;
import me.autismrealms.practiceserver.mechanics.damage.Damage;
import me.autismrealms.practiceserver.mechanics.damage.Staffs;
import me.autismrealms.practiceserver.mechanics.donations.Crates.CratesMain;
import me.autismrealms.practiceserver.mechanics.donations.Nametags.Nametag;
import me.autismrealms.practiceserver.mechanics.donations.StatTrak.PickTrak;
import me.autismrealms.practiceserver.mechanics.donations.StatTrak.WepTrak;
import me.autismrealms.practiceserver.mechanics.drops.DropPriority;
import me.autismrealms.practiceserver.mechanics.drops.Mobdrops;
import me.autismrealms.practiceserver.mechanics.drops.buff.BuffHandler;
import me.autismrealms.practiceserver.mechanics.drops.generate.GenerateOrb;
import me.autismrealms.practiceserver.mechanics.duels.Duels;
import me.autismrealms.practiceserver.mechanics.dungeon.EntityRegistry;
import me.autismrealms.practiceserver.mechanics.enchants.Enchants;
import me.autismrealms.practiceserver.mechanics.guilds.GuildMechanics;
import me.autismrealms.practiceserver.mechanics.guilds.guild.GuildChat;
import me.autismrealms.practiceserver.mechanics.idler.Idler;
import me.autismrealms.practiceserver.mechanics.idler.IdlerConfig;
import me.autismrealms.practiceserver.mechanics.item.Durability;
import me.autismrealms.practiceserver.mechanics.item.Repairing;
import me.autismrealms.practiceserver.mechanics.item.Untradeable;
import me.autismrealms.practiceserver.mechanics.item.betavendor.Vendor;
import me.autismrealms.practiceserver.mechanics.item.scroll.ScrollGUIHandler;
import me.autismrealms.practiceserver.mechanics.loot.LootChests;
import me.autismrealms.practiceserver.mechanics.market.MarketHandler;
import me.autismrealms.practiceserver.mechanics.mobs.Mobs;
import me.autismrealms.practiceserver.mechanics.mobs.elite.GolemElite;
import me.autismrealms.practiceserver.mechanics.mobs.elite.SkeletonElite;
import me.autismrealms.practiceserver.mechanics.moderation.ModerationMechanics;
import me.autismrealms.practiceserver.mechanics.moderation.Reports;
import me.autismrealms.practiceserver.mechanics.money.Banks;
import me.autismrealms.practiceserver.mechanics.money.Economy.Economy;
import me.autismrealms.practiceserver.mechanics.money.GemPouches;
import me.autismrealms.practiceserver.mechanics.party.Parties;
import me.autismrealms.practiceserver.mechanics.patch.PatchIO;
import me.autismrealms.practiceserver.mechanics.patch.PatchListener;
import me.autismrealms.practiceserver.mechanics.player.*;
import me.autismrealms.practiceserver.mechanics.player.GamePlayer.GamePlayer;
import me.autismrealms.practiceserver.mechanics.player.Mounts.Elytras;
import me.autismrealms.practiceserver.mechanics.player.Mounts.Horses;
import me.autismrealms.practiceserver.mechanics.player.Stats.StatsMain;
import me.autismrealms.practiceserver.mechanics.profession.Enchanter;
import me.autismrealms.practiceserver.mechanics.profession.Fishing;
import me.autismrealms.practiceserver.mechanics.profession.Mining;
import me.autismrealms.practiceserver.mechanics.profession.ProfessionMechanics;
import me.autismrealms.practiceserver.mechanics.progress.Progress;
import me.autismrealms.practiceserver.mechanics.progress.ProgressionHandler;
import me.autismrealms.practiceserver.mechanics.pvp.Alignments;
import me.autismrealms.practiceserver.mechanics.pvp.ForceField;
import me.autismrealms.practiceserver.mechanics.pvp.Respawn;
import me.autismrealms.practiceserver.mechanics.shard.Shard;
import me.autismrealms.practiceserver.mechanics.spawner.Spawners;
import me.autismrealms.practiceserver.mechanics.teleport.Hearthstone;
import me.autismrealms.practiceserver.mechanics.teleport.TeleportBooks;
import me.autismrealms.practiceserver.mechanics.vendors.*;
import me.autismrealms.practiceserver.mechanics.world.Antibuild;
import me.autismrealms.practiceserver.mechanics.world.Logout;
import me.autismrealms.practiceserver.mechanics.world.region.RegionHandler;
import me.autismrealms.practiceserver.utils.ArmorListener;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 *
 * @author Jaxson (Red29 - uncureableAutism@outlook.com)
 * @author Giovanni N. (VawkeNetty - development@vawke.io)
 * @author Subby (Availor - Haven't learnt any java but is already fluent???)
 *         (xxbboy)
 *         <p>
 *         Original Authors -> - I Can't Code (BPWeber - Naughty, Naughty,
 *         Naughty) - Randal Gay Boy (iFamasssRAWRxD - Hentai, Hentai, Hentai)
 *         <p>
 *         Updated to Minecraft 1.9 -> - Written by Giovanni (VawkeNetty) 2017.
 *         - Written by Jaxson (Red29) 2016/2017.
 *         <p>
 *         Development continued by -> - Written by Jaxson (Red29) 2016/2017. -
 *         Written by Brandon (Kayaba) 2017. (Big scammer) (Stole $2k from
 *         Jaxson)
 *         <p>
 *         --------------- From Feb 2017 to May 2017 -------------- Server
 *         offline --------------------- - Written by Giovanni N. (VawkeNetty)
 *         2017-2017. (April 30th to August 1st) - Subby (Availor) April 30th
 *         2017 - ??? (Brought back AR with Giovanni). - Jaxson (Red) (HES BACK)
 *         (HE LEFT AGAIN) 10th July 2017 - 17th October 2017 - Khalid
 *         (Lightlord323) (Took temporary leave) 25th July 2017 - 1st Sepetmber
 *         2017
 *         <p>
<<<<<<< HEAD
 *         - Subby (Availor) April 30th 2017 - ???? - Khalid (Lightlord323) 25th
 *         July 2017 - ???? - Egimfun (Disregard) 7th November 2017 - ???? -
 *         Invested (Screm111) August 2018 - ???? - Kav_ (Kaveen) August 2018 -
 *         ????
=======
 *         - Subby (Availor) April 30th 2017 - ???? 
 *         - Khalid (Lightlord323) 25th July 2017 - ???? 
 *         - Egimfun (Disregard) 7th November 2017 - ????
 *         - Invested (Screm111) August 2018 - ????
 *         - Kav_ (Kaveen) August 2018 - ????
 *         - CUXNT October 2018 - ????
>>>>>>> assorted changes, droprates, fixes, etc
 */

public class PracticeServer extends JavaPlugin {

	public static int tier = 7;
	public static final boolean OPEN_BETA_STATS = true;
	public static final boolean FFA = false;
	public static final boolean BETA_VENDOR_ENABLED = false;
	public static final double MIN_DAMAGE_MULTIPLIER = 1.3;
	public static final boolean DOUBLE_DROP_RATE = false;
	public static final double MAX_DAMAGE_MULTIPLIER = 1.5;
	public static final double HPS_MULTIPLIER = 2.5;
	public static final boolean GLOWING_NAMED_ELITE_DROP = true;
	public static final boolean RANDOM_DURA_NAMED_ELITE_DROP = false;
	public static final double HP_MULTIPLIER = 1.7;
	public static final String VERSION_STRING = "1.1.2.4";
	public static boolean ALIGNMENT_GLOW = false;
	public static World world = Bukkit.getWorld("seven");


	public static PracticeServer plugin;
	public static Logger log;
	private static PatchIO patchIO;
	private static Alignments alignments;
	private static Antibuild antibuild;
	private static Banks banks;
	private static Buddies buddies;
	private static DropPriority dropPriority;
	private static ChatMechanics chatMechanics;
	private static Damage damage;
	private static Durability durability;
	private static Enchants enchants;
	private static Energy energy;
	private static GemPouches gemPouches;
	private static Hearthstone hearthstone;
	private static Horses horses;
	private static ItemVendors itemVendors;
	private static Listeners listeners;
	public static Logout logout;
	private static LootChests lootChests;
	private static MerchantMechanics merchantMechanics;
	private static Mining mining;
	private static Mobdrops mobdrops;
	private static Mobs mobs;
	private static GuildMechanics guildMechanics;
	private static GenerateOrb orbs;
	private static Parties parties;
	private static ProfessionMechanics professionMechanics;
	private static Repairing repairing;
	private static Respawn respawn;
	private static Spawners spawners;
	private static BossEnchant bossEnchant;
	private static Speedfish speedfish;
	private static Staffs staffs;
	private static Duels duels;
	private static TeleportBooks teleportBooks;
	private static Toggles toggles;
	private static Untradeable untradeable;
	private static Trading trading;
	private static CratesMain cm;
	private static Idler idler;
	private static Economy em;
	private static ForceField ff;
	private static Nametag nt;
	private static WepTrak wepTrak;
	private static GolemElite golemElite;
	private static Altar altar;
	private static PickTrak pickTrak;
	private static StatsMain stat;
	private static GamePlayer gap;
	private static Vendor vendor;
	private static ModerationMechanics moderationMechanics;
	private static Reports reports;
	private static IdlerConfig idlerConfig;
	private static RuneHandler rune;
	private static Progress progress;
	private static PracticeServer instance;
	private static MarketData marketData;
	private static PlayerData playerData;
	private static ProgressionHandler handler;
	private static ManagerHandler managerHandler; // here
	private static BuffHandler buffHandler;
	private static GemGambling gemGambling;
	private static Shard shard;
	private static GuildChat guildchat;
	public static boolean devstatus;
	public static ArrayList<String> patchnotes = new ArrayList<String>();

	public static Spawners getSpawners() {
		return spawners;
	}

	public static Economy getEconomy() {
		return em;
	}

	public static Mobs getMobs() {
		return mobs;
	}

	public static PracticeServer getInstance() {
		return instance;
	}

	public static BuffHandler buffHandler() {
		return buffHandler;
	}

	public static PatchIO getPatchIO() {
		return patchIO;
	}

	public static MarketData getMarketData() {
		return marketData;
	}

	public static PlayerData getPlayerData() {
		return playerData;
	}

	public static ManagerHandler getManagerHandler() {
		return managerHandler;
	}

	@Override
	public void onEnable() {
		plugin = this;
		instance = this;
		handler = new ProgressionHandler();
		handler.onEnable();
		loadDefaultDevStatusConfig();
		this.devstatus = plugin.getConfig().getBoolean("dev-server");
		Bukkit.getWorlds().get(0).setAutoSave(false);
		new BukkitRunnable() {

			public void run() {
				Bukkit.getServer().getOnlinePlayers().forEach(Player::saveData);
			}
		}.runTaskTimerAsynchronously(this, 6000, 6000);
		if (!getDataFolder().exists()) {
			getDataFolder().mkdirs();
		}
		marketData = new MarketData(this);
		rune = new RuneHandler();
		playerData = new PlayerData(this);
		Bukkit.getPluginManager().registerEvents(new ChatTagGUIHandler(), this);
		Bukkit.getPluginManager().registerEvents(new ScrollGUIHandler(), this);
		Bukkit.getPluginManager().registerEvents(new MarketHandler(), this);
		managerHandler = new ManagerHandler();
		log = plugin.getLogger();
		gap = new GamePlayer();
		moderationMechanics = new ModerationMechanics();
		cm = new CratesMain();
		guildMechanics = GuildMechanics.getInstance();
		stat = new StatsMain();
		progress = new Progress();
		trading = new Trading();
		shard = new Shard();
		bossEnchant = new BossEnchant();
		altar = new Altar();
		nt = new Nametag();
		wepTrak = new WepTrak();
		idlerConfig = new IdlerConfig();
		idler = new Idler();
		pickTrak = new PickTrak();
		alignments = new Alignments();
		antibuild = new Antibuild();
		banks = new Banks();
		buddies = new Buddies();
		ff = new ForceField();
		vendor = new Vendor();
		duels = new Duels();
		chatMechanics = new ChatMechanics();
		damage = new Damage();
		durability = new Durability();
		enchants = new Enchants();
		energy = new Energy();
		gemPouches = new GemPouches();
		hearthstone = new Hearthstone();
		horses = new Horses();
		itemVendors = new ItemVendors();
		listeners = new Listeners();
		em = new Economy();
		dropPriority = new DropPriority();
		logout = new Logout();
		lootChests = new LootChests();
		merchantMechanics = new MerchantMechanics();
		mining = new Mining();
		mobdrops = new Mobdrops();
		mobs = new Mobs();
		orbs = new GenerateOrb();
		parties = new Parties();
		professionMechanics = new ProfessionMechanics();
		repairing = new Repairing();
		respawn = new Respawn();
		spawners = new Spawners();
		speedfish = new Speedfish();
		staffs = new Staffs();
		teleportBooks = new TeleportBooks();
		toggles = new Toggles();
		untradeable = new Untradeable();
		reports = new Reports();
		guildchat = new GuildChat();
		golemElite = new GolemElite();
		managerHandler.onEnable();
		gap.onEnable();
		moderationMechanics.onEnable();
		alignments.onEnable();
		antibuild.onEnable();
		altar.onEnable();
		banks.onEnable();
		buddies.onEnable();
		progress.onEnable();
		chatMechanics.onEnable();
		vendor.onEnable();
		damage.onEnable();
		durability.onEnable();
		enchants.onEnable();
		nt.onEnable();
		duels.onEnable();
		wepTrak.onEnable();
		rune.onEnable();
		pickTrak.onEnable();
		energy.onEnable();
		gemPouches.onEnable();
		hearthstone.onEnable();
		stat.onEnable();
		dropPriority.onEnable();
		horses.onEnable();
		idler.onEnable();
		itemVendors.onEnable();
		ff.onEnable();
		cm.onEnable();
		listeners.onEnable();
		logout.onEnable();
		lootChests.onEnable();
		bossEnchant.onEnable();
		merchantMechanics.onEnable();
		mining.onEnable();
		mobdrops.onEnable();
		mobs.onEnable();
		orbs.onEnable();
		parties.onEnable();
		professionMechanics.onEnable();
		repairing.onEnable();
		respawn.onEnable();
		idlerConfig.onEnable();
		spawners.onEnable();
		speedfish.onEnable();
		shard.onEnable();
		staffs.onEnable();
		teleportBooks.onEnable();
		em.onEnable();
		toggles.onEnable();
		guildMechanics.onEnable();
		trading.onEnable();
		untradeable.onEnable();
		reports.onEnable();
		guildchat.onEnable();
		golemElite.onEnable();
		registerCommands();
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		ItemAPI.init();
		new RegionHandler().init();
		new SkeletonElite().init();
		Fishing.getInstance().loadFishingLocations();
		OrbGambling orbGambling = new OrbGambling();
		gemGambling = new GemGambling();
		orbGambling.initPool();
		buffHandler = new BuffHandler();
		getServer().getPluginManager().registerEvents(buffHandler, this);
		getServer().getPluginManager().registerEvents(orbGambling, this);
		getServer().getPluginManager().registerEvents(gemGambling, this);

		buffHandler.init();
		EntityRegistry.registerEntities();
		getServer().getPluginManager().registerEvents(new OreMerchant(), this);
		getServer().getPluginManager().registerEvents(new Enchanter(), this);
		Fishing.checkMonkRegion();
		patchIO = new PatchIO();
		getServer().getPluginManager().registerEvents(new PatchListener(), this);
		TabMenu.init2();
		API.getRainbowSheepTask().init();
		new BukkitRunnable() {
			@Override
			public void run() {
				refreshPatchNotes();
			}
		}.runTaskAsynchronously(this);
		new Elytras(this);
		new ArmorListener(this);

	}

	public void onDisable() {
		instance = null;

		duels.onDisable();
		managerHandler.onDisable();
		trading.onDisable();
		em.onDisable();
		logout.onDisable(false);
		moderationMechanics.onDisable();
		buddies.onDisable();
		alignments.onDisable();
		antibuild.onDisable();
		banks.onDisable();
		lootChests.onDisable();
		spawners.onDisable();
		chatMechanics.onDisable();
		progress.onDisable();
		damage.onDisable();
		durability.onDisable();
		idler.onDisable();
		enchants.onDisable();
		dropPriority.onDisable();
		energy.onDisable();
		gemPouches.onDisable();
		hearthstone.onDisable();
		horses.onDisable();
		itemVendors.onDisable();
		listeners.onDisable();
		merchantMechanics.onDisable();
		mining.onDisable();
		mobdrops.onDisable();
		mobs.onDisable();
		stat.onDisable();
		parties.onDisable();
		professionMechanics.onDisable();
		repairing.onDisable();
		respawn.onDisable();
		speedfish.onDisable();
		staffs.onDisable();
		teleportBooks.onDisable();
		toggles.onDisable();
		untradeable.onDisable();
		reports.onDisable();
		guildMechanics.onDisable();
	}

	public void registerCommands() {
		getCommand("givemask").setExecutor(new GiveMaskCommand()); // share all
		getCommand("drop").setExecutor(new DropCommand());															// of this
		getCommand("givecandy").setExecutor(new GiveCandyCommand()); // tjis
		getCommand("givehallowcrate").setExecutor(new HalloweenCrateCommand()); // this
		getCommand("CheckGems").setExecutor(new CheckGemsCommand()); // this
		getCommand("giveLegendaryOrb").setExecutor(new LegendaryOrbCommand()); // this
																				// kk
		getCommand("shard").setExecutor(new ShardCommand());
		getCommand("toggleff").setExecutor(new ToggleFFCommand());
		getCommand("maxpartysize").setExecutor(new MaxPartySizeCommand());
		getCommand("togglemobs").setExecutor(new ToggleMobsCommand());
		getCommand("listparties").setExecutor(new ListPartiesCommand());
		getCommand("togglechaos").setExecutor(new ToggleChaosCommand());
		getCommand("togglepvp").setExecutor(new TogglePVPCommand());
		getCommand("toggledebug").setExecutor(new ToggleDebugCommand());
		getCommand("setAlignment").setExecutor(new SetAlignmentCommand());
		getCommand("market").setExecutor(new MarketCommand());
		getCommand("lootbuff").setExecutor(new BuffCommand());
		getCommand("duel").setExecutor(new DuelCommand());
		getCommand("daccept").setExecutor(new DuelAcceptCommand());
		getCommand("pduel").setExecutor(new PartyDuelCommand());
		getCommand("pdaccept").setExecutor(new PartyDuelAcceptCommand());
		getCommand("dquit").setExecutor(new DuelQuitCommand());
		getCommand("createBankNote").setExecutor(new CreateBankNoteCommand());
		getCommand("giveScroll").setExecutor(new GiveProtectionScrollCommand());
		getCommand("tags").setExecutor(new ChatTagCommand());
		getCommand("tellall").setExecutor(new TellAllCommand());
		getCommand("gl").setExecutor(new GlobalCommand());
		getCommand("boss").setExecutor(new BossCommand());
		getCommand("givePouch").setExecutor(new GivePouchCommand());
		getCommand("GiveCrate").setExecutor(new GiveCrateCommand());
		getCommand("GiveOrb").setExecutor(new GiveOrbCommand());
		getCommand("GiveNameTag").setExecutor(new GiveNameTagCommand());
		getCommand("giveEnchant").setExecutor(new GiveEnchantCommand());
		getCommand("giveGodPick").setExecutor(new GiveGodPickCommand());
		getCommand("giveInsanePick").setExecutor(new GiveInsanePickCommand());
		getCommand("giveAll").setExecutor(new GiveAllCommand());
		getCommand("giveMagnetPick").setExecutor(new GiveMagnetPickCommand());
		getCommand("giveWepTrak").setExecutor(new GiveWepTrakCommand());
		getCommand("givePickTrak").setExecutor(new GivePickTrakCommand());
		getCommand("bquit").setExecutor(new BossLeave());
		getCommand("pet").setExecutor(new PetCommand());
		getCommand("message").setExecutor(new MessageCommand());
		getCommand("reply").setExecutor(new ReplyCommand());
		getCommand("roll").setExecutor(new RollCommand());
		getCommand("fakeroll").setExecutor(new FakeRollCommand());
		getCommand("toggle").setExecutor(new ToggleCommand());
		getCommand("add").setExecutor(new AddCommand());
		getCommand("del").setExecutor(new DeleteCommand());
		getCommand("logout").setExecutor(new LogoutCommand());
		getCommand("setrank").setExecutor(new SetRankCommand());
		getCommand("banksee").setExecutor(new BankSeeCommand());
		getCommand("psvanish").setExecutor(new VanishCommand());
		getCommand("invsee").setExecutor(new InvSeeCommand());
		getCommand("guildwipeall").setExecutor(new GuildWipeAllCommand());
		getCommand("drheal").setExecutor(new HealCommand());
		getCommand("createdrop").setExecutor(new CreateDropCommand());
		getCommand("sc").setExecutor(new StaffChatCommand());
		getCommand("showms").setExecutor(new ShowMSCommand());
		getCommand("hidems").setExecutor(new HideMSCommands());
		getCommand("killall").setExecutor(new KillAllCommand());
		getCommand("monspawn").setExecutor(new MonSpawnCommand());
		getCommand("showloot").setExecutor(new ShowLootCommand());
		getCommand("hideloot").setExecutor(new HideLootCommand());
		getCommand("pinvite").setExecutor(new PInviteCommand());
		getCommand("paccept").setExecutor(new PAcceptCommand());
		getCommand("pkick").setExecutor(new PKickCommand());
		getCommand("pquit").setExecutor(new PQuitCommand());
		getCommand("pdecline").setExecutor(new PDeclineCommand());
		getCommand("p").setExecutor(new PartyCommand());
		getCommand("food").setExecutor(new FeedCommand());
		getCommand("report").setExecutor(new ReportCommand());
		getCommand("editreport").setExecutor(new EditReportCommand());
		getCommand("bugs").setExecutor(new BugsCommand());
		getCommand("patch").setExecutor(new PatchCommand());
		getCommand("givebuff").setExecutor(new GiveBuffCommand());
		getCommand("orbs").setExecutor(new OrbsCommand());
		getCommand("armorsee").setExecutor(new ArmorSeeCommand());
		getCommand("psmute").setExecutor(new MuteCommand());
		getCommand("psunmute").setExecutor(new UnmuteCommand());
		getCommand("discord").setExecutor(new DiscordCommand());
		getCommand("broadcasttopplayers").setExecutor(new TopPlayersCommand());
		getCommand("toggleenergy").setExecutor(new ToggleEnergyCommand());
		getCommand("dpsdummy").setExecutor(new DPSDummyCommand(this));
		getCommand("deploy").setExecutor(new DeployCommand(this));
		getCommand("rrversion").setExecutor(new RRVersionCommand());
		getCommand("togglegm").setExecutor(new ToggleGMCommand());
		getCommand("togglealignmentglow").setExecutor(new ToggleAlignmentGlowCommand());
		getCommand("elytra").setExecutor(new ElytraCommand());
		getCommand("giverune").setExecutor(new GiveRune());


	}

	public static void refreshPatchNotes() {

		try {
			patchnotes.clear();
			URL url = new URL("https://retrorealms.net/patchnotes.txt");
			Scanner scan = new Scanner(url.openStream());
			while (scan.hasNext()) {
				PracticeServer.patchnotes.add(scan.nextLine());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void loadDefaultDevStatusConfig() {
		plugin.getConfig().addDefault("dev-server", false);
		plugin.getConfig().options().copyDefaults(true);
		plugin.saveConfig();

	}

}
