package me.autismrealms.practiceserver.commands.misc;

import me.autismrealms.practiceserver.mechanics.player.Listeners;
import me.autismrealms.practiceserver.mechanics.pvp.Alignments;
import me.autismrealms.practiceserver.mechanics.shard.gui.ServerGUI;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Jaxon on 8/20/2017.
 */
public class ShardCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender instanceof Player) {
            Player player = (Player)commandSender;
            if(Listeners.isInCombat(player)) {
                player.sendMessage(ChatColor.RED + "You cannot change servers while in combat!");
                return false;
            }else{
                player.openInventory(ServerGUI.serverGUI(player));
                player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 1, 1);
            }
        }
        return false;
    }
}
