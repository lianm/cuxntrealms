package me.autismrealms.practiceserver.mechanics.dungeon.abyss;

import me.autismrealms.practiceserver.mechanics.dungeon.reflect.Reflection;
import net.minecraft.server.v1_9_R2.EntityWither;
import net.minecraft.server.v1_9_R2.GenericAttributes;
import net.minecraft.server.v1_9_R2.PathfinderGoalSelector;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_9_R2.CraftWorld;

import java.util.Set;

/**
 * Created by Giovanni on 20-5-2017.
 */
public class EntityDaemonLord extends EntityWither {


    public EntityDaemonLord(World world) {
        super(((CraftWorld) world).getHandle());

        this.getAttributeInstance(GenericAttributes.maxHealth).setValue(150000);
        this.setHealth(150000);

        this.setCustomName(ChatColor.DARK_RED + "Infernal Daemon Deathlord of the Abyss");
        this.setCustomNameVisible(true);
    }

    public EntityDaemonLord clearAI() {
        ((Set) Reflection.getPrivateField("c", PathfinderGoalSelector.class, this.goalSelector)).clear();
        ((Set) Reflection.getPrivateField("b", PathfinderGoalSelector.class, this.goalSelector)).clear();

        ((Set) Reflection.getPrivateField("c", PathfinderGoalSelector.class, this.targetSelector)).clear();
        ((Set) Reflection.getPrivateField("b", PathfinderGoalSelector.class, this.targetSelector)).clear();

        return this;

    }
}
