package me.autismrealms.practiceserver.mechanics.enchants;

import me.autismrealms.practiceserver.mechanics.drops.generate.GenerateOrb;
import me.autismrealms.practiceserver.mechanics.item.ItemType;
import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.item.Rarity;
import me.autismrealms.practiceserver.mechanics.tiers.Tiers;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class GenerateEnchant {

    public static boolean canEnchant(ItemStack scroll, ItemStack item){

        int scrollTier = Tiers.getStringTier(scroll.getItemMeta().getDisplayName().substring(8));
        int itemTier = Tiers.getStringTier(item.getItemMeta().getDisplayName());

        if(itemTier == scrollTier) return true;
        else return false;
    }

    public void createFirework(Player p){
        p.getWorld().playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.25f);
        final Firework fw = (Firework) p.getWorld().spawnEntity(p.getLocation(), EntityType.FIREWORK);
        final FireworkMeta fwm = fw.getFireworkMeta();
        final FireworkEffect effect = FireworkEffect.builder().flicker(false).withColor(Color.YELLOW).withFade(Color.YELLOW).with(FireworkEffect.Type.BURST).trail(true).build();
        fwm.addEffect(effect);
        fwm.setPower(0);
        fw.setFireworkMeta(fwm);
    }

    public static ItemStack doEnchant(ItemStack item){

        return item;
    }

    public static ItemStack enchantItem(GenerateOrb.Type orb, ItemType itemType, ItemStack item){
        ItemMeta itemMeta = item.getItemMeta();
        List<String> lore = item.getItemMeta().getLore();
        if (itemType == ItemType.WEAPON) {
            if(orb== GenerateOrb.Type.LEGENDARY) {
                for (int i = 0; i < 4- Items.getPlussedAmount(item); i++) {
                    itemMeta.setDisplayName(applyPlus(itemMeta.getDisplayName()));
                    lore.set(0, increaseDmg(lore.get(0)));
                }
            }else{
                itemMeta.setDisplayName(itemMeta.getDisplayName());
                lore.set(0, increaseDmg(lore.get(0)));
            }
        } else if (itemType == ItemType.ARMOR) {
            if(orb == GenerateOrb.Type.LEGENDARY) {
                for (int i = 0; i < 4 - Items.getPlussedAmount(item); i++) {
                    itemMeta.setDisplayName(applyPlus(itemMeta.getDisplayName()));
                    lore.set(1, increaseHp(lore.get(1)));
                    lore.set(2, increaseEnergyHpRegen(lore.get(2)));
                }
            }else{
                itemMeta.setDisplayName(itemMeta.getDisplayName());
                lore.set(1, increaseHp(lore.get(1)));
                lore.set(2, increaseEnergyHpRegen(lore.get(2)));
            }
        }
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);
        return item;

    }


    public static String applyPlus(String name){
        int amt = 1;
        String rest = name;
        if(Items.isPlussed(ChatColor.stripColor(name))){
            amt = getPlus(name) +1;
            rest = name.split("] ")[1];
        }
        return ChatColor.RED + "[+" + amt + "] " + rest;
    }

    public static int getPlus(String name){
        if(name.contains("[+")) {
            name = name.split("\\[+")[1];
            name = name.split("] ")[0];
            return Integer.parseInt(name);
        }
        return 0;
    }

    public static String increaseDmg(String dmg){
        String[] dmgPart = dmg.split(": ")[1].split(" - ");
        double min = Integer.parseInt(dmgPart[0]) * 1.05D;
        double max = Integer.parseInt(dmgPart[1]) * 1.05D;
        if(min - Integer.parseInt(dmgPart[0]) < 1){
            min = Integer.parseInt(dmgPart[0]) + 1;
        }
        if(max - Integer.parseInt(dmgPart[1]) < 1){
            max = Integer.parseInt(dmgPart[1]) + 1;
        }
        return ChatColor.RED + "DMG: " + (int) min + " - " + (int) max;
    }

    public static String increaseHp(String hpline){
        if(hpline.contains("HP")) {
            int hpint = Integer.parseInt(hpline.split(": +")[1]);
            double newHp = hpint * 1.05D;

            if (newHp - hpint < 1) {
                newHp = hpint + 1;
            }
            hpline = ChatColor.RED + "HP: +" + (int) newHp;
        }
        return hpline;
    }

    public static String increaseEnergyHpRegen(String regen){
        double stat;
        if(regen.contains("ENERGY")){
            stat = Integer.parseInt(regen.split(": +")[1].split("%")[0]);
            regen = ChatColor.RED + "ENERGY REGEN: +" + (int) (stat +1) + "%";
        }else{
            stat = Integer.parseInt(regen.split(": +")[1].split(" HP/s")[0]);
            stat = stat * 1.05D;
            regen = ChatColor.RED + "HP REGEN: +" + (int) stat + " HP/s";
        }
        return regen;
    }
    
}
