package me.autismrealms.practiceserver.mechanics.drops;

import me.autismrealms.practiceserver.mechanics.bosses.BossTypes;
import me.autismrealms.practiceserver.mechanics.item.ItemHandler;
import me.autismrealms.practiceserver.mechanics.mobs.MobType;

public class GenerateLore {

    public static String createLore(BossTypes boss, int item){
        MobType mobType = boss.type;
        return MobType.getName(mobType) + " " + ItemHandler.getItem(item).toLowerCase() +
                " made from " + ItemHandler.getMatString(boss.tier, item).toLowerCase() + ".";
    }

}
