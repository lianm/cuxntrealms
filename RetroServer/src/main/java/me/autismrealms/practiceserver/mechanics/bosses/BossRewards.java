package me.autismrealms.practiceserver.mechanics.bosses;

import me.autismrealms.practiceserver.mechanics.item.Items;
import me.autismrealms.practiceserver.mechanics.money.Money;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public enum BossRewards {

    LegendaryOrbs("Legendary Orbs"),
    Gems("Gems"),
    Enchant("Powerful Enchant");

    public String name;

    BossRewards(String name){
        this.name = name;
    }


    public static ItemStack getReward(BossRewards item, int tier){
        switch (item){
            case LegendaryOrbs:
                ItemStack reward = Items.orb(false);
                reward.setAmount(new Random().nextInt(3) + 5);
                return reward;
            case Gems:
                int amt = new Random().nextInt(100) + 300 * tier;
                return Money.createBankNote(amt * tier);
            case Enchant:
                int enchAmt = new Random().nextInt(8)+4;
                return BossEnchant.create(enchAmt, tier);
        }
        return null;
    }

}
